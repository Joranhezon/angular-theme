# Angular Voce.serpro THEME


## Getting started

**1. To use with `npm start` command**
> **PS:** To developer mode

Run these commands to set the proper theme and skin

`npm config set angular-theme:theme angular-voce-serpro`

**2. To generate a build**
> **PS:** Deploy to production

* Create a specific `package.json` file into this directory
> Use the **`npm init`** command to create this file

