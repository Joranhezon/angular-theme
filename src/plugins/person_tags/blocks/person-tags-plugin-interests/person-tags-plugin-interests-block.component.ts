import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { ProfileService } from '../../../../app/services/profile.service';
import { PersonService } from './../../../../app/services/person.service';
import { Tag } from '../../../../app/models/tag.model';

@Component({
    selector: "noosfero-interest-tags-block",
    templateUrl: './person-tags-plugin-interests-block.html',
    styleUrls: ['./person-tags-plugin-interests-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PersonTagsPluginInterestsBlockComponent implements OnInit {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    profile: any;
    tags: Tag[];

    constructor(private profileService: ProfileService) { }

    ngOnInit() {
        this.profile = this.owner;
        this.tags = [];
        this.profileService.getTags(this.owner).subscribe((tags: Tag[]) => {
            this.tags = tags;
            this.block.hide = (<any>this.tags.length === 0);
        });
    }
}
