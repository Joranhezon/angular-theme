import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import * as _ from "lodash";

import {BlockService} from '../../../../app/services/block.service';
import {ArticleService} from './../../../../app/services/article.service';
import {DiscussionPresentationMode} from './discussion-presentation-mode';
import { Article } from '../../../../app/models/article.model';
import { Profile } from '../../../../app/models/profile.model';
import { Block } from '../../../../app/models/block.model';

@Component({
    selector: "noosfero-comment-paragraph-plugin-discussion-block",
    templateUrl: './discussion-block.html',
    styleUrls: ['./discussion-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class DiscussionBlockComponent implements OnInit {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    profile: Profile;
    documents: Array<Article>;
    presentation_mode: DiscussionPresentationMode;
    public loading = false;

    constructor(private blockService: BlockService, public articleService: ArticleService) { }

    ngOnInit() {
        this.profile = this.owner;
        if (this.block.api_content) {
            this.documents =  this.block.api_content['articles'];
        } else {
            this.loading = true;
            this.blockService.getBlock(this.block).subscribe((block: Block) => {
                this.documents = block.api_content.articles;
                this.block.hide = !this.documents || this.documents.length === 0;
                this.loading = false;
            });
        }
        this.presentation_mode = DiscussionPresentationMode.TITLE_ONLY;
        if (this.block && this.block.settings && this.block.settings.presentation_mode) {
            this.presentation_mode = this.block.settings.presentation_mode;
        }
        this.watchArticles();
    }

    watchArticles() {
        this.articleService.articleRemoved.subscribe((article: Article) => {
            _.reject(this.documents, article);

        });
    }

    presentAbstract() {
        return this.presentation_mode === DiscussionPresentationMode.TITLE_AND_ABSTRACT;
    }

    presentFullContent() {
        return this.presentation_mode === DiscussionPresentationMode.FULL_CONTENT;
    }

}
