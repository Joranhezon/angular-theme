import { Comment } from '../../../app/models/comment.model';

export class CommentParagraph extends Comment {
    paragraph_uuid?: string;
}
