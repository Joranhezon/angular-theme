import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';

import { ArticleService, EnvironmentService, ProfileService } from './../../../app/services';
import { CommentParagraphService } from './comment-paragraph.service';
import { Article } from '../../../app/models/article.model';
import * as helpers from '../../../spec/helpers';
import { Comment } from '../../../app/models/comment.model';

describe("Services", () => {
    describe("Comment Paragraph Service", () => {
        const mocks = helpers.getMocks();
        let service: CommentParagraphService;
        let httpMock: HttpTestingController;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    ArticleService, CommentParagraphService, ProfileService, EnvironmentService,
                    { provide: ArticleService, useValue: mocks.articleService },
                    { provide: ProfileService, useValue: mocks.profileService },
                    { provide: EnvironmentService, useValue: mocks.environmentService },
                ],
            });
            service = TestBed.get(CommentParagraphService);
            httpMock = TestBed.get(HttpTestingController);
        }));

        afterEach(() => {
            httpMock.verify();
        });


        describe("Succesfull requests", () => {

            // FIXME see if it's usefull
            // it("should create a paragraph comment", (done) => {
            //     let articleId = 1;
            //     $httpBackend.expectPOST(`/api/v1/articles/${articleId}/comment_paragraph_plugin/comments`).respond(200, { comment: { body: "comment1" } });
            //     commentParagraphService.createInArticle(<Article>{ id: articleId }, <Comment>{ body: "comment1" }).then((result: RestResult<Comment>) => {
            //         expect(result.data).toEqual({ body: "comment1" });
            //         done();
            //     });
            //     $httpBackend.flush();
            // });


            it("return counts for paragraph comments", () => {
                const article = <Article>{ id: 1 };
                const forceResponse = { 'uuid': 12 }

                service.commentParagraphCount(article).subscribe((response: any) => {
                    expect(response).toEqual(forceResponse);
                });

                const req = httpMock.expectOne(`/api/v1/articles/${article.id}/comment_paragraph_plugin/comments/count`);
                expect(req.request.method).toBe("GET");
                req.flush(forceResponse);

            });

            it("should get comments's articles using comment paragraph plugin", () => {
                const article = <Article>{ id: 1 };
                const comments = [<Comment>{ id: 1 }, <Comment>{ id: 3 }, <Comment>{ id: 5 }];
                const params = { 'without_reply': true };

                service.getByArticle(article).subscribe((response: any) => {
                    expect(response.body).toEqual(comments);
                });

                const req = httpMock.expectOne({ url: '/api/v1/articles/1/comment_paragraph_plugin/comments?without_reply=true' })

                expect(req.request.method).toBe("GET");
                req.flush(comments);

            });

            // FIXME see if it's usefull
            // it("activate paragraph comments for an article", (done) => {
            //     let articleId = 1;
            //     $httpBackend.expectPOST(`/api/v1/articles/${articleId}/comment_paragraph_plugin/activate`).respond(200, { article: { title: "article1" } });
            //     commentParagraphService.activateCommentParagraph(<Article>{ id: articleId }).then((result: RestResult<Article>) => {
            //         expect(result.data).toEqual({ title: "article1" });
            //         done();
            //     });
            //     $httpBackend.flush();
            // });

            // FIXME see if it's usefull
            // it("deactivate paragraph comments for an article", (done) => {
            //     let articleId = 1;
            //     $httpBackend.expectPOST(`/api/v1/articles/${articleId}/comment_paragraph_plugin/deactivate`).respond(200, { article: { title: "article1" } });
            //     commentParagraphService.deactivateCommentParagraph(<Article>{ id: articleId }).then((result: RestResult<Article>) => {
            //         expect(result.data).toEqual({ title: "article1" });
            //         done();
            //     });
            //     $httpBackend.flush();
            // });



        });
    });
});
