
import { Injectable, Inject } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import * as _ from "lodash";

import { BackendBaseService } from '../../../app/services/backend_base.service';
import { Article } from '../../../app/models/article.model';
import { Comment } from '../../../app/models/comment.model';

@Injectable()
export class CommentParagraphService extends BackendBaseService {


    getByArticle(article: Article, params: any = {}): Observable<HttpResponse<Comment[]>> {
        params['without_reply'] = true;

        return this.http.get<Comment[]>(this.baseURL + '/articles/' + article.id + '/comment_paragraph_plugin/comments', {observe: 'response', params: this.parseParams(params)});
    }

    exportCommentParagraph(article: Article): Observable<any> {
        return this.http.get<any>(this.baseURL + '/articles/' + article.id + '/comment_paragraph_plugin/export');
    }

    commentParagraphCount(article: Article): Observable<any> {
        if (article) {
            return this.http.get<any>(this.baseURL + '/articles/' + article.id + '/comment_paragraph_plugin/comments/count');
        } else {
            return Observable.of({});
        }
    }

}
