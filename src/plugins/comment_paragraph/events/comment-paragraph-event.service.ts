import {Injectable, EventEmitter} from '@angular/core';
import { Article } from '../../../app/models/article.model';

@Injectable()
export class CommentParagraphEventService {

    private toggleCommentParagraphEmitter: EventEmitter<Article>;

    constructor() {
        this.toggleCommentParagraphEmitter = new EventEmitter();
    }

    toggleCommentParagraph(article: Article) {
        this.toggleCommentParagraphEmitter.next(article);
    }

    subscribeToggleCommentParagraph(fn: (article: Article) => void) {
        this.toggleCommentParagraphEmitter.subscribe(fn);
    }
}
