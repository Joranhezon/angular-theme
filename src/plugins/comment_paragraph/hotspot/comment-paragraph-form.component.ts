import { CommentFormHotspotComponent, CommentFormHotspotInterface } from './../../../app/hotspot/comment-form-hotspot.component';
import { Inject, Input, Component, Injector, OnInit } from '@angular/core';
import { Hotspot } from '../../../app/hotspot/hotspot.decorator';
import { Comment } from '../../../app/models/comment.model';
import { CommentParagraph } from '../models/comment_paragraph';

@Component({
    selector: "comment-paragraph-form-hotspot",
    template: "<span></span>",
})
@Hotspot("comment_form_extra_contents")
export class CommentParagraphFormHotspotComponent implements OnInit, CommentFormHotspotInterface {

    parent: CommentParagraph;
    comment: CommentParagraph;

    constructor() { }

    ngOnInit() {
        if (this.comment && this.parent && this.parent.paragraph_uuid) {
            this.comment.paragraph_uuid = this.parent.paragraph_uuid;
        }
    }
}
