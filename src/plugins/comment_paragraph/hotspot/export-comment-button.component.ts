import { ArticleToolbarHotspotComponent, ArticleToolbarHotspotInterface } from './../../../app/hotspot/article-toolbar-hotspot.component';
import { Input, Inject, Injector, Component, ViewEncapsulation } from '@angular/core';
import { Hotspot } from '../../../app/hotspot/hotspot.decorator';
import { CommentParagraphService } from '../http/comment-paragraph.service';
import { saveAs } from 'file-saver';
import { Article } from '../../../app/models/article.model';

@Component({
    selector: "export-comment-button-hotspot",
    templateUrl: './export-comment-button.html',
    styleUrls: ['./export-comment-button.scss'],
    encapsulation: ViewEncapsulation.None,
})
@Hotspot("article_extra_toolbar_buttons")
export class ExportCommentButtonHotspotComponent implements ArticleToolbarHotspotInterface {

    article: Article;
    exportCommentPath: any;

    constructor(private commentParagraphService: CommentParagraphService) { }

    isActivated() {
        return this.article && this.article.setting && this.article.setting.comment_paragraph_plugin_activate;
    }

    openFileForDownload(response: any) {
        const parsedResponse = response.data;
        const blob = new Blob([parsedResponse], { type: 'text/csv' });
        const filename = 'data.csv';
        saveAs(blob, filename);
      }

    export() {
        this.commentParagraphService.exportCommentParagraph(this.article).subscribe(
            (response) => {
                this.openFileForDownload(response);
            }
        );
    }


}
