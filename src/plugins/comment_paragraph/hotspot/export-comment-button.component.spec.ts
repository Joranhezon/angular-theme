import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from './../../../app/shared/services/translator.service';
import { hotspots } from './../../recent_activities/index';
import { ArticleToolbarHotspotComponent } from './../../../app/hotspot/article-toolbar-hotspot.component';
import { SharedModule } from './../../../app/shared.module';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ExportCommentButtonHotspotComponent } from './export-comment-button.component';
import { CommentParagraphService } from '../http/comment-paragraph.service';
import { HotspotModule } from '../../../app/hotspot/hotspot.module';

import * as helpers from '../../../spec/helpers';
import { Article } from '../../../app/models/article.model';

// FIXME refactor this test
xdescribe("Components", () => {
    describe("Export Comment Button Hotspot Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<ExportCommentButtonHotspotComponent>;
        let component: ExportCommentButtonHotspotComponent;

        const commentParagraphService = {};

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ExportCommentButtonHotspotComponent],
                providers: [
                    { provide: TranslatorService, useValue: mocks.translatorService },
                    { provide: CommentParagraphService, useValue: commentParagraphService },
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA],
                imports: [RouterTestingModule, SharedModule, TranslateModule.forRoot()]
            });
            fixture = TestBed.createComponent(ExportCommentButtonHotspotComponent);
            component = fixture.componentInstance;
            component.article = <Article>{};
        }));

        it('return true when comment paragraph is active', () => {
            component.article = <Article>{ setting: { comment_paragraph_plugin_activate: true } };
            fixture.detectChanges();
            const hotspotComponent = fixture.debugElement.query(By.css('export-comment-button-hotspot')).componentInstance;
            expect(hotspotComponent.isActivated()).toBeTruthy();
            expect(fixture.debugElement.queryAll(By.css('.export-comment-button')).length).toEqual(1);
        });

        it('return false when comment paragraph is not active', () => {
            fixture.detectChanges();
            const hotspotComponent = fixture.debugElement.query(By.css('export-comment-button-hotspot')).componentInstance;
            expect(component.isActivated()).toBeFalsy();
            expect(fixture.debugElement.queryAll(By.css('.export-comment-button')).length).toEqual(0);
        });

        it('return false when article has no setting attribute', () => {
            component.article = <Article>{};
            fixture.detectChanges();
            const hotspotComponent = fixture.debugElement.query(By.css('export-comment-button-hotspot')).componentInstance;
            expect(hotspotComponent.isActivated()).toBeFalsy();
            expect(fixture.debugElement.queryAll(By.css('.export-comment-button')).length).toEqual(0);
        });
    });
});
