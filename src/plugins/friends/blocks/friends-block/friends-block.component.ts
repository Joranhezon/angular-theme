import { BlockService } from './../../../../app/services/block.service';
import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { Block } from '../../../../app/models/block.model';
import { Profile } from '../../../../app/models/profile.model';

@Component({
    selector: "noosfero-friends-block",
    templateUrl: './friends-block.html',
    styleUrls: ['./friends-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FriendsBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;
    public loading = false;

    profiles: any = [];
    constructor(private blockService: BlockService) { }

    ngOnInit() {
        const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;

        if (this.block.api_content) {
            this.profiles = this.block.api_content['people'];
        } else {
            this.loading = true;
            this.blockService.getBlock(this.block).subscribe((block: Block) => {
                // FIXME we don't need to use api_content and friends at the same time
                this.profiles = block.api_content['people'];
                this.block.api_content = block.api_content;
                this.loading = false;
            });
        }
    }
}
