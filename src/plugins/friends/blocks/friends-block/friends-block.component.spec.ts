import { BlockService } from './../../../../app/services/block.service';
import { ProfileImageComponent } from './../../../../app/profile/image/profile-image.component';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FriendsBlockComponent } from './friends-block.component';
import * as helpers from '../../../../spec/helpers';
import { Block } from '../../../../app/models/block.model';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {

    describe("Friends Block Component", () => {
        const mocks = helpers.getMocks();

        let fixture: ComponentFixture<FriendsBlockComponent>;
        let component: FriendsBlockComponent;
        const state = jasmine.createSpyObj("$state", ["href"]);

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [FriendsBlockComponent],
                providers: [
                    { provide: BlockService, useValue: mocks.blockService },

                    { provide: "$state", useValue: state }
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(FriendsBlockComponent);
                component = fixture.componentInstance;
                component.block = <Block>{ id: 1 };
            });
        }));

        it("get block with one friend", (fakeAsync(() => {
            component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({api_content: { people: [{ identifier: "person1" }] }}));
            fixture.detectChanges();
            expect(component['blockService'].getBlock).toHaveBeenCalled();
            expect(component.profiles[0].identifier).toEqual("person1");
        })));

        it("render the noosfero profile-list", () => {
            expect(fixture.debugElement.queryAll(By.css("profile-list")).length).toEqual(1);
        });
    });
});
