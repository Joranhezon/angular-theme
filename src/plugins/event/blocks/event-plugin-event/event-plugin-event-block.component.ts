import { BlockService } from './../../../../app/services/block.service';
import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { Block } from '../../../../app/models/block.model';
import { Profile } from '../../../../app/models/profile.model';

@Component({
    selector: "noosfero-event-plugin-event-block",
    templateUrl: './event-plugin-event-block.html',
    styleUrls: ['./event-plugin-event-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class EventPluginEventBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;

    events: any;
    options: any;
    monthEvents: any;

    constructor(private blockService: BlockService) { }

    populateMonthEvents(month: number, year: number) {
        const events: any = [];
        this.events.forEach((e: any) => {
            const date: any = new Date(e.date);
            if (month === date.getMonth() && year === date.getFullYear()) {
                e.label = (("0" + date.getDate()).slice(-2)) + '/' + ("0" + (date.getMonth() + 1)).slice(-2) + ' ' + date.getHours() + ':' + date.getMinutes() + ' ' + e.title;
                events.push(e);
            }
        });
        this.monthEvents = events.slice();
    }

    ngOnInit() {
        this.events = [];
        this.monthEvents = [];

        this.options = {
            changeMonth(month: any, year: number) {
                this.populateMonthEvents(month.index, year);
            }
        };

        this.blockService.getBlock(this.block).subscribe((block: Block) => {
            this.events = block.api_content.events;
            const curr: any = new Date();
            this.populateMonthEvents(curr.getMonth(), curr.getFullYear());
        });
    }
}
