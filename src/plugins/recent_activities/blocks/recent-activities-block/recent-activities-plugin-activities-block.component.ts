import { BlockService } from './../../../../app/services/block.service';
import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { Block } from '../../../../app/models/block.model';

@Component({
    selector: "noosfero-recent-activities-plugin-activities-block",
    templateUrl: './recent-activities-plugin-activities-block.html',
    styleUrls: ['./recent-activities-plugin-activities-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class RecentActivitiesPluginActivitiesBlockComponent implements OnInit {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    profile: any;
    activities: any;

    constructor(private blockService: BlockService) { }

    // FIXME see if this code is needed
    getActivityTemplate(activity: any) {
        if (activity.label === 'events') {
            // return 'app/layout/blocks/recent-activities-plugin-activities/activities/event.html';
        } else {
            // return 'app/layout/blocks/recent-activities-plugin-activities/activities/' + activity.verb + '.html';
        }
    }

    ngOnInit() {
        this.profile = this.owner;
        this.activities = [];
        this.blockService.getBlock(this.block).subscribe((block: Block) => {
            this.activities = block.api_content.activities;
        });
    }
}
