import { BlockService } from './../../../../app/services/block.service';
import { ProfileImagesBlockComponent } from './profile-images-block.component';
import * as helpers from './../../../../spec/helpers';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NoosferoUrlPipe } from './../../../../app/shared/pipes/noosfero-url.pipe';
import { By } from '@angular/platform-browser';
import { Person } from '../../../../app/models/person.model';
import { Block } from '../../../../app/models/block.model';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {
    describe("Plugin Profile Images - Profile Images  Block Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<ProfileImagesBlockComponent>;
        let component: ProfileImagesBlockComponent;
        const person = <Person>{ id: 1 };

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ProfileImagesBlockComponent, NoosferoUrlPipe],
                providers: [
                    { provide: BlockService, useValue: mocks.blockService }
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(ProfileImagesBlockComponent);
                component = fixture.componentInstance;
                component.owner = person;
                component.block = <Block>{ id: 1 };
            });
        }));

        it("verify getBlock is called ", fakeAsync(() => {
            component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({}));
            component.ngOnInit();
            tick();
            expect(component['blockService'].getBlock).toHaveBeenCalledWith(component.block);
        }));

        it("set images getting content from api ", fakeAsync(() => {
            const images = [{ id: 1 }];
            component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({api_content: { images: images }}));
            component.ngOnInit();
            tick();
            expect(component.images).toBe(images);
        }));

        it("set profile on component initialization", () => {
            component.ngOnInit();
            expect(component.profile).toBe(person);
        });

        it("render the images defined on block", fakeAsync(() => {
            const images = [
                { id: 1, title: 'Test', view_url: { host: 'localhost', page: ['image'] }, path: '/articles/0000/0001/test.png' },
                { id: 2, title: 'Test', view_url: { host: 'localhost', page: ['image'] }, path: '/articles/0000/0002/test.png' },
                { id: 3, title: 'Test', view_url: { host: 'localhost', page: ['image'] }, path: '/articles/0000/0003/test.png' },
            ];
            component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({api_content: { images: images }}));
            component.ngOnInit();
            fixture.detectChanges();
            tick();
            fixture.detectChanges();
            expect(fixture.debugElement.queryAll(By.css(".image-block")).length).toEqual(3);
        }));
    });

});
