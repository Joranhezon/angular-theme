import { BlockService } from '../../../../app/services/block.service';
import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { Block } from '../../../../app/models/block.model';

@Component({
    selector: "noosfero-profile-images-plugin-profile-images-block",
    templateUrl: './profile-images-block.html',
    styleUrls: ['./profile-images-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileImagesBlockComponent implements OnInit {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    profile: any;
    images: any;

    constructor(private blockService: BlockService) { }

    ngOnInit() {
        this.profile = this.owner;
        this.images = [];
        this.blockService.getBlock(this.block).subscribe((block: Block) => {
            if (block.api_content && block.api_content.images) {
                this.images = block.api_content.images;
            }
        });
    }
}
