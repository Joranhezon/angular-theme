import { EventEmitter } from '@angular/core';
import { NoosferoKnownEvents } from './../app/known-events';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { Response, ResponseOptions, Headers } from '@angular/http';
import { Person } from '../app/models/person.model';
import { User } from '../app/models/user.model';
import { Profile } from '../app/models/profile.model';
import { Community } from '../app/models/community.model';
import { Environment } from '../app/models/environment.model';
import { Article } from '../app/models/article.model';
import { Task } from '../app/models/task.model';
import { Block } from '../app/models/block.model';
import { NgModel } from '@angular/forms';
import { Credentials } from '../app/models/credentials.model';

const DEBUG = false;

export function getMocks() {
    const mocks = {
        modalInstance: {
            close: () => { }
        },
        $modal: {
            open: (args: {}) => {
                return this.modalInstance;
            }
        },
        $transitions: {
            onSuccess: () => { }
        },
        $state: {
            href: () => { },
            go: () => { },
            transitionTo: (param: string) => { }
        },
        $stateParams: {
            href: () => { },
            go: () => { }
        },
        $scope: {
            $watch: (param: string, f: Function) => { f(); },
            $apply: () => { }
        },
        profile: {
            id: 1,
            identifier: 'profile-id',
            top_image: null
        },
        injector: {
            get: (obj: any) => ({ block: { identifier: 'identifier', settings: '', api_content: '' } })
        },
        community: {
            id: 1,
            closed: true,
            identifier: 'community-id',
            name: 'community-id'
        },
        person: <Person>{
            id: 1
        },
        user: <User>{
            id: 1,
            login: 'user',
            email: 'user@company.com',
        },
        popover: {
            hide: () => { }
        },
        taskService: {
            getAllPending: () => Observable.of({ headers: () => { }, body: {} }),
            closeTask: () => {},
            finishTask: (task: Task) => {
                return Observable.of({});
            },
            cancelTask: (task: Task) => {
                return Observable.of({});
            },
            get: (id: any) => {
                return Observable.of({ group_for_friend: 'group1' });
            },
            put: () => {
                return Observable.of({ group_for_friend: 'group1' });
            },
            taskClosed:
            {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
        },
        newPasswordConfirmation: {
            field: null,
            prefix: 'some',
            aditionalFields: [],
            translatorService: this.translatorService,
            underscorePipe: {},
            getErrors: [],
            getErrorKey: '',
            pushAditionalField: {},
            dasherize: {},
            getCompleteErrorKey: {},
            setBackendErrors: { },
            pushError: {},
        },
        authService: {
            currentUser: () => this.profile,
            currentPerson: () => this.profile,
            loginSuccess: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            loginFailed: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            logoutSuccess: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            login: (credentials: Credentials) => Observable.of(this.person),
            logout: () => Observable.of({}),
            logoutSuccessCallback: () => {},
            loginSuccessCallback: () => {},
            // FIXME remove this code
            // subscribe: (eventName: string, fn: Function) => {
            //     mocks.authService[eventName].subscribe(fn);
            // },
            isAuthenticated: () => { },
            forgotPassword: (value: string) => Observable.of({}),
            changePassword: () => Observable.of(this.person),
            createAccount: (user: User) => Observable.of(this.person),
            // FIXME remove this code
            // new_password: (param: any) => {
            //     return Promise.resolve({ status: 201 });
            // },
            newPassword: (code: string, password: string, password_confirmation: string) => {
                return Observable.of({});
            }
        },
        articleService: {
            subscribeToModelRemoved: (fn: Function) => { },
            subscribeToModelAdded: (fn: Function) => { },
            get: () => Observable.of({}),
            articleRemoved: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            articleCreated: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            modelRemovedEventEmitter:
            {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            modelAddedEventEmitter:
            {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            remove: (article: Article) => {
                return {
                    catch: (func?: Function) => {
                    }
                };
            },
            getByProfile: (profileId: number, params?: any) => Observable.of([]),
            getArticleByProfileAndPath: (profile: Profile, path: string) => {
                return {
                    then: (func?: Function) => {
                        if (func) {
                            func({
                                data: {
                                    article: null
                                }
                            });
                        }
                    }
                };
            },
            getChildren: (articleId: number, params?: any) => {
                return {
                    then: (func?: Function) => { if (func) func(); }
                };
            },
            setCurrent: (article: Article) => { },
            getCurrent: () => Observable.of({}),
            search: (filter: any) => Observable.of({}),
            createInParent: () => { },
            updateArticle: () => { },
        },
        environmentService: {
            getEnvironmentPeople: (params: any) => {
                return mocks.promiseResultTemplate({
                    people: {}
                });
            },
            allowChangeIdentifier: () => false,
            getCurrentEnvironment: (): any => {
                return Observable.of({
                    id: 1,
                    settings: {},
                    layout_template: '',
                    signup_intro: 'Welcome to Noosfero',
                    host: 'http://localhost'
                });
            },
            getBlockPreview: (id: any, type: string) => Observable.of( {api_content: [] }),
            update: (environment: Environment) => Observable.of({ id: 2 }),
            get: (environment: string) => Observable.of({id: 2}),
            getBoxes: () => Observable.of({}),
            environmentChangeEvent: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
        },
        profileService: {
            getCurrentProfile: () => mocks.profile,
            getMembersByRole: () => Observable.of(new Response(new ResponseOptions({status: 200, body: "blablablabla", headers: new Headers({total: 10})}))),
            instant: () => { },
            update: (profile: Profile) => Observable.of(mocks.profile),
            remove: () => Observable.of({success: true}),
            getBlockPreview: (id: any, type: string) => Observable.of({api_content: []}),
            getTags: () => { },
            getHomePage: () => {},
            getNetworkActivities: () => {},
            isMember: () => Observable.of(true),
            addMember: () => Observable.of({}),
            removeMember: () => Observable.of({}),
            getBoxes: () => Observable.of({}),
            profileImageChangeEvent: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            profileChangeEvent: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
        },
        personService: {
            search: () => Observable.of([mocks.profile]),
            getFriendshipState: () => Observable.of({ }),
            addFriend: () => Observable.of({}),
            removeFriend: () => Observable.of({}),
            getCommunities: () => {
                return Observable.of({
                    headers: {
                        get: () => ({ total: 1 })
                    },
                    body: [{id: 1}] });
            },
            getFriends: () => {
                return Observable.of({
                    headers: {
                        get: () => ({ total: 1 })
                    },
                    body: [{id: 1}] });
            },
            getLoggedPerson: () => Observable.of({})
        },
        communityService: {
            sendInvitations: (communityId: number, people: Person[]) => Observable.of({ success: true }),
            createNewCommunity: (community: Community) => Observable.of({}),
            getMembershipState: () => Observable.of({ })
        },
        sessionService: {
            currentUser: () => <User>{ person: { id: 1, identifier: 'test_user' } },
            currentPerson: () => <Person>{ id: 1 },
            destroy: () => {}
        },
        commentService: {
            commentRemoved: Observable.of({}),
            commentUpdated: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            commentCreated: {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            subscribeToModelRemoved: (fn: Function) => { },
            subscribeToModelAdded: (fn: Function) => { },
            modelRemovedEventEmitter:
            {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            modelAddedEventEmitter:
            {
                subscribe: (fn: Function) => { },
                next: (param: any) => { }
            },
            getByArticle: (article: Article) => {
                return Observable.of({
                    headers: {
                        get: () => ({ total: 1 })
                    },
                    body: [{id: 1}] });
            },
            createInArticle: (article: Article, comment: Comment) => {
                return Observable.of({});
            },
        },
        sessionWithCurrentUser: (user: any) => {
            return {
                currentUser: () => user,
                localStorage: {}
            };
        },
        inDesignMode: false,
        designModeService: {
            onToggle: new EventEmitter(),
            isInDesignMode: () => mocks.inDesignMode,
            setInDesignMode: (value: boolean) => { mocks.inDesignMode = value; }
        },
        translateService: {
            use: (lang?: string) => {
                return lang ? Promise.resolve(lang) : "en";
            },
            getDefaultLang: () => {
                return "en";
            },
            instant: (text: string) => text,
            getBrowserLang: () => 'en',
            setDefaultLang: (lang: string) => { },
        },
        tmhDynamicLocale: {
            get: () => { },
            set: (lang: string) => { }
        },
        amMoment: {
            changeLocale: () => { }
        },
        promiseResultTemplate: (response?: {}) => {
            const callback = (func?: (response: any) => any) => {
                if (func) {
                    const ret = func(response);
                    if (ret && typeof ret.then === "function") return ret;
                }
                return mocks.promiseResultTemplate(response);
            };
            return {
                then: callback,
                finally: callback,
                catch: callback
            };
        },
        translatorService: {
            currentLanguage: () => { },
            changeLanguage: (lang: string) => { },
            translate: (text: string) => text,
            hasTranslation: (text: string) => text
        },
        notificationService: {
            success: () => { },
            confirmation: () => { },
            info: () => { },
            error: () => { }
        },
        blockService: {
            getBlock: (block: Block) => Observable.of({}),
            uploadImages: () => Observable.of({}),
        },
        settingsService: {
            getAvailableBlocks: () => { }
        },
        noosferoTemplateFilter: (text: string, options: any) => {
            return text;
        },
        stateService: {
            transitionTo: () => { }
        },
        eventsHubService: {
            subscribeToEvent: () => { },
            emitEvent: () => { },
            knownEvents: new NoosferoKnownEvents()
        },
        permissionService: {
            isAllowed: () => true
        },
        amParseFilter: () => {
            return {
                toISOString: () => { }
            };
        },
        localStorageService: {
            storage: {},
            clear: (key: string) => { delete mocks.localStorageService.storage[key]; },
            retrieve: (key: string) => mocks.localStorageService.storage[key],
            store: (key: string, value: any) => { mocks.localStorageService.storage[key] = value; }
        },
        themeService: {
            verifyTheme: (theme: string) => { }
        },
        roleService: {
            getByProfile: (profileId: number, params: any = {}) =>  Observable.of([{}])
        },
        headerService: {
            setEnvironmentTitle: () => {}
        },
        route: {
            snapshot: { data: {}, queryParams: {}, params: {} },
            parent: {
                snapshot: { data: {}, queryParams: {}, params: {} }
            }
        },
        window: {
            location: {
                reload: () => {},
                pathname: ""
            }
        },
        router: {
            navigate: () => {}
        },
        location: {
            back: () => {}
        },
        bodyStateClassesService: {
            start: () => {},
            changeClasses: new EventEmitter()
        },
        jsonp: {
            get: () => Observable.of({})
        }
    };
    return mocks;
};
