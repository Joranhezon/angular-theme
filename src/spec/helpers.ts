import { MockBackend, MockConnection } from '@angular/http/testing';
import { ResponseOptions, Response, Http, Headers, RequestOptions, URLSearchParams, Request, RequestMethod, HttpModule, BaseRequestOptions } from "@angular/http";
import { Person } from '../app/models/person.model';

export { getMocks } from "./mocks";

export let fixtures = {
    user: {
        id: 1,
        login: 'user',
        email: 'user@company.com',
        person: <Person>{
            id: 1,
            identifier: 'user'
        },
        private_token: 'token',
        userRole: 'admin',
        permissions: []
    }
};
