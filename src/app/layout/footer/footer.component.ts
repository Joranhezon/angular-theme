import { Component, Inject, ViewEncapsulation } from '@angular/core';

import { AuthService, SessionService } from './../../services';
import { User } from '../../models/user.model';

@Component({
    selector: "noosfero-footer",
    templateUrl: './footer.html',
    styleUrls: ['./footer.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FooterComponent {

    private currentUser: User;

    constructor(private authService: AuthService, private session: SessionService) {
      this.currentUser = this.session.currentUser();
    }

    logout() {
        this.authService.logout(this.currentUser).subscribe(() => {
            this.authService.logoutSuccessCallback(this.currentUser);
        });
    };
}
