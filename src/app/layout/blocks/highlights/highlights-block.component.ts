import { Input, Inject, Component, ViewEncapsulation, OnInit, EventEmitter, Output } from '@angular/core';
import * as _ from "lodash";
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: 'noosfero-highlights-block',
    templateUrl: './highlights-block.html',
    styleUrls: ['./highlights-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HighlightsBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;
    @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    images: any;

    ngOnInit() {
        this.images = (<any>this.block.api_content || {}).slides || [];
        if (!this.block.settings) {
            this.block.settings = <any>{};
        }
        if ((<any>this.block.settings).shuffle) {
            this.images = _.shuffle(this.images);
        }
        this.block.hide = (<any>this.images == null || <any>this.images.length === 0);
        if (!this.block['active']) {
            this.block['active'] = 0;
        }
        this.canDisplayOnPreviewChanged.emit(this.canDisplayOnPreview());
    }

    canDisplayOnPreview() {
        return !_.isNil(this.block.id);
    }

    getTarget(image: any) {
        if (image.new_window) {
            return "_blank";
        }
        return "";
    }

    getTransitionInterval() {
        if (this.designMode) {
            return 0;
        }
        return (<any>this.block.settings).interval * 1000;
    }

    hideControls() {
        return !(<any>this.block.settings).navigation;
    }

    updateLink(i: number, item: any) {
        this.images[i].title = item.name;
        this.images[i].address = item.address;
    }

    hasSlide() {
        return !_.isEmpty(this.images);
    }

    isPreview() {
        return !_.isInteger(this.block.id);
    }
}
