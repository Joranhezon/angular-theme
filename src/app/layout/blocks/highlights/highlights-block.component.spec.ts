import { HighlightsBlockComponent } from './highlights-block.component';
import * as helpers from '../../../../spec/helpers';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CarouselModule } from 'ngx-bootstrap';
import { By } from '@angular/platform-browser';
import { TranslatorService } from '../../../shared/services/translator.service';
import { TranslateModule } from '@ngx-translate/core';

describe("Highlights Block Component", () => {

    const mocks = helpers.getMocks();
    let fixture: ComponentFixture<HighlightsBlockComponent>;
    let component: HighlightsBlockComponent;

    beforeEach(async(() => {

        TestBed.configureTestingModule({
            declarations: [HighlightsBlockComponent],
            providers: [
                { provide: TranslatorService, useValue: mocks.translatorService },
            ],
            imports: [TranslateModule.forRoot()],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(HighlightsBlockComponent);
            component = fixture.componentInstance;
            component.block = <any>{ settings: { interval: 2, shuffle: true } };
        });
    }));

    it("link target should be empty when new_window is false", () => {
        expect(component.getTarget({ new_window: false })).toEqual("");
    });

    it("link target should be _blank when new_window is true", () => {
        expect(component.getTarget({ new_window: true })).toEqual("_blank");
    });

    it("return transition interval in miliseconds", () => {
        expect(component.getTransitionInterval()).toEqual(2000);
    });

    it("not render highlights block if there is no image", () => {
        fixture.detectChanges();
        expect(component.block.hide).toBeTruthy();
    });

    it("render highlights block if there are images on block", () => {
        component.block.api_content = { slides: [{ id: 1 }] };
        component.ngOnInit();
        expect(component.block.hide).toBeFalsy();
    });

    it("not render highlights block if images array is empty", () => {
        component.block.api_content = { slides: [] };
        component.ngOnInit();
        expect(component.block.hide).toBeTruthy();
    });

    it("transition interval should be zero in design mode", () => {
        (<any>component.block.settings).interval = 5;
        component.designMode = true;
        expect(component.getTransitionInterval()).toEqual(0);
    });

    it("return transition interval in miliseconds when not in design mode", () => {
        (<any>component.block.settings).interval = 5;
        component.designMode = false;
        expect(component.getTransitionInterval()).toEqual(5000);
    });

    it("hasSlide be false if images array is empty", () => {
        component.block.api_content = { slides: [] };
        component.ngOnInit();
        expect(component.hasSlide()).toBeFalsy();
    });

    it("hasSlide be true if images array is not empty", () => {
        component.block.api_content = { slides: [{ id: 1 }] };
        component.ngOnInit();
        expect(component.hasSlide()).toBeTruthy();
    });

    it("isPreview be false if the block id is null", () => {
        component.block = <any>{ id: null, settings: { interval: 2, shuffle: true } };
        expect(component.isPreview()).toBeTruthy();
    });

    it("isPreview be true if the block id is not null", () => {
        component.block = <any>{ id: 1, settings: { interval: 2, shuffle: true } };
        expect(component.isPreview()).toBeFalsy();
    });

    it("not display highlights-block on preview mode", () => {
        component.block = <any>{ id: null, settings: { interval: 2, shuffle: true } };
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('.highlights-block')).length).toEqual(0);
    });

    it("display highlights-block-preview on preview mode", () => {
        component.block = <any>{ id: null, settings: { interval: 2, shuffle: true } };
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('.highlights-block-preview')).length).toEqual(1);
    });

    it("display message in highlights block if has no slide", () => {
        component.block = <any>{ id: 1, api_content: { slides: [] } };
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('.highlights-block .message')).length).toEqual(1);
    });


    it("return false in canDisplayOnPreview when the block has no id", () => {
        expect(component.canDisplayOnPreview()).toEqual(false);
    });

});
