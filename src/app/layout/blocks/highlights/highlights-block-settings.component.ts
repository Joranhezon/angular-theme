import { Input, Inject, Component, OnInit, forwardRef, Injector, ViewEncapsulation } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { BlockSettingsComponent } from '../block-settings.component';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { Environment } from '../../../models/environment.model';
import * as _ from "lodash";

@Component({
    selector: "noosfero-highlights-block-settings",
    templateUrl: './highlights-block-settings.html',
    styleUrls: ['./highlights-block-settings.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class HighlightsBlockSettingsComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile | Environment;

    isCollapsed: any;
    images: any;
    bagId: any;

    constructor(
        injector: Injector,
        private blockService: BlockService) {
    }

    ngOnInit() {
        this.bagId = 'highlights-block-bag-' + this.block.id;
        this.isCollapsed = true;
        this.images = (<any>this.block.api_content || {}).slides || [];
        (<any>this.block.settings).block_images = this.images;
    }

    addSlide() {
        this.images.push({ image_src: "", title: "", address: "http://" });
        this.block.hide = false;
    }

    removeSlide(index: number) {
        this.images.splice(index, 1);
    }

    selectSlide(index: number) {
        (<any>this.block)['active'] = index;
    }

    upload(data: any, slide: any) {
        this.blockService.uploadImages(this.block, [data]).subscribe((block: Block) => {
            this.block.images = block.images;
            if (!_.isNil(this.block.images) && (this.block.images.length > 0)) {
                const image = this.block.images[this.block.images.length - 1];
                slide.image_id = image.id;
                slide.image_src = image.url;
            }
        });
    }

    getInterval() {
        return this.block &&  this.block.settings &&  this.block.settings['interval'] ? this.block.settings['interval'] : 0;
    }
}
