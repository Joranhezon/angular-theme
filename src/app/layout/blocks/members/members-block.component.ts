import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: "noosfero-members-block",
    templateUrl: './members-block.html',
    styleUrls: ['./members-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class MembersBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;
    public loading = false;

    profiles: any = [];
    constructor(private blockService: BlockService) { }

    ngOnInit() {
        this.loading = true;
        const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;
        if (this.block.api_content) {
            this.profiles =  this.block.api_content['people'];
            this.loading = false;
        } else {
            this.blockService.getBlock(this.block).subscribe((block: Block) => {
                this.block.api_content = block.api_content;
                this.profiles = this.block.api_content.people;
                this.loading = false;
            });
        }
    }
}
