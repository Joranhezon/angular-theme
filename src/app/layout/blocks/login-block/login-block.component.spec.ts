import { TranslateModule } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { MomentModule } from 'angular2-moment';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';

import { TranslatorService } from './../../../shared/services/translator.service';
import { SessionService } from './../../../services/session.service';
import { DateFormatPipe } from './../../../shared/pipes/date-format.pipe';
import {LoginBlockComponent} from './login-block.component';
import * as helpers from './../../../../spec/helpers';
import { AuthService, AuthEvents } from './../../../services';
import { User } from '../../../models/user.model';
import { Credentials } from '../../../models/credentials.model';

const htmlTemplate = '<noosfero-login-block></noosfero-login-block>';

describe("Components", () => {

    describe("Login Block Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<LoginBlockComponent>;
        let component: LoginBlockComponent;

        const user = <User>{ person: {id: 1} };

        beforeEach(async(() => {
            spyOn(mocks.authService, 'currentUser').and.returnValue(user);
            spyOn(mocks.authService, 'loginSuccessCallback').and.callThrough();
            spyOn(mocks.authService, 'logoutSuccessCallback').and.callThrough();
            spyOn(mocks.authService, 'login').and.returnValue(Observable.of(user));
            spyOn(mocks.authService, 'logout').and.callThrough();

            TestBed.configureTestingModule({
                declarations: [LoginBlockComponent, DateFormatPipe],
                providers: [
                    { provide: SessionService, useValue: mocks.sessionService },
                    { provide: AuthService, useValue: mocks.authService },
                    { provide: TranslatorService, useValue: mocks.translatorService }
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA],
                imports: [FormsModule, MomentModule, TranslateModule.forRoot()]
            });
            fixture = TestBed.createComponent(LoginBlockComponent);
            component = fixture.componentInstance;
        }));

        it("expect call login auth service on login", () => {
            component.credentials = <Credentials>{login: 'some', password: '123456'}
            fixture.detectChanges()
            component.login();
            expect(mocks.authService.login).toHaveBeenCalledWith(component.credentials);
        });

        it("expect call loginSuccessCallback on login", () => {
            component.login();
            expect(mocks.authService.loginSuccessCallback).toHaveBeenCalledWith(user);
        });

        it("expect call logout autho service on logout", () => {
            component.logout();
            expect(mocks.authService.logout).toHaveBeenCalledWith(user);
        });

        it("expect call logoutSuccessCallback on logout", () => {
            component.logout();
            expect(mocks.authService.logoutSuccessCallback).toHaveBeenCalledWith(user);
        });

        it("expect current user be the current user of auth service", () => {
            expect(component.currentUser).toEqual( mocks.authService.currentUser());
        });

    });

});
