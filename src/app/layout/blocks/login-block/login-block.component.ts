import { Input, Inject, Component, ViewEncapsulation } from '@angular/core';

import { AuthService, AuthEvents } from '../../../services';
import { SessionService } from '../../../services/session.service';
import { User } from '../../../models/user.model';
import { Credentials } from '../../../models/credentials.model';
import { DefaultResponse } from '../../../models/default-response.model';

@Component({
    selector: "noosfero-login-block",
    templateUrl: './login-block.html',
    styleUrls: ['./login-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LoginBlockComponent {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    currentUser: User;

    credentials = <Credentials>{};

    constructor(public authService: AuthService) {

        this.currentUser = this.authService.currentUser();
        this.authService.loginSuccess.subscribe((user: User) => {
            this.currentUser = user;
        });
        this.authService.logoutSuccess.subscribe(() => {
            this.currentUser = null;
        });
    }

    login() {
        this.authService.login(this.credentials).subscribe((user: User) => {
            this.authService.loginSuccessCallback(user);
        });
    }

    logout() {
        this.authService.logout(this.currentUser).subscribe(() => {
            this.authService.logoutSuccessCallback(this.currentUser);
        });
    };
}
