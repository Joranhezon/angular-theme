import { BlockService } from './../../../services/block.service';
import { ProfileImageComponent } from './../../../profile/image/profile-image.component';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PeopleBlockComponent } from './people-block.component';
import * as helpers from '../../../../spec/helpers';
import { Block } from '../../../models/block.model';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {

    describe("People Block Component", () => {
        let fixture: ComponentFixture<PeopleBlockComponent>;
        let component: PeopleBlockComponent;
        const mocks = helpers.getMocks();


        beforeEach(async(() => {
            spyOn(mocks.blockService, "getBlock").and.returnValue(Observable.of({api_content: {people: [{ identifier: "person1" }]}}));

            TestBed.configureTestingModule({
                declarations: [PeopleBlockComponent],
                providers: [
                    { provide: BlockService, useValue: mocks.blockService },
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(PeopleBlockComponent);
                component = fixture.componentInstance;
                component.block = <Block>{ id: 1 };
            });
        }));

        it("get block with one person", (fakeAsync(() => {
            fixture.detectChanges();
            tick();
            expect(TestBed.get(BlockService).getBlock).toHaveBeenCalled();
            expect(component.profiles[0].identifier).toEqual("person1");
        })));

        it("render the noosfero profile-list", () => {
            expect(fixture.debugElement.queryAll(By.css("profile-list")).length).toEqual(1);
        });
    });
});
