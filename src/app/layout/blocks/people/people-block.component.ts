import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Environment } from '../../../models/environment.model';

@Component({
    selector: "noosfero-people-block",
    templateUrl: './people-block.html',
    styleUrls: ['./people-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PeopleBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Environment;
    @Input() designMode: boolean;

    profiles: any = [];

    constructor(private blockService: BlockService) { }

    ngOnInit() {
        const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;
        this.blockService.getBlock(this.block).subscribe((block: Block) => {
            this.block.api_content = block.api_content;
            this.profiles = this.block.api_content.people;
        });
    }
}
