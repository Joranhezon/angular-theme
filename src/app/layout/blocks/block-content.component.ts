import { Input, Inject, Component, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'noosfero-block-content',
    templateUrl: './block-content.html'
})
export class BlockContentComponent {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;
    @Output() canDisplayOnPreviewChanged: EventEmitter<boolean> = new EventEmitter<boolean>();

    canDisplayOnPreviewChangedOnChild(canDisplayOnPreview: boolean) {
        this.canDisplayOnPreviewChanged.emit(canDisplayOnPreview);
    }

}
