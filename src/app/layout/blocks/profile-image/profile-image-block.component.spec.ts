import { BlockService } from './../../../services/block.service';
import { ProfileImageBlockComponent } from './profile-image-block.component';
import * as helpers from './../../../../spec/helpers';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { Person } from '../../../models/person.model';
import { Block } from '../../../models/block.model';

describe("Components", () => {
    describe("Profile Image  Block Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<ProfileImageBlockComponent>;
        let component: ProfileImageBlockComponent;
        const person = <Person>{ id: 1, identifier: 'some' };

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [ProfileImageBlockComponent],
                providers: [
                    { provide: BlockService, useValue: mocks.blockService }
                ],
                schemas: [NO_ERRORS_SCHEMA]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(ProfileImageBlockComponent);
                component = fixture.componentInstance;
                component.owner = person;
                component.block = <Block>{ id: 1 };
            });
        }));

        it("render the images defined on block", () => {
            fixture.detectChanges();
            expect(fixture.debugElement.queryAll(By.css("noosfero-profile-image")).length).toEqual(1);
        });
    });

});
