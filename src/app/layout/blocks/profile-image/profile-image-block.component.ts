import { ProfileImageComponent } from './../../../profile/image/profile-image.component';
import { Component, Inject, Input, ViewEncapsulation } from '@angular/core';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: "noosfero-profile-image-block",
    templateUrl: './profile-image-block.html',
    styleUrls: ['./profile-image-block.scss'],
    encapsulation: ViewEncapsulation.None,

})
export class ProfileImageBlockComponent {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;

}
