import { Component, Input, Inject, OnChanges, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { DragulaService } from 'ng2-dragula';

@Component({
    selector: "noosfero-link-list-block",
    templateUrl: './link-list-block.html',
    styleUrls: ['./link-list-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LinkListBlockComponent implements OnChanges, OnInit, OnDestroy {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    links: any;
    newIndex = -1;

    bagId: any;

    constructor(private dragulaService: DragulaService) { }

    ngOnInit() {
        this.bagId = 'link-list-block-bag-' + this.block.id;
        this.dragulaService.setOptions(this.bagId, {
            invalid: () => {
                return !this.designMode;
            },
            moves: (el, container, handle) => {
                return this.designMode;
            }
        });
        if (!this.block.settings) this.block.settings = {};
        if (!this.block.settings.links) this.block.settings.links = [];
        this.links = this.block.settings.links;
        this.applyVisibility();
    }


    updateLink(i: number, item: any) {
        this.links[i].name = item.name;
        this.links[i].address = item.address;
    }


    updateIcon(i: number, icon: string) {
        this.links[i].icon = icon;
    }

    addLink() {
        this.links.push({ name: "", address: "http://", icon: "fa-file-o" });
        this.newIndex = this.links.length - 1;
    }

    isNewLink(index: number) {
        return index === this.newIndex;
    }

    removeLink(index: number) {
        this.links.splice(index, 1);
    }

    ngOnChanges() {
        this.applyVisibility();
    }

    applyVisibility() {
        if (this.links) {
            this.block.hide = this.links.length < 1;
        }
    }

    ngOnDestroy() {
        this.dragulaService.destroy(this.bagId);
    }


}
