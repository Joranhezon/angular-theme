import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { BlockService } from '../../../services/block.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: "noosfero-communities-block",
    templateUrl: './communities-block.html',
    styleUrls: ['./communities-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CommunitiesBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;

    profiles: any = [];

    constructor(private blockService: BlockService) { }

    ngOnInit() {
        const limit: number = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 4;
        if (this.block.api_content) {
          this.profiles =  this.block.api_content['communities'];
        } else {
            this.blockService.getBlock(this.block).subscribe((block: Block) => {
                this.profiles = block.api_content['communities'];
                this.block.api_content = block.api_content;
            });
        }
    }
}
