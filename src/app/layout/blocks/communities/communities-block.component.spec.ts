import { BlockService } from './../../../services/block.service';
import { ProfileImageComponent } from './../../../profile/image/profile-image.component';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommunitiesBlockComponent } from './communities-block.component';
import * as helpers from '../../../../spec/helpers';
import { Block } from '../../../models/block.model';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {

    describe("Community Block Component", () => {
        let fixture: ComponentFixture<CommunitiesBlockComponent>;
        let component: CommunitiesBlockComponent;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            spyOn(mocks.blockService, "getBlock").and.returnValue(Observable.of({api_content: { communities: [{ identifier: "community1" }]}}));

            TestBed.configureTestingModule({
                declarations: [CommunitiesBlockComponent],
                providers: [
                    { provide: BlockService, useValue: mocks.blockService },
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            }).compileComponents().then(() => {
                fixture = TestBed.createComponent(CommunitiesBlockComponent);
                component = fixture.componentInstance;
                component.block = <Block>{ id: 1 };
            });
        }));

        it("get block with one community", (fakeAsync(() => {
            fixture.detectChanges();
            tick();
            expect(TestBed.get(BlockService).getBlock).toHaveBeenCalled();
            expect(component.profiles[0].identifier).toEqual("community1");
        })));

        it("render the noosfero profile-list", () => {
            expect(fixture.debugElement.queryAll(By.css("profile-list")).length).toEqual(1);
        });
    });
});
