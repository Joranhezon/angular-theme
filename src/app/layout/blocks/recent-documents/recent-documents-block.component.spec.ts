import { BlockService } from './../../../services/block.service';
import { ArticleService } from './../../../services/article.service';
import { DateFormatPipe } from './../../../shared/pipes/date-format.pipe';
import { RecentDocumentsBlockComponent } from './recent-documents-block.component';
import * as helpers from './../../../../spec/helpers';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NgPipesModule } from 'ngx-pipes';
import { MomentModule } from 'angular2-moment';
import { By } from '@angular/platform-browser';
import { Article } from '../../../models/article.model';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {
    describe("Recent Documents Block Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<RecentDocumentsBlockComponent>;
        let component: RecentDocumentsBlockComponent;
        const article = <Article>{ name: "article1" };

        beforeEach(async(() => {
            spyOn(mocks.articleService, 'articleRemoved');
            spyOn(mocks.blockService, 'getBlock').and.callThrough();

            TestBed.configureTestingModule({
                declarations: [RecentDocumentsBlockComponent, DateFormatPipe],
                providers: [
                    { provide: BlockService, useValue: mocks.blockService },
                    { provide: "$state", useValue: mocks.$state },
                    { provide: ArticleService, useValue: mocks.articleService },
                    { provide: "amParseFilter", useValue: mocks.amParseFilter }
                ],
                schemas: [NO_ERRORS_SCHEMA],
                imports: [NgPipesModule, MomentModule]
            });
            fixture = TestBed.createComponent(RecentDocumentsBlockComponent);
            component = fixture.componentInstance;
            component.block = <Block>{ id: 1 };
            component.owner = <Profile>{ identifier: 'identifier' };
        }));

        it("verify getBlock is called ", fakeAsync(() => {
            component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({api_content: {}}));
            component.ngOnInit();
            tick();
            expect(component['blockService'].getBlock).toHaveBeenCalledWith(component.block);
        }));

        it("set documents getting content from api ", fakeAsync(() => {
            const articles = [<Article>{ id: 1 }];
            component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({api_content: {articles: articles}}));
            component.ngOnInit();
            tick();
            expect(component.documents).toBe(articles);
        }));

        // FIXME see a way to see if article removed event was registered
        // fit("verify articleRemoved is called", fakeAsync(() => {
        //     component['blockService'].getBlock = jasmine.createSpy("getBlock").and.returnValue(Observable.of({api_content: {}}));

        //     tick();
        //     component.ngOnInit();
        //     expect(component['articleService'].articleRemoved).toHaveBeenCalled();
        // }));

        // FIXME put this test to works
        // it("verify removed article has been removed from list", fakeAsync(() => {
        //     let articles = [article];
        //     component['blockService'].getApiContent = jasmine.createSpy("getApiContent").and.returnValue(Promise.resolve({ articles: articles }));
        //     component.ngOnInit();
        //     tick();
        //     expect(component.documents.length).toEqual(1);
        //     simulateRemovedEvent(component);
        //     expect(component.documents.length).toEqual(0);
        // }));
        // /**
        //  * Simulate the ArticleService ArticleEvent.removed event
        //  */
        // function simulateRemovedEvent(recentDocumentsBlock: RecentDocumentsBlockComponent) {
        //     recentDocumentsBlock['articleService']['modelRemovedEventEmitter'].next(article);
        // }

    });

});
