import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';

import {BlockService} from '../../../services/block.service';
import {ArticleService} from './../../../services/article.service';
import { Article } from '../../../models/article.model';
import * as _ from "lodash";
import { Block } from '../../../models/block.model';

@Component({
    selector: "noosfero-recent-documents-block",
    templateUrl: './recent-documents-block.html',
    styleUrls: ['./recent-documents-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class RecentDocumentsBlockComponent implements OnInit {

    @Input() block: any;
    @Input() owner: any;
    @Input() designMode: boolean;

    profile: any;
    documents: Article[];
    public loading = false;

    constructor(private blockService: BlockService,
        private articleService: ArticleService) { }


    ngOnInit() {
        this.profile = this.owner;
        this.documents = [];
        if (this.block.api_content) {
            this.documents = this.block.api_content.articles;
        } else {
            this.loading = true;
            this.blockService.getBlock(this.block).subscribe((block: Block) => {
                this.documents = block.api_content.articles;
                this.loading = false;
            });
        }

        this.articleService.articleRemoved.subscribe((article: Article) => {
            _.reject(this.documents, article);
        });
    }
}
