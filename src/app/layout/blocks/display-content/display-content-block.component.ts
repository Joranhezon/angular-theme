import { Input, Inject, Component, ViewEncapsulation, OnInit } from '@angular/core';

import { ArticleService } from '../../../services/article.service';
import { Block } from '../../../models/block.model';
import { Profile } from '../../../models/profile.model';
import { Article } from '../../../models/article.model';
import { Section } from '../../../models/section.model';

@Component({
    selector: "noosfero-display-content-block",
    templateUrl: './display-content-block.html',
    styleUrls: ['./display-content-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class DisplayContentBlockComponent implements OnInit {

    @Input() block: Block;
    @Input() owner: Profile;
    @Input() designMode: boolean;

    profile: Profile;
    articles: Article[];
    sections: Section[];

    documentsLoaded = false;

    constructor(private articleService: ArticleService) { }

    ngOnInit() {
        this.profile = this.owner;
        const limit = ((this.block && this.block.settings) ? this.block.settings.limit : null) || 5;
        this.articleService.getByProfile(this.profile, { per_page: limit })
            .subscribe((articles: Article[]) => {
                this.articles = articles;
                this.sections = this.block.settings.sections || [<Section>{ value: 'title' }, <Section>{ value: 'publish_date' }, <Section>{ value: 'abstract' }];
                this.documentsLoaded = true;
            });
    }

    /**
     * Returns whether a settings section should be displayed.
     *
     */
    private display(sectionName: string): boolean {
        const section: Section = this.sections.find( s => {
            return s.value === sectionName;
        });
        return section !== undefined && section.checked !== undefined;
    }

}
