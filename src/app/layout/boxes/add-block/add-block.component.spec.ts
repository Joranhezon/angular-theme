import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from './../../../shared/services/translator.service';
import { EnvironmentService } from './../../../services/environment.service';
import { ProfileService } from './../../../services/profile.service';
import { SettingsService } from './../../../services/settings.service';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { NgPipesModule } from 'ngx-pipes';
import { AddBlockComponent } from './add-block.component';
import * as helpers from '../../../../spec/helpers';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Block } from '../../../models/block.model';

describe("Components", () => {
    describe("Add Block Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<AddBlockComponent>;
        let component: AddBlockComponent;

        beforeEach(async(() => {
            spyOn(mocks.settingsService, 'getAvailableBlocks').and.returnValue(Observable.of([]));
            TestBed.configureTestingModule({
                declarations: [AddBlockComponent],
                providers: [
                    { provide: SettingsService, useValue: mocks.settingsService },
                    { provide: ProfileService, useValue: mocks.profileService },
                    { provide: EnvironmentService, useValue: mocks.environmentService },
                    { provide: TranslatorService, useValue: mocks.translatorService }
                ],
                schemas: [NO_ERRORS_SCHEMA],
                imports: [NgPipesModule, ModalModule.forRoot(), FormsModule, TranslateModule.forRoot()]
            });
            fixture = TestBed.createComponent(AddBlockComponent);
            component = fixture.componentInstance;
        }));

        it("load available blocks", () => {
            component.loadAvailableBlocks();
            expect(TestBed.get(SettingsService).getAvailableBlocks).toHaveBeenCalled();
        });

        it("emit event when add block", fakeAsync(() => {
            spyOn(component.onAdd, 'emit');
            TestBed.get(ProfileService).getBlockPreview = jasmine.createSpy('getBlockPreview').and.returnValue(Observable.of({ api_content: [] }));
            component.owner = <any>{id: 54, type: 'Person'};
            fixture.detectChanges();
            component.addBlock(<Block>{ type: 'RecentDocumentsBlock'});
            tick();
            expect(TestBed.get(ProfileService).getBlockPreview).toHaveBeenCalledWith(component.owner, 'RecentDocumentsBlock');
            expect(component.onAdd.emit).toHaveBeenCalled();
        }));

        it("filter blocks by whitelist when load available blocks", fakeAsync(() => {
            TestBed.get(SettingsService).getAvailableBlocks = jasmine.createSpy("getAvailableBlocks").and.returnValue(Observable.of([
                    {type: 'RawHTMLBlock'},
                    {type: 'OtherInvalidBlock'},
                ]
            ));
            component.loadAvailableBlocks();
            tick();
            expect(component.blocks.length).toEqual(1);
        }));

        it("permission action have allow_edit_design", () => {
            expect(component.permissionAction).toContain('allow_edit_design');
        });

        it("permission action have edit_environment_design", () => {
            expect(component.permissionAction).toContain('edit_environment_design');
        });

        it("isProfile return true for person profile", () => {
            component.owner = <any>{id: 1, type: 'Person'};
            expect(component.isProfile()).toBeTruthy();
        });

        it("isProfile return true for community profile", () => {
            component.owner = <any>{id: 1, type: 'Community'};
            expect(component.isProfile()).toBeTruthy();
        });

        it("isProfile return false for environment", () => {
            component.owner = <any>{id: 1, type: 'Environment'};
            expect(component.isProfile()).toBeFalsy();
        });

        it("getOwnerService return profile service for profile owner", () => {
            component.owner = <any>{id: 1, type: 'Person'};
            expect(mocks.profileService).toEqual(component['getOwnerService']());
        });

        it("getOwnerService return profile service for environment owner", () => {
            component.owner = <any>{id: 1, type: 'Environment'};

            expect(mocks.environmentService).toEqual(component['getOwnerService']());
        });

    });
});
