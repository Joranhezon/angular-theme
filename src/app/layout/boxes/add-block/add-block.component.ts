import { SettingsService } from './../../../services/settings.service';
import { Inject, Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import * as _ from "lodash";

import { ProfileService } from '../../../services/profile.service';
import { EnvironmentService } from '../../../services/environment.service';
import { Block } from '../../../models/block.model';
import { Box } from '../../../models/box.model';
import { Profile } from '../../../models/profile.model';
import { Environment } from '../../../models/environment.model';
import { BlockDefinition } from '../../../models/block-definition.model';


@Component({
    selector: "add-block",
    templateUrl: './add-block.html',
    styleUrls: ['./add-block.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AddBlockComponent {

    @Input() box: Box;
    @Input() owner: Profile | Environment;
    @Output() onAdd = new EventEmitter<Block>();
    permissionAction = "allow_edit_design,edit_environment_design";
    public loading = false;

    search: string;
    whitelist = ['RawHTMLBlock', 'CommunitiesBlock', 'HighlightsBlock', 'DisplayContentBlock', 'LinkListBlock', 'LoginBlock',
                 'MembersBlock', 'MenuBlock', 'PeopleBlock', 'ProfileImageBlock', 'RecentDocumentsBlock', 'StatisticsBlock',
                 'BreadcrumbsPlugin::ContentBreadcrumbsBlock', 'CommentParagraphPlugin::DiscussionBlock',
                 'EventPlugin::EventBlock', 'FriendsBlock', 'InterestTagsBlock', 'ProfileImagesPlugin::ProfileImagesBlock',
                 'RecentActivitiesPlugin::ActivitiesBlock', 'SectionBlockPlugin::SectionBlock', 'VideoPlugin::VideoBlock',
                 'TagsCloudBlock'];
    blocks: BlockDefinition[];

    constructor(private settingsService: SettingsService,
                private profileService: ProfileService,
                private environmentService: EnvironmentService) { }

    loadAvailableBlocks() {
        this.loading = true;
        this.settingsService.getAvailableBlocks(this.owner).subscribe((blocks: BlockDefinition[]) => {
            this.blocks = _.filter(blocks, (block: BlockDefinition) => {
                return this.whitelist.indexOf(block.type) >= 0;
            });
            this.loading = false;
        });
    }

    addBlock(block: Block) {
        this.getOwnerService().getBlockPreview(this.owner, block.type).subscribe((localBlock: Block) => {
            localBlock.permissions = ["allow_edit"];
            localBlock.position = 1;
            this.onAdd.emit(Object.assign({}, localBlock));
        });
    }

    private getOwnerService(): any {
        if (this.isProfile()) {
            return this.profileService;
        } else {
            return this.environmentService;
        }
    }

    isProfile() {
        let type = '';
        if (this.owner !== undefined) {
            type = (<any>this.owner)['type'];
        }
        return type === "Community" || type === "Person";
    }

}
