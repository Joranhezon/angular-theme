import { Inject, Input, Component, OnInit } from '@angular/core';
import { DesignModeService } from '../../shared/services/design-mode.service';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { DragulaService } from 'ng2-dragula';
import { NoosferoKnownEvents } from '../../known-events';
import * as _ from 'lodash';
import { Box } from '../../models/box.model';
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';
import { Block } from '../../models/block.model';

@Component({
    selector: "noosfero-box",
    templateUrl: './box.html'
})
export class BoxComponent implements OnInit {

    @Input() box: Box;
    @Input() boxes: Box[];
    @Input() owner: Profile | Environment;
    @Input() designMode: boolean;
    @Input() column: any;
    @Input() startIndex: number;

    orderedBlocks: Block[] = [];
    bagId: any;

    constructor(private eventsHubService: EventsHubService,
        private dragulaService: DragulaService) {
    }

    ngOnInit() {

        if (_.isNil(this.box)) {
            return null;
        }

        this.bagId = 'box-bag-' + this.box.id;

        const bag: any = this.dragulaService.find(this.bagId);
        if (bag !== undefined ) this.dragulaService.destroy(this.bagId);

        this.dragulaService.setOptions(this.bagId, {
            moves: function (el, container, handle) {
                return handle.className === 'dragula-handle';
            }
        });
        this.box.blocks.forEach((block, index) => {
            const orderedBlock = <Block>{ id: block.id, position: index + 1, box: { id: this.box.id } };
            this.orderedBlocks.push(orderedBlock);
        });
        this.dragulaService.dropModel.subscribe((value) => {
            const [el, target, source] = value;
            if (el === this.bagId) {
                this.updateModel(value.slice(1));
            }
        });
        this.dragulaService.removeModel.subscribe((value) => {
            const [el, target, source] = value;
            if (el === this.bagId) {
                this.updateModel(value.slice(1));
            }
        });

    }

    updateModel(args) {
        this.orderedBlocks.forEach((block, index) => {
            block.position = index + 1;
            block.box.id = this.box.id;
        });
        const orderedBlocksCopy = _.cloneDeep(this.orderedBlocks);
        this.eventsHubService.emitEvent(this.eventsHubService.knownEvents.BOX_CHANGED, orderedBlocksCopy);
    }

    showButton(column: any) {
        return this.designMode && this.isNotParent(column);
    }

    isNotParent(column: any) {
        return column && column['subcolumns'] === undefined;
    }

    removeBlock(removedBlock: Block) {
        this.resetBlockPositions();
    }

    addBlock(block: Block) {
        this.box.blocks.unshift(block);
        this.resetBlockPositions();
        this.box.blocks = this.box.blocks.slice();
    }

    resetBlockPositions() {
        let position = 1;
        this.box.blocks.forEach((block: Block) => {
            if (!block._destroy) {
                block.position = position;
                position++;
            }
        });
    }

    updatePosition(sortedBlocks: Block[]) {
        let i = 0;
        sortedBlocks.forEach((block: Block) => {
            block.position = ++i;
        });
    }
}
