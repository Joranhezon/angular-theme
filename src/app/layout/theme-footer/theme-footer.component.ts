import { HotspotModule } from './../../hotspot/hotspot.module';
import { PluginHotspot } from '../../hotspot/plugin-hotspot';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'theme-footer',
    templateUrl: './theme-footer.html',
    styleUrls: ['./theme-footer.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ThemeFooterComponent extends PluginHotspot {

    constructor(private viewContainerRef: ViewContainerRef, private factory: ComponentFactoryResolver) {
        super('theme_footer');
    }

    addHotspot(component: any) {
        const compFactory = this.factory.resolveComponentFactory(component);
        this.viewContainerRef.clear();
        const componentRef = this.viewContainerRef.createComponent(compFactory);
        componentRef.changeDetectorRef.detectChanges();
    }
}
