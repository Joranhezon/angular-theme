import { HotspotModule } from './../../hotspot/hotspot.module';
import { PluginHotspot } from '../../hotspot/plugin-hotspot';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'theme-header',
    template: ''
})
export class ThemeHeaderComponent extends PluginHotspot {


    constructor(private viewContainerRef: ViewContainerRef, private factory: ComponentFactoryResolver) {
        super('theme_header');
    }

    addHotspot(component: any) {
        const compFactory = this.factory.resolveComponentFactory(component);
        this.viewContainerRef.clear();
        const componentRef = this.viewContainerRef.createComponent(compFactory);
        componentRef.changeDetectorRef.detectChanges();
    }
}
