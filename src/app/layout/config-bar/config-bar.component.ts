import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import { DesignModeService } from '../../shared/services/design-mode.service';
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';

@Component({
    selector: "config-bar",
    templateUrl: './config-bar.html',
    styleUrls: ['./config-bar.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ConfigBarComponent {

    @Input() owner: Profile | Environment;
    @Input() permissionAction = "allow_edit,view_environment_admin_panel";

    designModeOn = false;

    constructor(public designModeService: DesignModeService) {
        this.designModeService.onToggle.subscribe((designModeOn: boolean) => {
            this.designModeOn = designModeOn;
        });

        this.designModeOn = this.designModeService.isInDesignMode();
    }

}
