import { Router } from '@angular/router';
import { Inject, Input, Component, ChangeDetectorRef, ViewEncapsulation, OnDestroy, OnInit, DoCheck } from '@angular/core';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { NoosferoKnownEvents } from '../../known-events';
import { BlockService } from '../../services/block.service';
import { ProfileService } from '../../services/profile.service';
import { EnvironmentService } from '../../services/environment.service';
import { NotificationService } from '../../shared/services/notification.service';
import { DesignModeService } from '../../shared/services/design-mode.service';
import * as _ from "lodash";
import { Profile } from '../../models/profile.model';
import { Environment } from '../../models/environment.model';
import { Block } from '../../models/block.model';
import { Box } from '../../models/box.model';

@Component({
    selector: "context-bar",
    templateUrl: './context-bar.html',
    styleUrls: ['./context-bar.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ContextBarComponent implements OnInit, OnDestroy, DoCheck {

    @Input() owner: Profile | Environment;
    @Input() permissionAction = 'allow_edit';

    blocksChanged: Block[];
    designModeOn = false;
    originalLayout: string;
    originalCustomHeader: string;
    originalCustomFooter: string;
    destroyed = false;

    constructor(private ref: ChangeDetectorRef,
        private router: Router,
        private eventsHubService: EventsHubService,
        private blockService: BlockService,
        private notificationService: NotificationService,
        private designModeService: DesignModeService,
        private profileService: ProfileService,
        private environmentService: EnvironmentService) {
    }

    ngOnInit() {
        if (this.isProfile()) {
            this.originalCustomHeader = (<Profile>this.owner).custom_header;
            this.originalCustomFooter = (<Profile>this.owner).custom_footer;
        }
        this.originalLayout = this.owner.layout_template;
        this.blocksChanged = [];
        this.eventsHubService.subscribeToEvent(this.eventsHubService.knownEvents.BOX_CHANGED, (blocks: Block[]) => {
            blocks.forEach((block) => {
                this.pushChangedBlock(block);
            });
        });
        this.eventsHubService.subscribeToEvent(this.eventsHubService.knownEvents.BLOCK_CHANGED, (block: Block) => {
            this.pushChangedBlock(block);
        });
        this.designModeService.onToggle.subscribe((designModeOn: boolean) => {
            this.designModeOn = designModeOn;
        });
        this.designModeOn = this.designModeService.isInDesignMode();
    }

    pushChangedBlock(block: Block) {
        let blockAlreadyChangedIndex: any;
        const blockAlreadyChanged = this.blocksChanged.find((b: Block, index) => {
            if (block.id === b.id) {
                blockAlreadyChangedIndex = index;
                return true;
            } else {
                return false;
            }
        });
        if (blockAlreadyChanged) {
            this.blocksChanged.splice(blockAlreadyChangedIndex);
            block = Object.assign(blockAlreadyChanged, block);
        }
        if ((block.id || !block._destroy) &&
            (block.title != null || Object.keys(block).length > 3 ||
                (block.api_content && Object.keys(block.api_content).length >= 1) ||
                (block.position))) {
            this.blocksChanged.push(block);
        }
        if (!this.destroyed) this.ref.detectChanges();
    }

    ngOnDestroy() {
        this.destroyed = true;
    }

    ngDoCheck() {
        this.designModeService.hasChanges = this.hasChanges();
    }

    private getOwnerService(): any {
        if (this.isProfile()) {
            return this.profileService;
        } else {
            return this.environmentService;
        }
    }

    apply() {
        if (!this.isLayoutTemplateChanged() && !this.hasBlockChanges()) return Promise.resolve();
        this.applyChanges();
    }

    discard() {
        this.getOwnerService().getBoxes(this.owner).subscribe((boxes: Box[]) => {
            this.owner.boxes = boxes;
            this.blocksChanged = [];
            this.owner.layout_template = this.originalLayout;
            this.notificationService.info({ title: "contextbar.edition.discard.success.title", message: "contextbar.edition.discard.success.message" });
            this.designModeService.setInDesignMode(false);
        });
    }

    applyChanges() {
        const boxesHolder = { id: this.owner.id };
        if (this.hasBlockChanges()) {

            const groupedBoxesChanged = _.groupBy(this.blocksChanged, (block) => {
                const boxId = block.box.id;
                return boxId;
            });
            this.blocksChanged.forEach((b) => {
                if (b.box) {
                    delete b.box;
                }
            });

            const boxes = [];
            Object.keys(groupedBoxesChanged).forEach(function (key) {
                boxes.push({ id: +key, blocks_attributes: groupedBoxesChanged[key] });
            });
            boxesHolder['boxes_attributes'] = boxes;
        }
        if (this.isLayoutTemplateChanged()) boxesHolder['layout_template'] = this.owner.layout_template;
        this.getOwnerService().update(boxesHolder).subscribe(() => {
            this.blocksChanged = [];
            this.originalLayout = this.owner.layout_template;
            this.notificationService.success({ title: "contextbar.edition.apply.success.title", message: "contextbar.edition.apply.success.message" });
            this.designModeService.setInDesignMode(false);
        })
    }

    hasChanges() {
        return this.hasBlockChanges() || this.isLayoutTemplateChanged();
    }

    hasBlockChanges() {
        return this.blocksChanged.length > 0;
    }

    isLayoutTemplateChanged() {
        return this.owner && this.originalLayout !== this.owner.layout_template;
    }

    isProfile() {
        let type = '';
        if (this.owner !== undefined) {
            type = (<any>this.owner)['type'];
        }
        return type === "Community" || type === "Person";
    }

}
