import { Router, ActivatedRoute } from '@angular/router';
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import {ArticleService} from './../services/article.service';
import { Article } from '../models/article.model';
import * as _ from "lodash";

@Component({
    selector: 'search',
    templateUrl: './search.html',
    styleUrls: ['./search.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SearchComponent {

    articles: Article[] = [];
    query: string;
    tag: string;
    totalResults = 0;
    perPage = 10;
    currentPage = 0;

    constructor(protected articleService: ArticleService, private router: Router, private route: ActivatedRoute) {
        this.query = route.snapshot.queryParams['query'];
        this.tag = route.snapshot.queryParams['tag'];
        this.perPage = route.snapshot.queryParams['per_page'] || this.perPage;
        this.loadPage();
    }

    search() {
        this.router.navigate(['search'], { queryParams: { query: this.query } });
    }

    loadPage() {
        const filters = {
            per_page: this.perPage,
            page: this.currentPage,
        };

        if (!_.isNil(this.query)) {
            filters['search'] = this.query;
        }

        if (!_.isNil(this.tag)) {
            filters['tag'] = this.tag;
        }

        this.articleService.search(filters).subscribe((result: any) => {
            this.totalResults = <number>(<any>result.headers).get("total");
            this.articles = result.body;
        });
    }
}
