import { Router } from '@angular/router';
import { Inject, Input, Component, Output, EventEmitter, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { NotificationService } from './../shared/services/notification.service';
import { EnvironmentService } from '../services/environment.service';
import { ValidationMessageComponent } from '../shared/components/validation-message/validation-message.component';
import { Environment } from '../models/environment.model'
import { AuthService } from '../services/auth.service';

@Component({
    selector: 'noosfero-register',
    templateUrl: './register-component.html',
    styleUrls: ['register.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class RegisterComponent implements OnInit {
    @Input() account: any;
    environment: Environment;

    @ViewChild('nameErrors') nameErrors: ValidationMessageComponent;
    @ViewChild('userNameErrors') userNameErrors: ValidationMessageComponent;
    @ViewChild('emailErrors') emailErrors: ValidationMessageComponent;
    @ViewChild('passwordErrors') passwordErrors: ValidationMessageComponent;
    @ViewChild('passwordConfirmErrors') passwordConfirmErrors: ValidationMessageComponent;

    constructor(private router: Router, public authService: AuthService,
        private notificationService: NotificationService, private environmentService: EnvironmentService) {
        this.account = {};
    }

    ngOnInit() {
        this.environmentService.get('default').subscribe((environment: Environment) => {
            this.environment = environment;
        });
    }

    signup() {
        const error = '';
        const field = '';
        this.authService.createAccount(this.account).subscribe( () => {
            this.router.navigate(['/']);
            this.notificationService.success({ title: "account.register.success.title", message: "account.register.success.message" });
        }, ( response) => {

            const errors = response.error;
            if (response.status === 422) {
                this.nameErrors.setBackendErrors(errors);
                this.userNameErrors.setBackendErrors(errors);
                this.emailErrors.setBackendErrors(errors);
                this.passwordErrors.setBackendErrors(errors);
                this.passwordConfirmErrors.setBackendErrors(errors);
            } else {
                this.notificationService.error({ title: "account.register.save.failed" });
            }
        });
    }
}
