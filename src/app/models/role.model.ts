export class Role {

    id: number;
    name: string;
    key: string;
    position: number;
    assigned: boolean;

}
