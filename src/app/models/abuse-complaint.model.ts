import { Task } from './task.model';
import { AbuseReport } from './abuse-report.model';

export class AbuseComplaint extends Task {

    abuse_reports: AbuseReport[];

}

