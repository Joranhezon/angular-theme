export class DefaultResponse {

    code: number;
    message: string;
    success: boolean;

}

