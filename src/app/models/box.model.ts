import { Block } from './block.model';

export class Box {

    id: number;
    position: number;
    blocks: Block[];

}

