import { Person } from './person.model';

export class User {

    id: number;
    login: string;
    email: string;
    person: Person;
    private_token: string;
    userRole: string;

}

