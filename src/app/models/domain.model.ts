import { Profile } from './profile.model';
import { Environment } from './environment.model';

export class Domain {

    id: number;
    name: string;
    is_default: boolean;
    owner: Profile | Environment;

}

