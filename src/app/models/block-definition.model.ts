export class BlockDefinition {

    description: string;
    short_description: string;
    name: string;
    type: string;
}

