import { Section } from './section.model'
/**
 * Represents a noosfero block settings.
 */
export class Settings {

    sections: Section[];
    limit: number;
    visualization: any;
}
