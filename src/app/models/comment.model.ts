import { Person } from './person.model';

export class Comment {

    id: number;
    source_id: number;
    reply_of_id: number;
    reply_of: Comment;
    replies: Comment[];
    body: string;
    author: Person;
    created_at: Date;
    permissions: string[];
    __show_reply?: boolean;
}
