import { Person } from './person.model';

export class AbuseReport {

    id: number;
    reporter: Person;
    reason: string;
    created_at: string;
}

