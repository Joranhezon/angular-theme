export class Image {

    id: number;
    filename: string;
    url: string;
}
