import { Block } from './block.model';
import { StatisticInfo } from './statistics-info.model';

export class StatisticsBlock extends Block {

    statistics: StatisticInfo[];

}

