import { Profile } from './profile.model';
import { Block } from './block.model';

export class Article {

    permissions: string[];
    abstract: string;
    path: string;
    profile: Profile;
    type: string;
    parent: Article;
    body: string;
    title: string;
    name: string;
    published: boolean;
    setting: any;
    start_date: string;
    end_date: string;
    accept_comments: boolean;
    id: number;
    position: number;
    blocks: Block[];

}

