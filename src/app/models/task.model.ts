import { Profile } from './profile.model';
import { Environment } from './environment.model';
import { Person } from './person.model';

export class Task {

    id: number;
    type: string;
    accept_details: boolean;
    reject_details: boolean;
    target: Profile | Environment;
    created_at: Date;
    requestor: Person;
    data: any;
}

