import { Box } from './box.model'

export class Profile {

    permissions: string[];

    /**
     * @ngdoc property
     * @name id
     * @propertyOf Profile
     * @returns {number} The Profile id
     */
    id: number;

    /**
     * @ngdoc property
     * @name identifier
     * @propertyOf Profile
     * @returns {string} The unque identifier for the Profile
     */
    identifier: string;

    /**
     * @ngdoc property
     * @name created_at
     * @propertyOf Profile
     * @returns {string} The timestamp this object was created
     */
    created_at: string;

    /**
     * @ngdoc property
     * @name type
     * @propertyOf Profile
     * @returns {string} The Profile type (e.g.: "Person", etc.)
     */
    type: string;

    /**
     * @ngdoc property
     * @name name
     * @propertyOf Profile
     * @returns {string} The name of Profile (e.g.: "Mr. Janson", etc.)
     */
    name: string;

    /**
     * @ngdoc property
     * @name additional_data
     * @propertyOf Profile
     * @returns {string} A key => value custom fields data of Profile (e.g.: "{'Address':'Street A, Number 102...'}")
     */
    additional_data?: any;

    /**
     * @ngdoc property
     * @name homepage
     * @propertyOf Profile
     * @returns {string} The Profile homepage
     */
    homepage: string;

    /**
     * @ngdoc property
     * @name custom_header
     * @propertyOf Profile
     * @returns {string} The Profile header
     */
    custom_header: string;

    /**
     * @ngdoc property
     * @name custom_footer
     * @propertyOf Profile
     * @returns {string} The Profile footer
     */
    custom_footer: string;

    /**
     * @ngdoc property
     * @name layout_template
     * @propertyOf Profile
     * @returns {string} The Profile layout template (e.g.: "rightbar", "default")
     */
    layout_template: string;

    /**
     * @ngdoc property
     * @name top_image
     * @propertyOf Profile
     * @returns {any} The Profile top image
     */
    top_image: any;

    /**
     * @ngdoc property
     * @name image
     * @propertyOf Profile
     * @returns {any} The Profile image
     */
    image: any;

    /**
     * @ngdoc property
     * @name boxes
     * @propertyOf Profile
     * @returns Box[] The Boxes of a profile
     */
    boxes: Box[];

    /**
     * @ngdoc property
     * @name theme
     * @propertyOf Profile
     * @returns string The profile theme
     */
    theme: string;
}
