import { Settings } from './settings.model'
import { Image } from './image.model'
import { Box } from './box.model'

export class Block {

    id: number;
    settings: Settings;
    limit: number;
    api_content: any;
    hide: boolean;
    title: string;
    type: string;
    images: Image[];
    _destroy: boolean;
    position: number;
    permissions: string[];
    box: Box;

}

