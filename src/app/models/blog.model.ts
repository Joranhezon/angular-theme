import { Article } from './article.model';
import { Block } from './block.model';

export class Blog extends Article {

    id: number;
    position: number;
    blocks: Block[];

}

