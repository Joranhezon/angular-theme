import { Profile } from './profile.model';

export class Activity {
        /**
         * @ngdoc property
         * @name verb
         * @propertyOf Activity
         * @returns {string} The activity verb.
         */
        verb: string;

        user: Profile;
        params: any[];
        created_at: any;
        updated_at: any;
        target: Profile;

}

