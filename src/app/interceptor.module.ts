import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
import { SessionService } from './services/session.service';
import { TranslatorService } from './shared/services/translator.service';
import { AuthService } from './services/auth.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

  lastAuthToken =  null;

  constructor(private sessionService: SessionService,
    private translatorService: TranslatorService,
    private cookieService: CookieService,
    private authService: AuthService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authRequest = request;
    const token = this.sessionService.getPrivateToken();

    const auth_token = this.cookieService.get('auth_token')
    if (token) {
      authRequest = request.clone({
        headers: request.headers.set('Private-Token', token)
                  .set('Accept-Language', this.translatorService.currentLanguage())
      });
    } else if (auth_token && (this.lastAuthToken !== auth_token)) {
      this.lastAuthToken = auth_token;
      this.authService.loginFromCookie().subscribe((user) => {
        this.authService.loginSuccessCallback(user)
      });
    }

    return next.handle(authRequest);

  }
};
