import { NoosferoTranslateLoader } from './shared/noosfero-translate.loader';
import swal from 'sweetalert2';
import { TagCloudModule } from 'angular-tag-cloud-module';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { BrowserXhr } from '@angular/http';
import { MyDatePickerModule } from 'mydatepicker';
import { DynamicHTMLModule, DynamicComponentModule } from 'ng-dynamic';
import { Ng2Webstorage } from 'ngx-webstorage';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ToastrModule } from 'ngx-toastr';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpModule, JsonpModule, Http } from '@angular/http';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { LoadingModule } from 'ngx-loading';
import { ImageCropperModule } from 'ng2-img-cropper';
import { DragulaModule, DragulaService } from 'ng2-dragula';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpsRequestInterceptor } from './interceptor.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

// Bootstrap
import { PopoverModule, ModalModule, TypeaheadModule} from 'ngx-bootstrap';
import {  BsDropdownModule, CarouselModule, CollapseModule, TabsModule } from 'ngx-bootstrap';

// Noosfero Blocks
import { HighlightsBlockSettingsComponent } from './layout/blocks/highlights/highlights-block-settings.component';
import { BlockSettingsComponent } from './layout/blocks/block-settings.component';
import { RawHTMLBlockComponent } from './layout/blocks/raw-html/raw-html-block.component';
import { StatisticsBlockComponent } from './layout/blocks/statistics/statistics-block.component';
// FIXME remove this line
import { DisplayContentBlockComponent } from './layout/blocks/display-content/display-content-block.component';
import { HighlightsBlockComponent } from './layout/blocks/highlights/highlights-block.component';
import { TagsBlockComponent } from './layout/blocks/tags/tags-block.component';
import { LoginBlockComponent } from './layout/blocks/login-block/login-block.component';
import { LinkListBlockComponent } from './layout/blocks/link-list/link-list-block.component';
import { MenuBlockComponent } from './layout/blocks/menu/menu-block.component';
import { CommunitiesBlockComponent } from './layout/blocks/communities/communities-block.component';
import { MembersBlockComponent } from './layout/blocks/members/members-block.component';
import { PeopleBlockComponent } from './layout/blocks/people/people-block.component';
import { ProfileImageBlockComponent } from './layout/blocks/profile-image/profile-image-block.component';
import { RecentDocumentsBlockComponent } from './layout/blocks/recent-documents/recent-documents-block.component';

// Other components
import { AppComponent } from './app.component';
import { AuthService } from './services/auth.service';
import { BoxComponent } from './layout/boxes/box.component';
import { BoxesComponent } from './layout/boxes/boxes.component';
import { BlockComponent } from './layout/blocks/block.component';
import { BlockContentComponent } from './layout/blocks/block-content.component';
import { MainBlockComponent } from './layout/blocks/main/main-block.component';
import { ForgotPasswordComponent } from './login/forgot-password.component';
import { LoginComponent } from './login/login.component';

import { CommunityMembersProfileComponent } from './profile/community-members/community-members-profile.component';
import { DestroyProfileComponent } from './profile/destroy/destroy-profile.component';
import { ProfileEditionComponent } from './profile/configuration/profile-edition.component';
import { ProfileConfigurationComponent } from './profile/configuration/profile-configuration.component';
import { CmsComponent } from './article/cms/cms.component';
import { ProfileAboutComponent } from './profile/about/profile-about.component';
import { DomainComponent } from './domain/domain.component';
import { EditableFieldComponent } from './shared/components/editable-field/editable-field.component';
import { EnvironmentHomeComponent } from './environment/environment-home.component';
import { ActivitiesComponent } from './profile/activities/activities.component';
import { ProfileHomeComponent } from './profile/profile-home.component';
import { ContentViewerComponent } from './article/content-viewer/content-viewer.component';
import { ProfileComponent } from './profile/profile.component';
import { EnvironmentComponent } from './environment/environment.component';
import { EventsHubService } from './shared/services/events-hub.service';
import { CommentParagraphEventService } from '../plugins/comment_paragraph/events/comment-paragraph-event.service';
import { ThemeService } from './shared/services/theme.service';
import { BodyStateClassesService } from './shared/services/body-state-classes.service';
import { DesignModeService } from './shared/services/design-mode.service';
import { DomainService } from './services/domain.service';
import { SessionService } from './services/session.service';
import { PersonService } from './services/person.service';
import { TaskService } from './services/task.service';
import { BlockService } from './services/block.service';
import { CommunityService } from './services/community.service';
import { SettingsService } from './services/settings.service';
import { RoleService } from './services/role.service';
import { PermissionService } from './shared/services/permission.service';
import { CommentParagraphService } from '../plugins/comment_paragraph/http/comment-paragraph.service';
import { NotificationService } from './shared/services/notification.service';
import { TranslatorService } from './shared/services/translator.service';
import { EnvironmentService } from './services/environment.service';
import { ProfileService } from './services/profile.service';
import { ArticleService } from './services/article.service';
import { CommentService } from './services/comment.service';
import { HeaderService } from './shared/services/header.service';
import { ArticleViewComponent } from './article/article-view.component';
import { ActivityComponent } from './profile/activities/activity/activity.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { ArticleEditorComponent } from './article/cms/article-editor/article-editor.component';
import { ActivityHeaderComponent } from './profile/activities/activity/header/activity-header.component';
import { CustomContentComponent } from './profile/custom-content/custom-content.component';
import { ValidationMessageComponent } from './shared/components/validation-message/validation-message.component';
import { ArticleDefaultViewComponent } from './article/types/default/article-default.component';
import { FolderComponent } from './article/types/folder/folder.component';
import { ArticleBlogComponent } from './article/types/blog/blog.component';
import { TaskAcceptComponent } from './task/task-list/task-accept.component';
import { ArticleContentHotspotComponent } from './hotspot/article-content-hotspot.component';
import { BasicOptionsComponent } from './article/cms/basic-options/basic-options.component';
import { ArticleToolbarHotspotComponent } from './hotspot/article-toolbar-hotspot.component';
import { ThemeFooterComponent } from './layout/theme-footer/theme-footer.component';
import * as theme from '../theme';
import { ThemeHeaderComponent } from './layout/theme-header/theme-header.component';
import { RegisterComponent } from './account/register.component';
import { SearchComponent } from './search/search.component';
import { PasswordComponent } from './login/new-password.component';
import { ArticleIconComponent } from './article/article-icon/article-icon.component';
import { SearchFormComponent } from './search/search-form/search-form.component';
import { TasksMenuComponent } from './task/tasks-menu/tasks-menu.component';
import { TasksComponent } from './task/tasks/tasks.component';
import { TaskComponent } from './task/task.component';
import { ProfileActionsComponent } from './profile/actions/profile-actions.component';
import { DesignModeTogglerComponent } from './layout/design-mode-toggler/design-mode-toggler.component';
import { ConfigBarComponent } from './layout/config-bar/config-bar.component';
import { LayoutConfigComponent } from './layout/layout-config/layout-config.component';
import { ContextBarComponent } from './layout/context-bar/context-bar.component';
import { ProfileHeaderComponent } from './profile/header/profile-header.component';
import { AddBlockComponent } from './layout/boxes/add-block/add-block.component';
import { TopProfileImageComponent } from './profile/top-image/top-profile-image.component';
import { BlockEditionComponent } from './layout/blocks/block-edition/block-edition.component';
import { NoosferoTemplatePipe } from './shared/pipes/noosfero-template.ng2.filter';
import { IconPickerComponent } from './shared/components/icon-picker/icon-picker.component';
import { EditableLinkComponent } from './shared/components/editable-link/editable-link.component';
import { ProfileSummaryComponent } from './profile/summary/profile-summary.component';
import { NewCommunityComponent } from './profile/configuration/communities/new-community.component';
import { EditCommunityComponent } from './profile/configuration/communities/edit-community.component';
import { PersonCommunitiesComponent } from './profile/configuration/communities/person-communities.component';
import { ChangePasswordComponent } from './profile/configuration/change-password/change-password.component';
import { PersonFriendsComponent } from './profile/configuration/friends/person-friends.component';
import { CommunityMembersMyProfileComponent } from './profile/configuration/communities/community-members-my-profile.component';
import { ProfilePersonalDataComponent } from './profile/configuration/personal-data/profile-personal-data.component';
import { ProfileConfigurationMenuComponent } from './profile/configuration/menu/profile-configuration-menu.component';
import { NoosferoUrlPipe } from './shared/pipes/noosfero-url.pipe';
import { ProfileListEditionComponent } from './profile/profile-list/edition/profile-list-edition.component';
import { ProfileFastEditionComponent } from './profile/fast-edition/profile-fast-edition.component';
import { CommunityMembersComponent } from './profile/community-members/community-members.component';
import { CommunityMembersPaginatedComponent } from './profile/community-members/community-members-paginated.component';
import { TaskListComponent } from './task/task-list/task-list.component';
import { LanguageSelectorComponent } from './layout/language-selector/language-selector.component';
import { FooterComponent } from './layout/footer/footer.component';
import { ProfileListComponent } from './profile/profile-list/profile-list.component';
import { ProfileJoinComponent } from './profile/profile-join/profile-join.component';
import { InviteComponent } from './profile/configuration/communities/invite.component';
import * as plugins from '../plugins';
import { SharedModule } from './shared.module';
import { BootstrapResizableDirective } from './shared/components/bootstrap-resizable/bootstrap-resizable.directive';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

// Tasks Components
import { AbuseComplaintTaskComponent } from './task/types/abuse-complaint/abuse-complaint-task.component';
import { AddFriendTaskComponent } from './task/types/add-friend/add-friend-task.component';
import { AddMemberTaskComponent } from './task/types/add-member/add-member-task.component';
import { ApproveArticleTaskComponent } from './task/types/approve-article/approve-article-task.component';
import { ApproveCommentTaskComponent } from './task/types/approve-comment/approve-comment-task.component';
import { CreateCommunityTaskComponent } from './task/types/create-community/create-community-task.component';

import { AbuseComplaintTaskAcceptComponent } from './task/types/abuse-complaint/abuse-complaint-task-accept.component';
import { AddFriendTaskAcceptComponent } from './task/types/add-friend/add-friend-task-accept.component';
import { AddMemberTaskAcceptComponent } from './task/types/add-member/add-member-task-accept.component';
import { ApproveArticleTaskAcceptComponent } from './task/types/approve-article/approve-article-task-accept.component';
import { ApproveCommentTaskAcceptComponent } from './task/types/approve-comment/approve-comment-task-accept.component';

import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';
import { HotspotModule } from './hotspot/hotspot.module';
// /home/81665687568/projetos/angular-theme/src/app/hotspot/hotspot.module.ts
export function TranslateLoaderFactory() {
    return new NoosferoTranslateLoader();
}

@NgModule({
    imports: [
        FormsModule,
        BrowserModule,
        HotspotModule,
        ModalModule.forRoot(),
        ImageCropperModule,
        PopoverModule.forRoot(),
        DragulaModule,
        TypeaheadModule.forRoot(),
        BsDropdownModule.forRoot(),
        CarouselModule.forRoot(),
        CollapseModule.forRoot(),
        TabsModule.forRoot(),
        BrowserAnimationsModule,
        TagCloudModule,
        SharedModule,
        MyDatePickerModule,
        LoadingModule,
        HttpClientModule,
        DynamicHTMLModule.forRoot({
            components: plugins.macros
        }),
        Ng2Webstorage.forRoot({
            prefix: 'noosfero',
            caseSensitive: true
        }),
        SweetAlert2Module,
        ToastrModule.forRoot(),
        HttpModule,
        JsonpModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: TranslateLoaderFactory,
                deps: [Http]
            }
        }),
        NgxPageScrollModule,
        NgProgressModule.forRoot(),
        NgProgressHttpModule,
        AppRoutingModule,
    ],
    declarations: [
        FooterComponent,
        LanguageSelectorComponent,
        RawHTMLBlockComponent,
        RecentDocumentsBlockComponent,
        StatisticsBlockComponent,
        TaskListComponent,
        CommunitiesBlockComponent,
        MembersBlockComponent,
        PeopleBlockComponent,
        ProfileImageBlockComponent,
        DisplayContentBlockComponent,
        TagsBlockComponent,
        LoginBlockComponent,
        LinkListBlockComponent,
        MenuBlockComponent,
        HighlightsBlockComponent,
        HighlightsBlockSettingsComponent,
        BlockEditionComponent,
        AddBlockComponent,
        MainBlockComponent,
        BlockContentComponent,
        BlockComponent,
        BlockSettingsComponent,
        CommunityMembersComponent,
        CommunityMembersPaginatedComponent,
        ProfileFastEditionComponent,
        ValidationMessageComponent,
        ProfileListComponent,
        ProfileJoinComponent,
        ProfileConfigurationMenuComponent,
        ProfilePersonalDataComponent,
        NoosferoUrlPipe,
        ProfileListEditionComponent,
        NewCommunityComponent,
        ChangePasswordComponent,
        PersonFriendsComponent,
        CommunityMembersMyProfileComponent,
        EditCommunityComponent,
        PersonCommunitiesComponent,
        InviteComponent,
        BootstrapResizableDirective,
        ProfileSummaryComponent,
        EditableLinkComponent,
        EditableFieldComponent,
        IconPickerComponent,
        NoosferoTemplatePipe,
        TopProfileImageComponent,
        ProfileHeaderComponent,
        ContextBarComponent,
        LayoutConfigComponent,
        ProfileActionsComponent,
        ConfigBarComponent,
        DesignModeTogglerComponent,
        TaskComponent,
        TasksComponent,
        TasksMenuComponent,
        SearchFormComponent,
        ArticleIconComponent,
        PasswordComponent,
        RegisterComponent,
        ThemeHeaderComponent,
        ThemeFooterComponent,
        ArticleToolbarHotspotComponent,
        BasicOptionsComponent,
        TaskAcceptComponent,
        ArticleContentHotspotComponent,
        ArticleBlogComponent,
        FolderComponent,
        ArticleDefaultViewComponent,
        CustomContentComponent,
        ActivityHeaderComponent,
        ArticleEditorComponent,
        NavbarComponent,
        ActivityComponent,
        ArticleViewComponent,
        LoginComponent,
        ForgotPasswordComponent,
        BoxesComponent,
        BoxComponent,
        AppComponent,
        EnvironmentComponent,
        ProfileComponent,
        ContentViewerComponent,
        ProfileHomeComponent,
        ActivitiesComponent,
        EnvironmentHomeComponent,
        DomainComponent,
        SearchComponent,
        ProfileAboutComponent,
        CmsComponent,
        ProfileConfigurationComponent,
        ProfileEditionComponent,
        DestroyProfileComponent,
        CommunityMembersProfileComponent,
        AbuseComplaintTaskComponent,
        AddFriendTaskComponent,
        AddMemberTaskComponent,
        ApproveArticleTaskComponent,
        ApproveCommentTaskComponent,
        CreateCommunityTaskComponent,
        AbuseComplaintTaskAcceptComponent,
        AddFriendTaskAcceptComponent,
        AddMemberTaskAcceptComponent,
        ApproveArticleTaskAcceptComponent,
        ApproveCommentTaskAcceptComponent,
        NotFoundComponent,
    ].concat(plugins.ng2MainComponents).concat(theme.components),
    entryComponents: [
        FooterComponent,
        LanguageSelectorComponent,
        RawHTMLBlockComponent,
        RecentDocumentsBlockComponent,
        StatisticsBlockComponent,
        TaskListComponent,
        CommunitiesBlockComponent,
        MembersBlockComponent,
        PeopleBlockComponent,
        ProfileImageBlockComponent,
        DisplayContentBlockComponent,
        HighlightsBlockComponent,
        TagsBlockComponent,
        BlockEditionComponent,
        AddBlockComponent,
        MenuBlockComponent,
        LoginBlockComponent,
        LinkListBlockComponent,
        BlockContentComponent,
        BlockComponent,
        CommunityMembersComponent,
        ProfileFastEditionComponent,
        ProfileConfigurationMenuComponent,
        ProfileListComponent,
        EditCommunityComponent,
        InviteComponent,
        ProfileSummaryComponent,
        EditableLinkComponent,
        IconPickerComponent,
        TopProfileImageComponent,
        ProfileHeaderComponent,
        ContextBarComponent,
        LayoutConfigComponent,
        ConfigBarComponent,
        DesignModeTogglerComponent,
        TasksMenuComponent,
        SearchFormComponent,
        ArticleIconComponent,
        ThemeHeaderComponent,
        ThemeFooterComponent,
        ArticleToolbarHotspotComponent,
        BasicOptionsComponent,
        TaskAcceptComponent,
        ArticleContentHotspotComponent,
        ArticleBlogComponent,
        FolderComponent,
        ArticleDefaultViewComponent,
        CustomContentComponent,
        ActivityHeaderComponent,
        ArticleEditorComponent,
        NavbarComponent,
        ActivityComponent,
        ArticleViewComponent,
        BoxesComponent,
        EditableFieldComponent,
        AbuseComplaintTaskComponent,
        AddFriendTaskComponent,
        AddMemberTaskComponent,
        ApproveArticleTaskComponent,
        ApproveCommentTaskComponent,
        CreateCommunityTaskComponent,
        AbuseComplaintTaskAcceptComponent,
        AddFriendTaskAcceptComponent,
        AddMemberTaskAcceptComponent,
        ApproveArticleTaskAcceptComponent,
        ApproveCommentTaskAcceptComponent,
    ].concat(plugins.ng2MainComponents),
    providers: [
        HeaderService,
        CommentService,
        ArticleService,
        SessionService,
        ProfileService,
        EnvironmentService,
        CommentParagraphService,
        PermissionService,
        RoleService,
        SettingsService,
        CommunityService,
        BlockService,
        TaskService,
        PersonService,
        DomainService,
        AuthService,
        DesignModeService,
        BodyStateClassesService,
        ThemeService,
        CommentParagraphEventService,
        EventsHubService,
        TranslatorService,
        NotificationService,
        CookieService,
        DragulaService,
        { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true },
        { provide: 'sweetAlert', useValue: swal },
        { provide: 'Window',  useValue: window },
        { provide: 'environment', useValue: environment }
    ],
    bootstrap: [ AppComponent ]
})

export class AppModule { }
