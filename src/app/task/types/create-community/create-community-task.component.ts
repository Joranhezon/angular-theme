import { Component } from '@angular/core';
import { TaskComponentInterface } from '../../task.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "create-community-task",
    templateUrl: './create-community.html'
})
export class CreateCommunityTaskComponent implements TaskComponentInterface {

    task: Task;
}
