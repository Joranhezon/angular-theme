import { Injector, Component, Input, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { ArticleService } from '../../../services/article.service';
import { Article } from '../../../models/article.model';
import { Comment } from '../../../models/comment.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "approve-comment-task-accept",
    templateUrl: './approve-comment-accept.html',
    styleUrls: ['./approve-comment-accept.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ApproveCommentTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

    task: Task;
    comment = <Comment>{};
    article = <Article>{};

    constructor(private articleService: ArticleService) {}

    ngOnInit() {
        const attrs = JSON.parse(this.task.data.comment_attributes);
        this.comment.body = attrs.body;
        this.comment.created_at = attrs.created_at;
        this.comment.author = this.task.requestor;
        this.articleService.get(attrs.source_id).subscribe((article: Article) => {
            this.article = article;
        });
    }
}
