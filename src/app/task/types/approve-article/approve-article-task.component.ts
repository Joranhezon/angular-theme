import { ViewEncapsulation } from '@angular/core';
import { Component } from '@angular/core';
import { TaskComponentInterface } from '../../task.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "approve-article-task",
    templateUrl: "./approve-article.html",
    styleUrls: ['./approve-article.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ApproveArticleTaskComponent implements TaskComponentInterface {

    task: Task;

}
