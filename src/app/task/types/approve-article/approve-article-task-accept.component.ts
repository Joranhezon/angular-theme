import { Component, Injector, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { ArticleService } from '../../../services/article.service';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "approve-article-task-accept",
    templateUrl: './approve-article-accept.html',
    styleUrls: ['./approve-article-accept.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ApproveArticleTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

    task: Task;
    folders: Array<any>;

    constructor(private articleService: ArticleService) { }

    ngOnInit() {
        const targetProfile = <Profile>this.task.target;
        const root = targetProfile.identifier;
        this.task.data = { create_link: true, article_id: this.task.data.article_id, name: this.task.data.name };

        this.folders = [{ id: null, path: root }];
        this.articleService.getByProfile(targetProfile, { content_type: "Folder,Blog" }).subscribe((articles: Article[]) => {
            articles.forEach((article: Article) => {
                this.folders.push({ id: article.id, path: `${root}/${article.title}` });
            });
        });
    }
}
