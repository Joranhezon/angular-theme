import { ViewEncapsulation } from '@angular/core';
import { Component } from '@angular/core';
import { TaskComponentInterface } from '../../task.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "abuse-complaint-task",
    templateUrl: './abuse-complaint.html',
    styleUrls: ['./abuse-complaint.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AbuseComplaintTaskComponent implements TaskComponentInterface {

    task: Task;

}

