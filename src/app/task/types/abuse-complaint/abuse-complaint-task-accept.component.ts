import { TaskService } from '../../../services/task.service';
import { AbuseComplaint } from '../../../../app/models/abuse-complaint.model';
import { Task } from '../../../models/task.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, OnInit } from '@angular/core';
import * as _ from "lodash";

@Component({
    selector: "abuse-complaint-task-accept",
    templateUrl: './abuse-complaint-accept.html',
})
export class AbuseComplaintTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

    task: Task;

    constructor(private taskService: TaskService) {}

    ngOnInit() {
        if (_.isNil(this.task.target)) {
            this.taskService.get(this.task.id).subscribe((task: AbuseComplaint) => {
                this.task = task;
            });
        }

    }
}
