import { Component, Injector, Inject, OnInit } from '@angular/core';
import { TaskService } from '../../../services/task.service';
import { AddFriend } from '../../../models/add-friend.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Task } from '../../../models/task.model';
import * as _ from "lodash";

@Component({
    selector: "add-friend-task-accept",
    templateUrl: './add-friend-accept.html',
})
export class AddFriendTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

    task: Task;

    constructor(private taskService: TaskService) {}

    ngOnInit() {
        if (_.isNil(this.task.target)) {
            this.taskService.get(this.task.id).subscribe((task: AddFriend) => {
                this.task = task;
            });

        }


    }
}
