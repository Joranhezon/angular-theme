import { ViewEncapsulation } from '@angular/core';
import { Component } from '@angular/core';
import { TaskComponentInterface } from '../../task.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "add-friend-task",
    templateUrl: './add-friend.html',
    styleUrls: ['./add-friend.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AddFriendTaskComponent implements TaskComponentInterface {

    task: Task;

}

