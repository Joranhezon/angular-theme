import { ViewEncapsulation } from '@angular/core';
import { Component } from '@angular/core';
import { TaskComponentInterface } from '../../task.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "add-friend-task",
    templateUrl: './add-member.html',
    styleUrls: ['./add-member.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class AddMemberTaskComponent implements TaskComponentInterface {

    task: Task;

}
