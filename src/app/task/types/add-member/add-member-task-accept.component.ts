import { Component, Input, Inject, Injector, OnInit } from '@angular/core';
import { RoleService } from '../../../services/role.service';
import { Observable } from 'rxjs/Observable';
import { AddMember } from '../../../models/add-member.model';
import { Role } from '../../../models/role.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "add-member-task-accept",
    templateUrl: './add-member-accept.html',
})
export class AddMemberTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

    task: Task;
    roles: Role[];

    constructor(private roleService: RoleService) {}

    ngOnInit() {
        if (!this.task.target) return;
        (<AddMember>this.task).roles = [];
        this.roleService.getByProfile(this.task.target.id).subscribe( (roles: Role[]) => {
            this.roles = roles;
        });
    }

    toggleSelection(role: any) {
        const index = (<AddMember>this.task).roles.indexOf(role.id);
        if (index >= 0) {
            (<AddMember>this.task).roles.splice(index, 1);
        } else {
            (<AddMember>this.task).roles.push(role.id);
        }
    }
}
