import { Component, Injector, Inject, OnInit } from '@angular/core';
import { ArticleService } from '../../../services/article.service';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';
import { TaskAcceptComponentInterface } from '../../task-list/task-accept.component';
import { Task } from '../../../models/task.model';

@Component({
    selector: "suggest-article-task-accept",
    templateUrl: './suggest-article-accept.html',
})
export class SuggestArticleTaskAcceptComponent implements TaskAcceptComponentInterface, OnInit {

    task: Task;
    folders: Array<any>;

    constructor(private articleService: ArticleService) {}

    ngOnInit() {
        const targetProfile = <Profile>this.task.target;
        const root = targetProfile.identifier;
        this.folders = [{ id: null, path: root }];

        this.articleService.getByProfile(targetProfile, { content_type: "Folder,Blog" }).subscribe((articles: Article[]) => {
            articles.forEach((article: Article) => {
                this.folders.push({ id: article.id, path: `${root}/${article.title}` });
            });
        });
    }
}
