import { ActivatedRoute } from '@angular/router';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { TaskService } from '../../services/task.service';
import { Task } from '../../models/task.model';
import { PageScrollConfig, PageScrollService, PageScrollInstance } from 'ngx-page-scroll';
import { DOCUMENT} from '@angular/common';
import * as _ from "lodash";

@Component({
    selector: "tasks",
    templateUrl: './tasks.html'
})
export class TasksComponent implements OnInit {

    tasks: Task[];
    total: number;
    perPage = 10;
    types: string;
    public loading = false;

    constructor(private taskService: TaskService,
        @Inject(DOCUMENT) private document: any,
        private pageScrollService: PageScrollService,
        private route: ActivatedRoute) {
        this.types = route.snapshot.queryParams['types'];
    }

    ngOnInit() {
        this.loadTasks({ page: 1 });
    }

    public goToTopTaskList(): void {
        const pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#task-list-header');
        this.pageScrollService.start(pageScrollInstance);
    };

    loadTasks($event: any) {
        if (!_.isNil($event.page)) {
            this.goToTopTaskList();
        }

        this.loading = true;
        this.taskService.getAllPending({content_type: this.types, page: $event.page, per_page: this.perPage }).subscribe((result: any) => {
            this.total = result.headers.get('total');
            this.tasks = result.body;
            this.loading = false;
        });
    }
}
