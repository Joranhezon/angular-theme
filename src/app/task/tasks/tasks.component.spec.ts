import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from './../../shared/services/translator.service';
import { TaskService } from './../../services/task.service';
import { MomentModule } from 'angular2-moment';
import { FormsModule } from '@angular/forms';
import { TaskListComponent } from './../task-list/task-list.component';
import { PaginationModule } from 'ngx-bootstrap';
import * as helpers from '../../../spec/helpers';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { TasksComponent } from './tasks.component';
import { AuthEvents } from './../../services';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PageScrollConfig, PageScrollService, PageScrollInstance } from 'ngx-page-scroll';

describe("Components", () => {
    describe("Tasks Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<TasksComponent>;
        let component: TasksComponent;

        const tasks = [{ id: 1 }, { id: 2 }];

        beforeEach(async(() => {
            spyOn(mocks.taskService, 'getAllPending').and.returnValue(Observable.of({ headers: { get: () => { } }, body: tasks }));
            TestBed.configureTestingModule({
                declarations: [TasksComponent],
                providers: [
                    { provide: TranslatorService, useValue: mocks.translatorService },
                    { provide: TaskService, useValue: mocks.taskService },
                    PageScrollService
                ],
                schemas: [NO_ERRORS_SCHEMA],
                imports: [RouterTestingModule, PaginationModule.forRoot(), FormsModule, TranslateModule.forRoot()]
            });
            fixture = TestBed.createComponent(TasksComponent);
            component = fixture.componentInstance;
        }));

        it("load person tasks", () => {
            fixture.detectChanges();
            expect(TestBed.get(TaskService).getAllPending).toHaveBeenCalled();
        });

        it("load person tasks with page parameter", () => {
            component.types = "AddFriend";
            fixture.detectChanges();
            expect(TestBed.get(TaskService).getAllPending).toHaveBeenCalledWith({ content_type: component.types, page: 1, per_page: 10 });
        });
    });
});
