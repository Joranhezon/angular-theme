import { TaskModule } from './../task.module';
import { TaskService } from './../../services/task.service';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, Input } from '@angular/core';
import { OnInit } from '@angular/core';
import * as types from '../types';
import { Task } from '../../models/task.model';

export interface TaskAcceptComponentInterface {
    task: Task;
}

@Component({
    selector: 'task-accept',
    template: ''
})
export class TaskAcceptComponent implements OnInit {

    @Input() task: Task;

    constructor(private viewContainerRef: ViewContainerRef,
        private factory: ComponentFactoryResolver) {
    }

    ngOnInit() {
        const taskType = `${this.task.type}TaskAcceptComponent`;
        if (TaskService.TASK_TYPES.indexOf(this.task.type) >= 0) {
            const taskComponent = types[`${this.task.type}TaskAcceptComponent`];
            const compFactory = this.factory.resolveComponentFactory(taskComponent);
            this.viewContainerRef.clear();
            const componentRef = (this.viewContainerRef.createComponent(compFactory) as ComponentRef<TaskAcceptComponentInterface>);
            componentRef.instance.task = this.task;
            componentRef.changeDetectorRef.detectChanges();
        }
    }
}
