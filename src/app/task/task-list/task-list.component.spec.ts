import { ModalModule } from 'ngx-bootstrap/modal';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationService } from './../../shared/services/notification.service';
import { EventsHubService } from './../../shared/services/events-hub.service';
import { TaskService } from './../../services/task.service';
import { TaskComponent } from './../task.component';
import { MomentModule } from 'angular2-moment';
import { Directive, Input, NO_ERRORS_SCHEMA } from '@angular/core';
import { ProfileImageComponent } from './../../profile/image/profile-image.component';
import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import * as helpers from '../../../spec/helpers';
import { TaskListComponent } from './task-list.component';
import { Observable } from 'rxjs/Observable';


@Directive({ selector: '[dynamicComponent]' })
class DynamicComponentMockDirective {
    @Input() template: string;
    @Input() context: any;
}

describe("Components", () => {
    describe("Task List Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<TaskListComponent>;
        let component: TaskListComponent;

        beforeEach(async(() => {
            spyOn(mocks.taskService, 'taskClosed');
            spyOn(mocks.taskService.taskClosed, 'next').and.callThrough();
            spyOn(mocks.taskService, 'cancelTask').and.callThrough();
            spyOn(mocks.taskService, 'finishTask').and.callThrough();

            const profileService = jasmine.createSpyObj("profileService", ["upload"]);

            TestBed.configureTestingModule({
                imports: [MomentModule, TranslateModule.forRoot(), ModalModule.forRoot()],
                declarations: [TaskListComponent, ProfileImageComponent, DynamicComponentMockDirective],
                providers: [
                    { provide: TaskService, useValue: mocks.taskService },
                    { provide: EventsHubService, useValue: mocks.eventsHubService },
                    { provide: NotificationService, useValue: mocks.notificationService },
                ],
                schemas: [NO_ERRORS_SCHEMA]
            });
            fixture = TestBed.createComponent(TaskListComponent);
            component = fixture.componentInstance;
            component.tasks = <any>[{ id: 1 }, { id: 2 }];
        }));

        it("open confirmation modal when it has details to accept a task", () => {
            const task = { accept_details: true };
            component.accept(<any>task);
            expect(component.showAcceptModal).toBeTruthy();
        });

        it("open confirmation modal when it has details to reject a task", () => {
            const task = { reject_details: true };
            component.reject(<any>task);
            expect(component.showRejectModal).toBeTruthy();
        });

        it("call api directly when it has no details to accept a task", () => {
            const task = { accept_details: false };
            component.accept(<any>task);
            expect(mocks.taskService.finishTask).toHaveBeenCalled();
        });

        it("call api directly when it has no details to reject a task", () => {
            const task = { accept_details: false };
            component.reject(<any>task);
            expect(mocks.taskService.cancelTask).toHaveBeenCalled();
        });

        it("call cancel and emit event when accept was called successfully", () => {
            component.currentTask = <any>{ id: 1 };
            const result = Observable.of({ id: 1 });
            component['taskService'].finishTask = jasmine.createSpy("finishTask").and.returnValue(result);
            component.cancel = jasmine.createSpy("cancel");
            component.confirmAccept();
            expect(component.cancel).toHaveBeenCalled();
            expect( component['taskService'].taskClosed.next).toHaveBeenCalled();
        });

        it("call cancel and emit event when reject was called successfully", () => {
            component.currentTask = <any>{ id: 1 };
            const result = Observable.of({ id: 1 });
            component['taskService'].cancelTask = jasmine.createSpy("cancelTask").and.returnValue(result);
            component.cancel = jasmine.createSpy("cancel");
            fixture.detectChanges();

            component.confirmReject();
            expect(component.cancel).toHaveBeenCalled();
            expect( component['taskService'].taskClosed.next).toHaveBeenCalled();
        });

        it("reset currentTask and close modal when call cancel", () => {
            component.currentTask = <any>{ id: 1 };
            component.cancel();
            expect(component.showAcceptModal).toBeFalsy();
            expect(component.showRejectModal).toBeFalsy();
            expect(component.currentTask).toBeNull();
        });

        it("not fail when call cancel with no modalInstance", () => {
            component.currentTask = null;
            component.cancel();
        });
    });
});
