import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from './../../shared/services/translator.service';
import { RoleService } from './../../services/role.service';
import { By } from '@angular/platform-browser';
import { TaskAcceptComponent } from './task-accept.component';
import * as helpers from '../../../spec/helpers';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Task } from '../../models/task.model';
import { AddMemberTaskAcceptComponent } from '../types/add-member/add-member-task-accept.component';
import { TaskModule } from './../task.module';

describe("Components", () => {
    describe("Task Accept Component", () => {
        const mocks = helpers.getMocks();
        const task = <Task>{ id: 1, type: "AddMember" };
        let fixture: ComponentFixture<TaskAcceptComponent>;
        let component: TaskAcceptComponent;

        beforeEach(async(() => {
            const taskAcceptComponent = {task: task, confirmationTask: {}};
            TestBed.configureTestingModule({
                declarations: [TaskAcceptComponent],
                providers: [
                    { provide: TranslatorService, useValue: mocks.translatorService },
                    { provide: RoleService, useValue: mocks.roleService },
                ],
                schemas: [NO_ERRORS_SCHEMA],
                imports: [TranslateModule.forRoot(), TaskModule]
            });
            fixture = TestBed.createComponent(TaskAcceptComponent);
            component = fixture.componentInstance;
            component.task = task;
        }));


        it("replace element with the specific task accept component", () => {
            fixture.detectChanges();
            expect(fixture.debugElement.nativeElement.innerHTML).toEqual('');
        });
    });
});
