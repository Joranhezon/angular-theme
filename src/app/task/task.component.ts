import { TaskService } from './../services/task.service';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, Input } from '@angular/core';
import { OnInit } from '@angular/core';
import * as types from './types';
import { Task } from '../models/task.model';

export interface TaskComponentInterface {
    task: Task;
}

@Component({
    selector: "task",
    template: ''
})
export class TaskComponent implements OnInit {

    @Input() task: Task;

    constructor(private viewContainerRef: ViewContainerRef,
        private factory: ComponentFactoryResolver) { }

    ngOnInit() {
        if (TaskService.TASK_TYPES.indexOf(this.task.type) >= 0) {

            const taskComponent = types[`${this.task.type}TaskComponent`];
            const compFactory = this.factory.resolveComponentFactory(taskComponent);
            this.viewContainerRef.clear();
            const componentRef = (this.viewContainerRef.createComponent(compFactory) as ComponentRef<TaskComponentInterface>);
            componentRef.instance.task = this.task;
            componentRef.changeDetectorRef.detectChanges();
        }
    }

}
