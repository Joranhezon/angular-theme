import { Profile } from '../../../models/profile.model';
import { Component, Inject, Input, OnInit } from '@angular/core';

@Component({
    selector: "not-found",
    templateUrl: './not-found.html',
    styleUrls: ['not-found.scss']
})
export class NotFoundComponent {

    @Input() profile: Profile;

    constructor() {}

}


