import { ArticleService } from './../../../services/article.service';
import { By } from '@angular/platform-browser';
import { NotFoundComponent } from './not-found.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import * as helpers from '../../../../spec/helpers';
import { Article } from '../../../models/article.model';
import { TranslateModule } from '@ngx-translate/core';

describe("NotFoundComponent Component", () => {
    let fixture: ComponentFixture<NotFoundComponent>;
    let component: NotFoundComponent;
    const mocks = helpers.getMocks();

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NotFoundComponent],
            providers: [
                { provide: ArticleService, useValue: mocks.articleService },
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            imports: [TranslateModule.forRoot()]
        });
        fixture = TestBed.createComponent(NotFoundComponent);
        component = fixture.componentInstance;
    }));

    it("renders the not found page", () => {
        expect(fixture.debugElement.queryAll(By.css('.not-found')).length).toEqual(1);
    });
});

