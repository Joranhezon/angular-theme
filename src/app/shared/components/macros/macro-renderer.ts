import { Injectable, Injector, ElementRef, ComponentFactoryResolver, ComponentFactory, ComponentRef, Component, ViewEncapsulation } from '@angular/core';
import * as plugins from '../../../../plugins';
import { Article } from '../../../models/article.model';

export interface DynamicHTMLRef {
    check: () => void;
    destroy: () => void;
}

function isBrowserPlatform() {
    return window != null && window.document != null;
}
export abstract class OnMount {
    abstract dynamicMacroParse(object: any, attrs?: Map<string, string>, html?: string, element?: Element): void;
}

@Injectable()
export class MacroRenderer {

    private componentFactories = new Map<string, ComponentFactory<any>>();

    private componentRefs = new Map<any, Array<ComponentRef<any>>>();

    options = [];

    constructor(private cfr: ComponentFactoryResolver, private injector: Injector) {

        this.options = plugins.macros;

        this.options.forEach(({ selector, component }) => {
            let cf: ComponentFactory<any>;
            cf = this.cfr.resolveComponentFactory(component);
            this.componentFactories.set(selector, cf);
        });
    }

    renderInnerHTML(object: any, elementRef: ElementRef, html: string): DynamicHTMLRef {
        elementRef.nativeElement.innerHTML = html;

        const componentRefs: Array<ComponentRef<any>> = [];

        this.options.forEach(({ selector }) => {
            const elements = (elementRef.nativeElement as Element).querySelectorAll(selector);
            Array.prototype.forEach.call(elements, (el: Element) => {
                const content = el.innerHTML;
                const cmpRef = this.componentFactories.get(selector).create(this.injector, [], el);
                // remove `ng-version` attribute
                el.removeAttribute('ng-version');

                if (cmpRef.instance.dynamicMacroParse) {
                    const attrsMap = new Map<string, string>();
                    if (el.hasAttributes()) {
                        Array.prototype.forEach.call(el.attributes, (attr: Attr) => {
                            attrsMap.set(attr.name, attr.value);
                        });
                    }
                    (cmpRef.instance as OnMount).dynamicMacroParse(object, attrsMap, content, el);
                }
                componentRefs.push(cmpRef);
            });
        });
        this.componentRefs.set(elementRef, componentRefs);
        return {
            check: () => componentRefs.forEach(ref => ref.changeDetectorRef.detectChanges()),
            destroy: () => {
                componentRefs.forEach(ref => ref.destroy());
                this.componentRefs.delete(elementRef);
            },
        };
    }
}
