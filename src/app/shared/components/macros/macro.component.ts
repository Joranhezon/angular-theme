import {
    Component,
    ElementRef,
    Input,
    SimpleChanges,
    OnChanges,
    OnDestroy,
    DoCheck,
    ViewEncapsulation,
} from '@angular/core';

import { MacroRenderer, DynamicHTMLRef } from './macro-renderer';
import { Article } from '../../../models/article.model';

@Component({
    selector: 'parse-macros',
    template: '',
})
export class MacroComponent implements DoCheck, OnChanges, OnDestroy {
    @Input() html: string;
    @Input() object: Article;

    private ref: DynamicHTMLRef = null;

    constructor(
        private renderer: MacroRenderer,
        private elementRef: ElementRef,
    ) { }

    ngOnChanges(_: SimpleChanges) {
        if (this.ref) {
            this.ref.destroy();
            this.ref = null;
        }
        if (this.html && this.elementRef) {
            this.ref = this.renderer.renderInnerHTML(this.object, this.elementRef, this.html);
        }
    }

    ngDoCheck() {
        if (this.ref) {
            this.ref.check();
        }
    }

    ngOnDestroy() {
        if (this.ref) {
            this.ref.destroy();
            this.ref = null;
        }
    }
}
