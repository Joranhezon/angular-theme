import { Title } from '@angular/platform-browser';
import { Injectable } from '@angular/core';
import * as _ from "lodash";

import { EnvironmentService } from '../../services/environment.service';
import { Environment } from '../../models/environment.model';

@Injectable()
export class HeaderService {

    environment: Environment;

    constructor(private titleService: Title, private environmentService: EnvironmentService) {

        this.environment = environmentService.getCurrentEnvironment();
        this.environmentService.environmentChangeEvent.subscribe(environment => {
            this.environment = environment;
            this.setTitle();
        });
        this.setTitle();
    }

    setTitle() {
        if (_.isObject(this.environment)) {
            this.titleService.setTitle(this.environment.name);
        }
    }

}
