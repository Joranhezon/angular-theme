import { Title } from '@angular/platform-browser';
import { EnvironmentService } from './../../services/environment.service';
import { EventEmitter } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture, flushMicrotasks } from '@angular/core/testing';
import * as helpers from '../../../spec/helpers';
import { HeaderService } from './header.service';
import { Observable } from 'rxjs/Observable';

describe("Header Service", () => {
    const mocks = helpers.getMocks();
    let service: HeaderService;
    let titleService;
    let environmentService;

    beforeEach(async(() => {
        spyOn(mocks.environmentService, "getCurrentEnvironment").and.returnValue({ id: 1, name: 'Noosfero' });
        TestBed.configureTestingModule({
            providers: [
                HeaderService,
                { provide: EnvironmentService, useValue: mocks.environmentService },
            ]
        });
        service = TestBed.get(HeaderService);
        titleService = TestBed.get(Title);
        environmentService = TestBed.get(EnvironmentService);
        spyOn(titleService, "setTitle");
    }));

    it("should set the header title element", fakeAsync(() => {
        tick();
        service = new HeaderService(titleService, environmentService);
        expect(titleService.setTitle).toHaveBeenCalledWith('Noosfero');
    }));
});
