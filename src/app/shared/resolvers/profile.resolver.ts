import { ProfileService } from './../../services/profile.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Profile } from '../../models/profile.model';

@Injectable()
export class ProfileResolver implements Resolve<Profile> {

    constructor(private profileService: ProfileService, private router: Router) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot ): Observable<Profile>|any {
        return this.profileService.getByIdentifier(route.params["profile"], { optional_fields: ['boxes'] }).subscribe(
            (profile: Profile) => {
                this.profileService.setCurrentProfile(profile);
                return profile;
        }, (error) => {
            this.router.navigate(['/', route.params["profile"], 'not-found']);
        }
    );
    }
}
