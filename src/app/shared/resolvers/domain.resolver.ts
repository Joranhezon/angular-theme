import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Domain } from '../../models/domain.model';
import { DomainService } from './../../services/domain.service';

@Injectable()
export class DomainResolver implements Resolve<Domain> {

    constructor(private domainService: DomainService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Domain>|any {
        return this.domainService.get("context").subscribe((domain: Domain) => {
            return domain;
        });
    }
}
