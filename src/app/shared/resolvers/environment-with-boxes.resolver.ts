import { EnvironmentService } from './../../services/environment.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Environment } from '../../models/environment.model';
import { Box } from '../../models/box.model';

@Injectable()
export class EnvironmentWithBoxesResolver implements Resolve<Environment> {

    constructor(private environmentService: EnvironmentService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Environment>|Promise<Environment>|any {
        let environment: Environment;
        return this.environmentService.get('default', { optional_fields: ['boxes'] }).map((remoteEnvironment: Environment) => {
            environment = remoteEnvironment;
            this.environmentService.setCurrentEnvironment(environment);
            return environment;
        });
    }
}
