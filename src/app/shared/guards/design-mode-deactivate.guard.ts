import { NotificationService } from './../services/notification.service';
import { DesignModeService } from './../services/design-mode.service';
import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DesignModeDeactivateGuard implements CanDeactivate<any> {

    constructor(private notificationService: NotificationService, private designModeService: DesignModeService) { }

    canDeactivate(component: any) {
        if (!this.designModeService.hasChanges) return true;
        return this.notificationService.confirmation({
            title: "contextbar.edition.leave.confirmation.title",
            message: "contextbar.edition.leave.confirmation.message"
        }, () => true, () => false );
    }
}
