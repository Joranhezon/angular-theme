import { Injectable } from '@angular/core';
import {CanActivate} from "@angular/router";

import { SessionService, AuthService } from '../../services';
import { NotificationService } from './../services/notification.service';

@Injectable()
export class OnlyLoggedInUsersGuard implements CanActivate {

  // FIXME remove this code
  // constructor(private sessionService: SessionService, private notificationService: NotificationService) {};
  constructor(private authService: AuthService, private notificationService: NotificationService) {};

  canActivate() {
    if (this.authService.isAuthenticated()) {
      return true;
    } else {
      this.notificationService.error({ message: "auth.login.must_be" });
      return false;
    }
  }
}
