import { AuthService } from '../services/auth.service';
import { NotificationService } from './../shared/services/notification.service';
import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Credentials } from '../models/credentials.model';
import { User } from '../models/user.model';

@Component({
    selector: "noosfero-login",
    templateUrl: './login.html',
    styleUrls: ['./login.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LoginComponent {
    @Input("show") show = false;
    @Output("showChange") showChange = new EventEmitter();

    credentials = <Credentials>{};
    showForgotPasswordModal = false;

    constructor(private authService: AuthService, private notificationService: NotificationService) { }

    login() {
        this.authService.login(this.credentials).subscribe((user: User) => {
            this.authService.loginSuccessCallback(user);
            this.notificationService.info({ title: "auth.login.success.title", message: "auth.login.success.message" });
        },
        (response) => {
            this.notificationService.error({ title: "auth.login.error.title", message: "auth.login.error.message" });
        });
    }

    hide() {
        this.show = false;
        this.showChange.emit(this.show);
    }

    openForgotPassword() {
        this.hide();
        this.showForgotPasswordModal = true;
    }
}
