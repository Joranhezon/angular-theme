import { ActivatedRoute, Router } from '@angular/router';
import { DomainService } from '../services/domain.service';
import { EnvironmentService } from '../services/environment.service';
import { AuthService } from '../services';
import { Component, Inject } from '@angular/core';
import { Environment } from '../models/environment.model';
import { Profile } from '../models/profile.model';
import { Domain } from '../models/domain.model';

@Component({
    selector: 'domain',
    template: "<div></div>",
})
export class DomainComponent {

    owner: Environment | Profile;
    domain: Domain;

    constructor(private domainService: DomainService, private environmentService: EnvironmentService,
        private router: Router, private route: ActivatedRoute) {

        this.domain = route.snapshot.data['domain'];

        this.owner = this.domain.owner;

        if (this.isProfile()) {
            this.router.navigate(['/', (<Profile>this.owner).identifier]);
        } else {
            environmentService.setCurrentEnvironment(<Environment>this.owner);
            this.router.navigate(['/']);
        }
    }

    isEnvironment() {
        return this.owner && this.owner.type === "Environment";
    }

    isProfile() {
        return this.owner && !this.isEnvironment();
    }

}
