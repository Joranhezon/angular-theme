export const MembershipStatus = {
    NotMember: 297,
    WaitingForApproval: 296,
    Member: 295
};

export const FriendshipStatus = {
    NotFriend: 294,
    WaitingForApproval: 293,
    Friend: 292
};
