import { Component, Inject, Input, ViewEncapsulation, OnInit } from '@angular/core';
import * as _ from 'lodash';

import { ProfileService } from '../../services/profile.service';
import { RoleService } from '../../services/role.service';
import { TranslatorService } from '../../shared/services/translator.service';
import { DisplayStyles } from '../profile-list/profile-list.component';
import { Observable } from 'rxjs/Observable';
import { Person } from '../../models/person.model';
import { Profile } from '../../models/profile.model';
import { Role } from '../../models/role.model';

@Component({
    selector: "noosfero-community-members",
    templateUrl: './community-members.html',
    styleUrls: ['./community-members.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CommunityMembersComponent implements OnInit {

    private members: Person[];
    private currentPage: number;
    private membersPerPage: number;
    private totalMembers: number;
    @Input() profile: Profile;

    public loading = false;
    public roles: Role[];

    constructor(private profileService: ProfileService, private roleService: RoleService, public translatorService: TranslatorService) {
        this.roles = [];
        this.members = [];
        this.membersPerPage = 20;
        this.totalMembers = 0;
    }

    ngOnInit() {
        this.roleService.getByProfile(this.profile.id).subscribe((roles: Role[]) => {
            this.roles = roles;
            this.orderRoles();
        });

        this.loadByRole({ page: 1 }, null);

    }

    orderRoles() {
        let max = _.size(this.roles);
        this.roles = _.map(this.roles, function(obj) {
            if (obj.key ===  'profile_member') {
                obj.position = 1;
            }else if (obj.key ===  'profile_admin') {
                obj.position = 2;
            }else {
                obj.position = max;
                max -= 1;
            }
            return obj;
        });
        this.roles = _.orderBy(this.roles, 'position');

    }

    loadByRole($event: any, role: Role) {
        this.loading = true;

        const filters = {
            per_page: this.membersPerPage,
            page: $event.page || 1,
            order: 'name ASC'
        };

        this.profileService.getMembersByRole(this.profile, role, filters).subscribe((result: any) => {
            this.totalMembers = <number>(<any>result.headers).get("total");
            this.members = result.body;
            this.loading = false;
        });

    }

    getTranslationKey(role: Role) {
        let name: string;
        const key = 'role.' + role.key + '.plural';
        if (this.translatorService.hasTranslation(key)) {
            name = key;
        } else {
            name = role.name;
        }
        return name;
    }


}
