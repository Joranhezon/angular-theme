import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';

import { Profile } from '../../../app/models/profile.model';

@Component({
    selector: 'community-members-route',
    templateUrl: './community-members-profile.html'
})
export class CommunityMembersProfileComponent implements OnInit {

    public profile: Profile;

    constructor(private profileService: ProfileService) { }

    ngOnInit() {
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });
    }
}
