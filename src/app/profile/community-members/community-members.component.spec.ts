import { TranslateModule } from '@ngx-translate/core';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { PaginationModule } from 'ngx-bootstrap';

import { TranslatorService } from './../../shared/services/translator.service';
import { ProfileService } from './../../services/profile.service';
import { RoleService } from './../../services/role.service';
import { ProfileImageComponent } from './../../profile/image/profile-image.component';
import { CommunityMembersComponent } from './community-members.component';
import * as helpers from '../../../spec/helpers';
import { Person } from '../../models/person.model';
import { Role } from '../../models/role.model';

describe("Components", () => {

    describe("Community Members Component", () => {
        let fixture: ComponentFixture<CommunityMembersComponent>;
        const mocks = helpers.getMocks();

        let component: CommunityMembersComponent;
        const state = jasmine.createSpyObj("$state", ["href"]);

        const currentProfile = { id: 1 };
        const members = [{ id: 1 }, { id: 2 }];

        beforeEach(async(() => {
            spyOn(mocks.profileService, "getMembersByRole").and.callThrough();


            TestBed.configureTestingModule({
                declarations: [CommunityMembersComponent],
                providers: [
                    { provide: ProfileService, useValue: mocks.profileService },
                    { provide: TranslatorService, useValue: mocks.translatorService },
                    { provide: RoleService, useValue: mocks.roleService }
                ],
                imports: [ TranslateModule.forRoot()],
                schemas: [NO_ERRORS_SCHEMA]
            });
            fixture = TestBed.createComponent(CommunityMembersComponent);
            component = fixture.componentInstance;
            component.profile =  <Person>{ id: 1 };
        }));

        it("load profile members when load page", () => {
            fixture.detectChanges();
            expect(mocks.profileService.getMembersByRole).toHaveBeenCalled();
        });

        it("loadByRole with default parameters", () => {
            const role = <Role>{};
            const event = {page: 1};
            component.loadByRole(event, role);

            expect(mocks.profileService.getMembersByRole).toHaveBeenCalledWith({ id: 1 }, role, { page: 1, per_page: 20, order: 'name ASC' });
        });

    });
});
