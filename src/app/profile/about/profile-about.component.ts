import { Component, Inject, ViewEncapsulation } from '@angular/core';

import { ProfileService } from '../../services/profile.service';
import { Profile } from '../../../app/models/profile.model';

@Component({
    selector: 'profile-about',
    templateUrl: './profile-about.html',
    styleUrls: ['./profile-about.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileAboutComponent {

    profile: Profile;
    profileFields: any;

    constructor(profileService: ProfileService) {

        this.profile = profileService.getCurrentProfile();
        this.loadProfileFields();
    }

    loadProfileFields() {
    const fields = ['name', 'email', 'formation', 'schooling', 'nationality', 'country', 'state', 'city'];
        this.profileFields = {};
        for (const field of fields) {
            if (this.profile[field]) {
                this.profileFields[field] = this.profile[field];
            }
        }
    }

    hasCustomFields() {
        return this.profile.additional_data && Object.keys(this.profile.additional_data).length > 0;
    }
}
