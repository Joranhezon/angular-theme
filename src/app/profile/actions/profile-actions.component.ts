import { Component, Input, Inject, ViewEncapsulation } from '@angular/core';

import { ArticleService } from '../../services/article.service';
import { Profile } from '../../models/profile.model';
import { Article } from '../../models/article.model';

@Component({
    selector: "profile-actions",
    templateUrl: './profile-actions.html',
    styleUrls: ['./profile-actions.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileActionsComponent {

    @Input() profile: Profile;
    article: Article;
    parentId: number;

    constructor(articleService: ArticleService) {
        this.article = articleService.getCurrent();
        this.parentId = this.getArticleContainer(this.article);
    }

    getArticleContainer(article: Article) {
        // FIXME get folder types from api
        if (article && (article.type === "Blog" || article.type === "Folder")) {
            return article.id;
        } else if (article && article.parent) {
            return article.parent.id;
        }
        return null;
    }
}
