import { Inject, Input, Component, ViewEncapsulation } from '@angular/core';

import { ProfileService } from '../../services/profile.service';
import { PermissionService } from '../../shared/services/permission.service';
import { Profile } from '../../../app/models/profile.model';

@Component({
    selector: "noosfero-top-profile-image",
    templateUrl: './top-profile-image.html',
    styleUrls: ['./top-profile-image.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class TopProfileImageComponent {

    @Input() profile: Profile;
    @Input() editable: boolean;
    picFile: any;

    constructor(private profileService: ProfileService,
        private permissionService: PermissionService) { }

    upload(data: any) {
        this.profileService.uploadImage(this.profile, data, 'top').subscribe((profile: Profile) => {
            this.profile.top_image = profile.top_image;
        });
    }

    hasTopImage() {
        return this.profile && this.profile.top_image;
    }

    isEditable() {
        return this.editable && this.permissionService.isAllowed(this.profile, 'allow_edit');
    }
}
