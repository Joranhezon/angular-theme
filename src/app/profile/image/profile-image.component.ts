import { SessionService } from './../../services/session.service';
import { NotificationService } from './../../shared/services/notification.service';
import { Inject, Input, Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';
import { PermissionService } from '../../shared/services/permission.service';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { NoosferoKnownEvents } from '../../known-events';
import { Profile } from '../../models/profile.model';
import { Person } from '../../models/person.model';

/**
 * @ngdoc controller
 * @name components.profile-image.ProfileImage
 * @description The component responsible for rendering the profile image
 * @exports ProfileImage
 */
@Component({
    selector: "noosfero-profile-image",
    templateUrl: './profile-image.html',
    styleUrls: ['./profile-image.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileImageComponent implements OnInit {

    @Input() profile: Profile;
    defaultIcon: string;

    @Input() iconSize: string;
    @Input() editable: boolean;

    nullImageFromApi = false;

    constructor(private profileService: ProfileService, private permissionService: PermissionService,
        private notificationService: NotificationService, private sessionService: SessionService, ) {
    }

    upload(data: any) {
        this.profileService.uploadImage(this.profile, data, "profile").subscribe((profile: Profile) => {
            this.profileService.profileImageChangeEvent.emit(profile);
            const currentPerson = this.sessionService.currentPerson();
            if (currentPerson.id === profile.id) {
                this.sessionService.setPerson(<Person>profile)
            }
            this.notificationService.success({ title: "profile.image.upload.success.title", message: "profile.image.upload.success.message" });
        });
    }

    isEditable() {
        return this.editable && this.profile && this.permissionService.isAllowed(this.profile, 'allow_edit');
    }

    ngOnInit() {
        this.iconSize = "fa-5x";
        this.defaultIcon = 'fa-users';
        if (this.profile && this.profile.type === 'Person') {
            this.defaultIcon = 'fa-user';
        }

        this.profileService.profileImageChangeEvent.subscribe((profile: Profile) => {
            if (this.profile.id === profile.id) {
                this.profile = profile;
            }
        });
    }

    showAlternativeProfileIcon() {
        return this.nullImageFromApi || !this.profile.image;
    }

}
