import { Component, Input, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { ProfileService } from '../../services/profile.service';
import { Router } from '@angular/router';

import { Profile } from '../../../app/models/profile.model';
import { Activity } from '../../models/activity.model';

const LIMIT = 10;
@Component({
    selector: "noosfero-activities",
    templateUrl: './activities.html',
    styleUrls: ['./activities.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ActivitiesComponent implements OnInit {

    activities: any;
    page = 1;
    profile: Profile;
    hasActivities = false;

    constructor(private profileService: ProfileService, private router: Router) { }

    ngOnInit() {
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });

        let responseObservable = null;
        if (this.isCommunity()) {
            responseObservable = this.profileService.getActivities(this.profile, { page: this.page });
        } else {
            responseObservable = this.profileService.getNetworkActivities(this.profile, { page: this.page });
        }

        responseObservable.subscribe((activities: Activity[]) => {
            this.activities = activities;
            if (this.activities.length > 0) {
                this.hasActivities = true;
            }
        }, (response: any) => {
            if (response.status === 403) {
                this.router.navigate(['/profile', this.profile.identifier, 'about']);
            }
        });
    }

    viewMore() {
        this.page++;
        this.profileService.getNetworkActivities(this.profile, { page: this.page }).subscribe((activities: Activity[]) => {
            this.hasActivities = (activities.length > 0) ? true : false;
            activities.forEach((value, key) => {
                this.activities.push(value);
            });
        });
    }

    isCommunity(): boolean {
        return this.profile.type === 'Community';
    }
}
