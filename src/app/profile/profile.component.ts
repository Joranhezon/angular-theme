import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, Input, ViewEncapsulation, OnInit, AfterContentChecked, AfterViewChecked } from '@angular/core';

import { ThemeService } from './../shared/services/theme.service';
import { DesignModeService } from './../shared/services/design-mode.service';
import { ProfileHomeComponent } from './profile-home.component';
import { CmsComponent } from '../article/cms/cms.component';
import { ContentViewerComponent } from '../article/content-viewer/content-viewer.component';
import { ActivitiesComponent } from './activities/activities.component';
import { ProfileAboutComponent } from './about/profile-about.component';
import { ProfileService } from '../services/profile.service';
import { NotificationService } from '../shared/services/notification.service';
import { TasksComponent } from '../task/tasks/tasks.component';
import { DestroyProfileComponent } from './destroy/destroy-profile.component';
import { ProfileActionsComponent } from './actions/profile-actions.component';
import { AuthService, AuthEvents } from './../services';
import { Profile } from '../../app/models/profile.model';
import { User } from '../models/user.model';

@Component({
    selector: 'profile',
    templateUrl: './profile.html',
    styleUrls: ['./profile.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileComponent implements OnInit {

    public profile: Profile;

    constructor(private profileService: ProfileService, private route: ActivatedRoute,
        private notificationService: NotificationService, private designModeService: DesignModeService,
        public authService: AuthService, private router: Router, private themeService: ThemeService) {

        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });
    }

    ngOnInit() {

        if (this.profile && this.themeService.verifyTheme(this.profile.theme)) return;

        this.designModeService.setInDesignMode(false);

        this.authService.loginSuccess.subscribe(() => {
            const currentProfile = this.profileService.getCurrentProfile();
            this.profileService.getByIdentifier(currentProfile.identifier, { optional_fields: ['boxes'] }).subscribe((profile: Profile) => {
                this.profile = profile;
            });
        });

        this.authService.logoutSuccess.subscribe(() => {
            this.profileService.getByIdentifier(this.profile.identifier, { optional_fields: ['boxes'] }).subscribe((profile: Profile) => {
                this.profile = profile;
                this.profile.permissions = undefined;
            });
        });

    }
}
