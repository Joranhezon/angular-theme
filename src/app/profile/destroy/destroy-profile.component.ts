import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, Inject } from '@angular/core';

import { NotificationService } from '../../shared/services/notification.service';
import { AuthService, ProfileService } from '../../services';

import { Profile } from '../../../app/models/profile.model';

@Component({
    selector: 'destroy-profile',
    template: "<div></div>",
})
export class DestroyProfileComponent {

    constructor(private notificationService: NotificationService,
        private profileService: ProfileService, private router: Router,
        private authService: AuthService, route: ActivatedRoute, private location: Location) {

        const profile = this.profileService.getCurrentProfile();

        notificationService.confirmation({ title: "profile.remove.confirmation.title", message: "profile.remove.confirmation.message" }, () => {
            profileService.remove(profile).subscribe((response: any) => {
                // FIXME refactor this method
                if (response.success) {
                    this.handleSuccess(profile);
                } else {
                    this.handleError(profile);
                }
            }, () => {
                this.handleError(profile);
            });
        }, () => {
            this.location.back();
        });
    }

    handleSuccess(profile: Profile) {
        this.router.navigate(['/']);
        if (profile && profile.type !== "Community") {
            this.authService.logout().subscribe(() => {
                this.authService.logoutSuccessCallback();
            });
        }
        this.notificationService.success({ title: "profile.remove.success.title", message: "profile.remove.success.message" });
    }

    handleError(profile: Profile) {
        this.router.navigate(['/myprofile', profile.identifier]);
        this.notificationService.error({ title: "profile.remove.failed.title" });
    }
}
