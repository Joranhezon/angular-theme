import { Router, ActivatedRoute } from '@angular/router';
import { ViewChild } from '@angular/core';
import { ProfileService } from './../../../services/profile.service';
import { TranslatorService } from './../../../shared/services/translator.service';
import { NotificationService } from './../../../shared/services/notification.service';
import { CommunityService } from './../../../services/community.service';
import { Component, Input, Output, Inject, EventEmitter } from '@angular/core';
import { ValidationMessageComponent } from '../../../shared/components/validation-message/validation-message.component';
import { Profile } from '../../../models/profile.model';
import { Community } from '../../../models/community.model';

export abstract class AbstractFormCommunity {
    @Input() public profile: Profile;
    public community: Community;
    @Output() public finished = new EventEmitter<Community>();
    @ViewChild('nameErrors') nameErrors: ValidationMessageComponent;

    public errors: any;
    public acceptBefore = true;
    public sessionProfile: Profile;

    constructor(@Inject(NotificationService) public notificationService: NotificationService,
        @Inject(CommunityService) public communityService: CommunityService,
        @Inject(ProfileService) public profileService: ProfileService,
        @Inject(TranslatorService) public translatorService: TranslatorService,
        @Inject(Router) protected router: Router,
        @Inject(ActivatedRoute) protected route: ActivatedRoute) {}

    abstract getTitle();
    abstract save();
    abstract cancel();

    onSelectionChange(entry) {
        this.community.closed = entry;
    }

}
