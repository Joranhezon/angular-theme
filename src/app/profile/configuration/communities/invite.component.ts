import { Component, Inject, Input, NgZone, ViewEncapsulation, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';


import { PersonService } from '../../../services/person.service';
import { CommunityService } from '../../../services/community.service';
import { ProfileService } from '../../../services/profile.service';
import { TranslatorService } from '../../../shared/services/translator.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { Profile } from '../../../../app/models/profile.model';
import { Person } from '../../../models/person.model';
import { DefaultResponse } from '../../../models/default-response.model';

@Component({
    selector: "noosfero-invite-component",
    templateUrl: './invite.html',
    styleUrls: ['./invite.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class InviteComponent implements OnInit {

    @Input() profile;

    public peopleToInvite: Person[];
    public listOfPeople: any[];
    public searchToken: string;

    constructor(private personService: PersonService,
        private communityService: CommunityService,
        private profileService: ProfileService,
        private translatorService: TranslatorService,
        public notificationService: NotificationService,
        private zone: NgZone) {
        this.peopleToInvite = [];
        this.listOfPeople = Observable.create((observer: any) => {
            observer.next(this.searchToken);
        }).mergeMap((token: string) => this.searchPerson(token));
    }

    ngOnInit() {
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });
    }

    sendInvitations() {
        this.communityService.sendInvitations(this.profile, this.peopleToInvite).subscribe((response: DefaultResponse) => {
            if (response.success) {
                this.zone.run(() => {
                    this.peopleToInvite = [];
                    this.searchToken = null;
                    this.notificationService.success({ title: "invite.send.success.title", message: "invite.send.success.message" });
                });
            }
        }, error => {
            this.notificationService.error({ title: "invite.send.error.title", message: "invite.send.error.message" });
        });
    }

    markToInvite(person) {
        this.zone.run(() => {
            if (person && person.id) {
                if (this.peopleToInvite.filter(e => e.id === person.id).length === 0) {
                    this.peopleToInvite.push(person);
                }
            }
            this.searchToken = null;
        });
    }

    unmarkToInvite(person) {
        this.peopleToInvite.splice(this.peopleToInvite.indexOf(person), 1);
    }

    searchPerson(key: any) {
        const filters = { search: key, per_page: 10 };
        return this.personService.search(filters).map(data => {
            if (data.length === 0) {
                return [{ name: this.translatorService.translate("profile.members.invitations.none") }];
            } else {
                return data;
            }
        });
    }

}
