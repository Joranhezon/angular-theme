import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { NotificationService } from './../../../shared/services/notification.service';
import { TranslatorService } from './../../../shared/services/translator.service';
import { CommunityService } from './../../../services/community.service';
import { SessionService } from './../../../services/session.service';
import { ProfileService } from './../../../services/profile.service';
import { ValidationMessageComponent } from './../../../shared/components/validation-message/validation-message.component';
import { NewCommunityComponent } from './new-community.component';
import { By } from '@angular/platform-browser';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import * as helpers from '../../../../spec/helpers';
import { FormsModule, NG_VALIDATORS, AbstractControl, NgForm, FormControl } from '@angular/forms';
import { Community } from '../../../models/community.model';
import { Profile } from '../../../models/profile.model';
import { Observable } from 'rxjs/Observable';

describe("Components", () => {
    describe("New Community", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<NewCommunityComponent>;
        let component: NewCommunityComponent;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RouterTestingModule, FormsModule, TranslateModule.forRoot()],
                declarations: [NewCommunityComponent, ValidationMessageComponent],
                providers: [
                    { provide: ProfileService, useValue: mocks.profileService },
                    { provide: CommunityService, useValue: mocks.communityService },
                    { provide: NotificationService, useValue: mocks.notificationService },
                    { provide: TranslatorService, useValue: mocks.translatorService },
                    { provide: Router, useValue: mocks.router },
                    { provide: ActivatedRoute, useValue: mocks.route },
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            });
            fixture = TestBed.createComponent(NewCommunityComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
        }));


        it("verify if community is initialized with close true ", () => {
            component.community = undefined;
            component.ngOnInit();
            expect(component.community).toEqual(<Community>{ closed: true });
        });

        it("verify if translate service is called with correct parameters ", () => {
            spyOn(component.translatorService, 'translate');
            component.getTitle();
            expect(component.translatorService.translate).toHaveBeenCalledWith('myprofile.configuration.community.new');
        });

        it("verify if changes the page after cancel is called ", () => {
            spyOn(TestBed.get(Router), 'navigate');
            component.cancel();
            expect(TestBed.get(Router).navigate).toHaveBeenCalledWith(['/myprofile', mocks.profile.identifier, 'communities']);
        });

        it("verify if community type is defined after save is called ", () => {
            expect(component.community.type).toBeUndefined();
            component.save();
            expect(component.community.type).toEqual('Community');
        });

        it("verify if the method createNewCommunity of community service is called after save ", () => {
            spyOn(component.communityService, 'createNewCommunity').and.callThrough();
            component.save();
            expect(component.communityService.createNewCommunity).toHaveBeenCalledWith(component.community);
        });

        it("verify if notification service success is called after community task is successfuly created ", fakeAsync(() => {
            spyOn(component.notificationService, 'success').and.callThrough();
            component.save();
            tick();
            expect(component.notificationService.success).toHaveBeenCalledWith({ title: "profile.community.new.task.success.title", message: "profile.community.new.task.success.message" });
        }));

        it("verify if notification service success is called after community is successfuly created ", fakeAsync(() => {
            spyOn(component.notificationService, 'success').and.callThrough();
            spyOn(component.communityService, 'createNewCommunity').and.returnValue(Observable.of({id: 2}));
            component.save();
            tick();
            expect(component.notificationService.success).toHaveBeenCalledWith({ title: "profile.community.new.success.title", message: "profile.community.new.success.message" });
        }));

        it("verify if changes page after community is successfuly created ", fakeAsync(() => {
            spyOn(TestBed.get(Router), 'navigate');
            component.save();
            tick();
            expect(TestBed.get(Router).navigate).toHaveBeenCalled();
        }));

        it("verify if set name error when the save is rejected by the server ", fakeAsync(() => {
            const response = { status: 422, error: { errors: { name: [{ error: 'blank', full_message: 'cant be blank' }] } } };
            const fn = () => Observable.throw(response);
            component.communityService.createNewCommunity = jasmine.createSpy("createNewCommunity").and.callFake(fn);
            fixture.detectChanges();
            component.save();
            tick();
            expect(component.nameErrors.getErrors()[0]).toEqual('profile.edition.name.blank');
        }));

        it("verify if set identifier error when the save is rejected by the server ", fakeAsync(() => {
            spyOn(component.notificationService, 'error').and.callThrough();
            const response = { status: 422, error: { message: 'Failed', errors: { identifier: [{ error: 'not_available' }] }, errors_messages: { identifier: [{ error: 'not_available' }] } } };
            const fn = () => Observable.throw(response);
            component.communityService.createNewCommunity = jasmine.createSpy("createNewCommunity").and.callFake(fn);

            fixture.detectChanges();
            component.save();
            tick();
            expect(component.nameErrors.getErrors()[0]).toEqual('profile.edition.name.not_available');
        }));

        it("verify if the server could not save the community ", fakeAsync(() => {
            spyOn(component.notificationService, 'error').and.callThrough();
            const response = { status: 400, message: 'Failed' };
            const fn = () => Observable.throw(response);
            component.communityService.createNewCommunity = jasmine.createSpy("createNewCommunity").and.callFake(fn);
            fixture.detectChanges();
            component.save();
            tick();
            expect(component.notificationService.error).toHaveBeenCalledWith({ title: "profile.community.new.error.title", message: 'Failed' });
        }));
    });
});
