import { ActivatedRoute } from '@angular/router';
import { Component, Input, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as _ from "lodash";

import { PersonService } from './../../../services/person.service';
import { DisplayStyles } from '../../profile-list/profile-list.component';
import { Profile } from '../../../models/profile.model';
import { Task } from '../../../models/task.model';
import { Community } from '../../../models/community.model';
import { ProfileService } from '../../../services';
import { TaskService } from '../../../services/task.service';

@Component({
    selector: "person-communities",
    templateUrl: './person-communities.html',
    styleUrls: ['./person-communities.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PersonCommunitiesComponent implements OnInit {

    profile: Profile;
    communities: Community[];
    search: string;
    private currentPage = 1;
    private perPage = 20;
    private total: number;
    searchChanged: Subject<string> = new Subject<string>();
    private displayStyle: string = DisplayStyles.card;
    public loading = false;

    constructor(private personService: PersonService, route: ActivatedRoute, private profileService: ProfileService,
        private taskService: TaskService) {
        this.search = route.snapshot.queryParams['search'];
        this.profile = profileService.getCurrentProfile();

        this.searchChanged.debounceTime(300).subscribe((search: string) => {
            this.search = search;
            this.loadPage({page: this.currentPage});
        });
        this.taskService.taskClosed.subscribe((task: Task) => {
            this.loadPage({ page: 1 });
        });
    }

    ngOnInit() {
        this.loadPage({ page: 1 });
    }

    loadPage($event: any) {
        this.loading = true;

        const filters = {
            per_page: this.perPage,
            page: $event.page,
            order: 'name ASC'
        };
        if (!_.isNil(this.search)) {
            filters['search'] = this.search;
        }

        this.personService.getCommunities(this.profile, filters).subscribe((result: any) => {
            this.total = <number>(<any>result.headers).get("total");
            this.communities = result.body;
            this.loading = false;
        });
    }

    searchCommunities() {
        this.loadPage({page: this.currentPage});
    }

    getDisplayStyle() {
        return this.displayStyle;
    }

}
