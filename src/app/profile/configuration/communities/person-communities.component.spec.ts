import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';
import { TranslatorService } from './../../../shared/services/translator.service';
import { PersonCommunitiesComponent } from './person-communities.component';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { tick, fakeAsync, async, TestBed, ComponentFixture } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { PersonService } from '../../../services/person.service';
import * as helpers from '../../../../spec/helpers';
import { Profile } from '../../../models/profile.model';
import { Observable } from 'rxjs/Observable';
import { ProfileService } from '../../../services';
import { TaskService } from './../../../services/task.service';

describe("Components", () => {
    describe("Person Communities Component", () => {
        let fixture: ComponentFixture<PersonCommunitiesComponent>;
        let component: PersonCommunitiesComponent;

        const stateParams = {};
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            spyOn(mocks.personService, "getCommunities").and.callThrough();

            TestBed.configureTestingModule({
                imports: [RouterTestingModule, FormsModule, TranslateModule.forRoot()],
                declarations: [PersonCommunitiesComponent],
                schemas: [NO_ERRORS_SCHEMA],
                providers: [
                    { provide: PersonService, useValue: mocks.personService },
                    { provide: TaskService, useValue: mocks.taskService },
                    { provide: ProfileService, useValue: mocks.profileService },
                    { provide: ActivatedRoute, useValue: mocks.route },
                    { provide: TranslatorService, useValue: mocks.translatorService }
                ]
            });
            fixture = TestBed.createComponent(PersonCommunitiesComponent);
            component = fixture.componentInstance;
            component.profile = <Profile>{ id: 1 };
        }));

        it("load first page of communities on init", () => {
            fixture.detectChanges();
            expect(mocks.personService.getCommunities).toHaveBeenCalledWith(component.profile, { per_page: 20, page: 1, order: 'name ASC' });
        });

        it("load communities when save", () => {
            component.searchCommunities();
            expect(mocks.personService.getCommunities).toHaveBeenCalledWith(component.profile, { per_page: 20, page: 1, order: 'name ASC' });
        });

        it("search for communities when change search text", (fakeAsync(() => {
            component.searchChanged.next("john");
            tick(300);
            expect(component.search).toEqual("john");
            expect(mocks.personService.getCommunities).toHaveBeenCalledWith(component.profile, { per_page: 20, page: 1, search: "john", order: 'name ASC' });
        })));

        it("has default display style equal to *card*", () => {
            expect(component.getDisplayStyle()).toEqual('card');
        });

    });
});
