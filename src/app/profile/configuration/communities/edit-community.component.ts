import { NotificationService } from './../../../shared/services/notification.service';
import { Component, Input, Output, Inject, EventEmitter, ViewEncapsulation, OnInit, ViewChild } from '@angular/core';
import { AbstractFormCommunity } from './abstract-form-community';
import * as _ from "lodash";
import { Community } from '../../../models/community.model';

@Component({
    selector: "edit-community",
    templateUrl: './form-community.html',
    styleUrls: ['./form-community.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class EditCommunityComponent extends AbstractFormCommunity implements OnInit {
    @ViewChild('communityForm') form;

    ngOnInit() {
        this.community = <Community>this.profile;
        this.acceptBefore = this.community.closed;
        this.nameErrors.pushAditionalField('identifier');
    }

    getTitle() {
        return this.translatorService.translate('myprofile.configuration.community.edit');
    }

    save() {
        this.form.submitted = true;

        const profile: any = Object.assign({}, _.omitBy(_.pick(this.community, ['id', 'identifier', 'name', 'closed']), _.isNull));
        this.profileService.update(profile).subscribe((result) => {
            this.notificationService.success({ title: "profile.edition.success.title", message: "profile.edition.success.message" });
        }, (error) => {
            const errors = error.error;
            if (error.status === 422) {
                this.nameErrors.setBackendErrors(errors);
            } else {
                this.notificationService.error({ title: "profile.edition.error.title", message: error.message });
            }
        });
    }

    cancel() { }

}
