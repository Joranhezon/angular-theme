import { Component, Input, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/profile.service';

import { Profile } from '../../../../app/models/profile.model';

@Component({
    selector: 'noosfero-community-members-my-profile',
    templateUrl: './community-members-my-profile.html',
    styleUrls: ['./community-members-my-profile.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CommunityMembersMyProfileComponent implements OnInit {

    profile: Profile;

    constructor(private profileService: ProfileService) { }

    ngOnInit() {
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });
    }

}
