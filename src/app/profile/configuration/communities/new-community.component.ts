import { NotificationService } from './../../../shared/services/notification.service';
import { CommunityService } from './../../../services/community.service';
import { Component, Input, Output, Inject, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { AbstractFormCommunity } from './abstract-form-community';
import { Community } from '../../../models/community.model';
import * as _ from "lodash";

@Component({
    selector: "new-community",
    templateUrl: './form-community.html',
})
export class NewCommunityComponent extends AbstractFormCommunity implements OnInit {
    @ViewChild('communityForm') form;

    ngOnInit() {
        this.community = <Community>{ closed: true };
        this.nameErrors.pushAditionalField('identifier');

        this.profile = this.profileService.getCurrentProfile();
    }

    getTitle() {
        return this.translatorService.translate('myprofile.configuration.community.new');
    }

    save() {
        this.form.submitted = true;
        this.community.type = 'Community';
        this.communityService.createNewCommunity(this.community).subscribe((community: Community) => {
            if (_.isNil(community.id)) {
                this.notificationService.success({ title: "profile.community.new.task.success.title", message: "profile.community.new.task.success.message" });
            } else {
                this.notificationService.success({ title: "profile.community.new.success.title", message: "profile.community.new.success.message" });
            }

            this.router.navigate(['/myprofile', this.profile.identifier, 'communities']);
        }, (error) => {
            const errors = error.error;
            if (error.status === 422) {
                this.nameErrors.setBackendErrors(errors);
            } else {
                this.notificationService.error({ title: "profile.community.new.error.title", message: error.message });
            }
        });
    }

    cancel() {
        this.router.navigate(['/myprofile', this.profile.identifier, 'communities']);
    }
}
