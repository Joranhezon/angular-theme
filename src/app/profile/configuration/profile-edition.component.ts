import { Component } from '@angular/core';

import { ProfileService } from '../../services/profile.service';
import { Profile } from '../../../app/models/profile.model';

@Component({
    selector: "profile-edition",
    templateUrl: './profile-edition.html'
})
export class ProfileEditionComponent {

    profile: Profile;

    constructor(private profileService: ProfileService) {
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });
    }

    isPerson() {
        return this.profile.type === "Person";
    }

    isCommunity() {
        return this.profile.type === "Community";
    }
}
