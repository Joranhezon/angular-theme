import { ActivatedRoute, Router } from '@angular/router';
import { Component, Inject, ViewEncapsulation } from '@angular/core';

import { ProfileService } from './../../services/profile.service';
import { SessionService } from './../../services';
import { Profile } from '../../../app/models/profile.model';

/**
 * @ngdoc controller
 * @name NoosferoActivities
 * @description
 *  The controller responsible to profile configuration.
 */
@Component({
    selector: "noosfero-profile-configuration",
    templateUrl: './profile-configuration.html',
    styleUrls: ['./profile-configuration.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileConfigurationComponent {

    profile: Profile;

    constructor(private profileService: ProfileService, route: ActivatedRoute, router: Router) {
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });

    }
}
