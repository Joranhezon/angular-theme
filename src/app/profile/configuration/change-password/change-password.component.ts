import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from './../../../shared/services/notification.service';
import { Component, Input, Inject, Output, ViewChild, EventEmitter, ViewEncapsulation } from '@angular/core';
import { ValidationMessageComponent } from '../../../shared/components/validation-message/validation-message.component';
import { Profile } from '../../../models/profile.model';
import { AuthService, ProfileService } from '../../../services';


/**
 * @ngdoc controller
 * @name ChangePasswordComponent
 * @description
 *  The controller responsible to change the user password.
 */
@Component({
    selector: "change-password",
    templateUrl: './change-password.html',
    styleUrls: ['./change-password.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ChangePasswordComponent {

    profile: Profile;

    @ViewChild('currentPasswordValidation') currentPassword: ValidationMessageComponent;
    @ViewChild('newPasswordValidation') newPassword: ValidationMessageComponent;
    @ViewChild('newPasswordConfirmationValidation') newPasswordConfirmation: ValidationMessageComponent;

    current_password: string;
    new_password: string;
    new_password_confirmation: string;
    errors: any;

    constructor(private authService: AuthService, private notificationService: NotificationService,
        private profileService: ProfileService, private router: Router) {
        this.profile = this.profileService.getCurrentProfile();

    }

    save(event: Event) {
        event.preventDefault();
        if (this.new_password !== this.new_password_confirmation) {
            this.newPasswordConfirmation.setBackendErrors({errors: { password_confirmation: [{error: 'profile.edition.password.error.message'}] }});
            return false;
        } else {
            this.authService.changePassword(this.profile, this.current_password, this.new_password, this.new_password_confirmation)
                .subscribe(response => {
                    this.errors = null;
                    this.notificationService.success({ title: "profile.edition.success.title", message: "new_password.success.message" });
                    this.router.navigate(['/myprofile', this.profile.identifier]);
                }, error => {
                    this.errors = error.error;
                    this.currentPassword.setBackendErrors(this.errors);
                    this.newPassword.setBackendErrors(this.errors);
                    this.newPasswordConfirmation.setBackendErrors(this.errors);
                });
        }
    }
}
