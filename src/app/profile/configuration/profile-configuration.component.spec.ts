import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from './../../services/profile.service';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ProfileConfigurationComponent } from './profile-configuration.component';
import * as helpers from '../../../spec/helpers';
import { Profile } from '../../../app/models/profile.model';

describe("Components", () => {
    describe("Profile Configuration Component", () => {
        const mocks = helpers.getMocks();
        let fixture: ComponentFixture<ProfileConfigurationComponent>;
        let component: ProfileConfigurationComponent;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RouterTestingModule, TranslateModule.forRoot()],
                declarations: [ProfileConfigurationComponent],
                providers: [
                    { provide: ProfileService, useValue: mocks.profileService },
                    { provide: ActivatedRoute, useValue: mocks.route },
                ],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            });
            fixture = TestBed.createComponent(ProfileConfigurationComponent);
            component = fixture.componentInstance;
        }));

        it("set profile", () => {
            expect(component.profile).toEqual(<Profile>mocks.profile);
        });
    });
});
