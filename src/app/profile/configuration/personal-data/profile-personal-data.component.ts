import { PersonalDataDictionary } from './personal-data-dictionary';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslatorService } from './../../../shared/services/translator.service';
import { NotificationService } from './../../../shared/services/notification.service';
import { ProfileService } from './../../../services/profile.service';
import { AuthService } from './../../../services/auth.service';
import { SessionService } from './../../../services/session.service';
import { Component, Input, Inject, Output, ViewChild, EventEmitter, ViewEncapsulation, OnInit } from '@angular/core';
import { Profile } from '../../../models/profile.model';
import { Person } from '../../../models/person.model';
import { EnvironmentService } from './../../../services/environment.service';
import * as _ from "lodash";
import { ValidationMessageComponent } from './../../../shared/components/validation-message/validation-message.component';

@Component({
    selector: "profile-personal-data",
    templateUrl: './profile-personal-data.html',
    styleUrls: ['./profile-personal-data.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfilePersonalDataComponent implements OnInit {

    @Input() profile: Profile;
    @Output() finished = new EventEmitter<Profile>();

    @ViewChild('emailErrors') emailErrors: ValidationMessageComponent;
    @ViewChild('nameErrors') nameErrors: ValidationMessageComponent;

    customFieldsDict: PersonalDataDictionary;
    updatedProfile: Profile;
    allowChangeIdentifier: boolean;

    constructor(private profileService: ProfileService, private authService: AuthService,
        private sessionService: SessionService, private environmentService: EnvironmentService,
        private notificationService: NotificationService, private translatorService: TranslatorService,
        private router: Router) {
        this.allowChangeIdentifier = this.environmentService.allowChangeIdentifier();
        this.updatedProfile = <Profile>{}

    }

    ngOnInit() {
        this.updatedProfile = Object.assign(this.updatedProfile, this.profile);
        this.emailErrors.pushAditionalField('user');
    }

    save() {
        const identifierChanged = this.profile.identifier !== this.updatedProfile.identifier;

        this.profileService.update(this.parseProfileFields(this.updatedProfile)).subscribe(() => {
            const currentPerson = this.sessionService.currentPerson();
            this.profile = Object.assign(this.profile, this.updatedProfile);
            if (currentPerson && (currentPerson.identifier === this.profile.identifier)) {
                this.sessionService.setPerson(<Person>this.profile)
            }

            this.finished.emit(this.profile);
            this.notificationService.success({ title: "profile.edition.success.title", message: "profile.edition.success.message" });
            if (identifierChanged) {
                this.router.navigate(['/', this.profile.identifier]); // go to the state with the new identifier url
            }
        }, (response) => {
            const errors = response.error;
            this.nameErrors.setBackendErrors(errors);
            this.emailErrors.setBackendErrors(errors);
        });

    }

    parseProfileFields(profile: Profile): Profile {

        const attrs = ['id', 'name', 'email'];
        if (this.allowChangeIdentifier) {
            attrs.push('identifier')
        }

        return <Profile>attrs.reduce((object, key) => {
            object[key] = profile[key];
            return object;
        }, {});

    }

    keys(): Array<string> {
        return Object.keys(this.customFieldsDict);
    }

    translateLabel(label) {
        const translation = this.translatorService.translate('profile.edition.' + label);
        return translation.indexOf('profile.edition.') >= 0 ? label : translation;
    }

    fieldType(label) {
        return label.indexOf('date') >= 0 ? 'date' : 'text';
    }
}
