import { ActivatedRoute } from '@angular/router';
import { Component, Input, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as _ from "lodash";

import { PersonService } from './../../../services/person.service';
import { DisplayStyles } from '../../profile-list/profile-list.component';
import { Profile } from '../../../models/profile.model';
import { Person } from '../../../models/person.model';
import { ProfileService } from '../../../services';

@Component({
    selector: "person-friends",
    templateUrl: './person-friends.html',
    styleUrls: ['./person-friends.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PersonFriendsComponent implements OnInit {

    profile: Profile;
    friends: Person[];
    search: string;
    private currentPage = 1;
    private perPage = 20;
    private total: number;
    public loading = false;
    searchChanged: Subject<string> = new Subject<string>();
    private displayStyle: string = DisplayStyles.card;

    constructor(private personService: PersonService, route: ActivatedRoute, private profileService: ProfileService) {
        this.search = route.snapshot.queryParams['search'];
        this.searchChanged.debounceTime(300).subscribe((search: string) => {
            this.search = search;
            this.loadPage({ page: this.currentPage });
        });
        this.profile = profileService.getCurrentProfile();

    }

    ngOnInit() {
        this.loadPage({ page: 1 });
    }

    loadPage($event: any) {
        this.loading = true;
        const filters = {
            per_page: this.perPage,
            page: $event.page,
            order: 'name ASC'
        };
        if (!_.isNil(this.search)) {
            filters['search'] = this.search;
        }
        this.personService.getFriends(this.profile, filters).subscribe((result: any) => {
            this.total = <number>result.headers.get("total");
            this.friends = result.body;
            this.loading = false;
        });
    }

    searchFriends() {
        this.loadPage({ page: this.currentPage });
    }

    getDisplayStyle() {
        return this.displayStyle;
    }

}
