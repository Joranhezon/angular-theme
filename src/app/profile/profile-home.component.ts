import { Router } from '@angular/router';
import { Component, Inject } from '@angular/core';

import { ProfileService } from '../services/profile.service';
import { Profile } from '../../app/models/profile.model';
import { Article } from '../../app/models/article.model';

@Component({
    selector: 'profile-home',
    template: "<div></div>",
})
export class ProfileHomeComponent {

    profile: Profile;

    constructor(profileService: ProfileService, router: Router) {
        this.profile = profileService.getCurrentProfile();
        profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });

        profileService.getHomePage(this.profile, { fields: 'path' }).subscribe((article: Article) => {
            if (article && article.path) {
                this.profile.homepage = article.path;
                router.navigate(['/' + this.profile.identifier + '/' + article.path], { skipLocationChange: true });
            } else {
                this.profile.homepage = null;
                router.navigate(['/profile', this.profile.identifier, 'about']);
            }
        });
    }
}
