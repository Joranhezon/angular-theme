import { SessionService } from './../../services/session.service';
import { Inject, Input, Component, EventEmitter, Output, ViewEncapsulation, OnInit } from '@angular/core';
import * as _ from "lodash";

import { ProfileService } from '../../services/profile.service';
import { MembershipStatus, FriendshipStatus } from '../../../app/noosfero-status';
import { NotificationService } from '../../shared/services/notification.service';
import { EventsHubService } from '../../shared/services/events-hub.service';
import { NoosferoKnownEvents } from '../../known-events';
import { CommunityService } from '../../services/community.service';
import { PersonService } from './../../services/person.service';
import { Profile } from '../../models/profile.model';
import { DefaultResponse } from '../../models/default-response.model';
import { User } from '../../models/user.model';
import { Person } from '../../models/person.model';

@Component({
    selector: 'profile-join',
    templateUrl: './profile-join.html',
    styleUrls: ['./profile-join.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileJoinComponent implements OnInit {

    @Input() profile: Profile;
    private relationshipState: number;
    currentPerson: Person;


    constructor(private profileService: ProfileService, private personService: PersonService,
        private session: SessionService, private notificationService: NotificationService,
        private eventsHubService: EventsHubService, private communityService: CommunityService) {
        this.currentPerson = this.session.currentPerson();
    }

    ngOnInit() {
        if (this.isPerson()) {
            this.eventsHubService.subscribeToEvent(this.eventsHubService.knownEvents.PROFILE_FRIENDSHIP_CHANGED, (friendshipState: number) => {
                this.relationshipState = friendshipState;
            });
        } else {
            this.eventsHubService.subscribeToEvent(this.eventsHubService.knownEvents.PROFILE_MEMBERSHIP_CHANGED, (membershipState: number) => {
                this.relationshipState = membershipState;
            });
        }
        this.loadRelationship();
    }

    loadRelationship() {
        if (!this.session.currentPerson()) {
            return
        }
        if (this.isPerson()) {
            this.personService.getFriendshipState(this.session.currentPerson(), this.profile).subscribe(
                (response: DefaultResponse) => {
                    this.eventsHubService.emitEvent(this.eventsHubService.knownEvents.PROFILE_FRIENDSHIP_CHANGED, response.code);
                },
                (error) => {
                    console.log(error)
                }
            );
        } else {
            this.communityService.getMembershipState(this.session.currentPerson(), this.profile).subscribe(
                (response: DefaultResponse) => {
                    this.eventsHubService.emitEvent(this.eventsHubService.knownEvents.PROFILE_MEMBERSHIP_CHANGED, response.code);
                },
                (error) => {
                    console.log(error);
                }
            );
        }
    }

    addRelationship() {
        let observableResponse;
        if (this.isPerson()) {
            observableResponse = this.personService.addFriend(this.profile);
        } else {
            observableResponse = this.profileService.addMember(this.session.currentPerson(), this.profile);

        }
        observableResponse.subscribe(
            (defaultResponse: DefaultResponse) => {
                if (_.includes([MembershipStatus.WaitingForApproval, FriendshipStatus.WaitingForApproval], defaultResponse.code)) {
                    this.notificationService.success({ title: 'profile.actions.add_in_' + this.profileType() + '.title', message: 'profile.actions.add_in_' + this.profile.type.toLowerCase() + '.moderation.message' });
                } else {
                    this.notificationService.success({ title: 'profile.actions.add_in_' + this.profileType() + '.title', message: 'profile.actions.add_in_' + this.profile.type.toLowerCase() + '.success.message' });
                }
                this.loadRelationship();
            }, (response: any) => {
                this.notificationService.error({ title: 'profile.actions.add_in_' + this.profileType() + '.error.title', message: 'profile.actions.add_in_' + this.profile.type.toLowerCase() + '.error.message' });
            }
        );
    }

    removeRelationship() {
        let promiseResponse;
        if (this.isPerson()) {
            promiseResponse = this.personService.removeFriend(this.profile);

        } else {
            promiseResponse = this.profileService.removeMember(this.session.currentPerson(), this.profile);
        }
        promiseResponse.subscribe((
            response: any) => {
            this.notificationService.success({ title: 'profile.actions.remove_of_' + this.profileType() + '.title', message: 'profile.actions.remove_of_' + this.profileType() + '.message' });
            this.loadRelationship();
        }, (response: any) => {
            this.notificationService.error({ title: 'profile.actions.remove_of_' + this.profileType() + '.title', message: 'profile.actions.remove_of_' + this.profileType() + '.error.message' });
        });
    }

    isPerson(): boolean {
        return this.profile.type === 'Person';
    }

    profileType(): string {
        return this.profile.type.toLowerCase();
    }

    hasNoRelationship(): boolean {
        const isNotFriend = (this.relationshipState === FriendshipStatus.NotFriend) && !this.isTheProfileOwner();
        const isNotMember = this.relationshipState === MembershipStatus.NotMember;
        return (isNotMember) || (isNotFriend);
    }

    hasRelationship(): boolean {
        const isMember = this.relationshipState === MembershipStatus.Member;
        const isFriend = (this.relationshipState === FriendshipStatus.Friend) && !this.isTheProfileOwner();
        return (isMember) || (isFriend);
    }

    isTheProfileOwner(): boolean {
        return (this.currentPerson) && (this.currentPerson.id === this.profile.id)
    }

    isWaitingApproval(): boolean {
        const isWaitingFriendApproval = this.relationshipState === FriendshipStatus.WaitingForApproval;
        const isWaitingCommunityApproval = this.relationshipState === MembershipStatus.WaitingForApproval;
        return (isWaitingCommunityApproval) || (isWaitingFriendApproval);
    }

}
