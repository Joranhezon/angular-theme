import { Router } from '@angular/router';
import { ValidationMessageComponent } from './../../shared/components/validation-message/validation-message.component';
import { NotificationService } from './../../shared/services/notification.service';
import { ProfileService } from './../../services/profile.service';
import { Component, Inject, Input, Output, EventEmitter, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { Profile } from '../../models/profile.model';
import { Person } from '../../models/person.model';
import { Environment } from '../../models/environment.model';
import { SessionService } from './../../services/session.service';
import { EnvironmentService } from './../../services/environment.service';

@Component({
    selector: "profile-fast-edition",
    templateUrl: './profile-fast-edition.html',
    styleUrls: ['./profile-fast-edition.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileFastEditionComponent implements OnInit {

    @Input() profile: Profile;
    @Input() environment: Environment;
    @Output() finished = new EventEmitter<Profile>();

    @ViewChild('identifierErrors') identifierErrors: ValidationMessageComponent;
    @ViewChild('nameErrors') nameErrors: ValidationMessageComponent;

    updatedProfile: Profile;
    allowChangeIdentifier: boolean;

    constructor(private profileService: ProfileService, private notificationService: NotificationService,
        private sessionService: SessionService, private environmentService: EnvironmentService,
        private router: Router) {
        this.allowChangeIdentifier = this.environmentService.allowChangeIdentifier();
        this.environmentService.environmentChangeEvent.subscribe(environment => {
            this.allowChangeIdentifier = this.environmentService.allowChangeIdentifier();
        });
    }

    ngOnInit() {
        this.cloneProfile();
    }

    save() {
        this.profileService.update(this.updatedProfile).subscribe(() => {
            const identifierChanged = this.profile.identifier !== this.updatedProfile.identifier;

            const currentPerson = this.sessionService.currentPerson();
            if (currentPerson && (currentPerson.identifier === this.profile.identifier)) {
                this.profile = Object.assign(this.profile, this.updatedProfile);
                this.sessionService.setPerson(<Person>this.profile)
            } else {
                this.profile = Object.assign(this.profile, this.updatedProfile);
            }

            this.notificationService.success({ title: "profile.edition.success.title", message: "profile.edition.success.message" });
            this.finished.emit(this.profile);
            if (this.allowChangeIdentifier && identifierChanged) {
                this.router.navigate(['/', this.profile.identifier]); // go to the state with the new identifier url
            }
        }, (response) => {
            const errors = response.error;
            if (this.allowChangeIdentifier) {
                this.identifierErrors.setBackendErrors(errors);
            }
            this.nameErrors.setBackendErrors(errors);
        });
    }

    cancel() {
        this.finished.emit(this.profile);
    }

    cloneProfile() {
        const fields = ['id', 'name'];
        if (this.allowChangeIdentifier) {
            fields.push('identifier');
        }
        this.updatedProfile = <Profile>fields.reduce((object, key) => {
            object[key] = this.profile[key]; return object;
        }, {});
    }

    getProfileLink() {
        if (!this.environment || !this.environment.host || !this.profile) return null;
        const host = this.environment.host.replace(/https?:\/\//, "");
        return `${host}/${this.updatedProfile.identifier}`;
    }

    identifierChanged() {
        return this.updatedProfile.identifier !== this.profile.identifier;
    }

}
