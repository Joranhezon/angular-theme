import { Inject, Input, Component, HostListener, ElementRef, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';

import { NotificationService } from './../../shared/services/notification.service';
import { DesignModeService } from './../../shared/services/design-mode.service';
import { PersonService } from './../../services/person.service';
import { EnvironmentService } from '../../services/environment.service';
import { ProfileJoinComponent } from './../../profile/profile-join/profile-join.component';
import { AuthService, AuthEvents } from './../../services';
import { Environment } from '../../models/environment.model';
import { Profile } from '../../models/profile.model';
import { User } from '../../models/user.model';


@Component({
    selector: "noosfero-profile-summary",
    templateUrl: './profile-summary.html',
    styleUrls: ['./profile-summary.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProfileSummaryComponent implements OnInit {

    @Input() profile: Profile;
    environment: Environment;
    currentUser: User;
    editPopoverOpen = false;
    designMode = false;
    @ViewChild("popover") popover;

    constructor(private elementRef: ElementRef, private environmentService: EnvironmentService,
        private notificationService: NotificationService, public authService: AuthService,
        private designModeService: DesignModeService) {

        this.environment = environmentService.getCurrentEnvironment();

        this.currentUser = this.authService.currentUser();

        this.authService.loginSuccess.subscribe((user: User) => {
            this.currentUser = user;
        });

        this.authService.logoutSuccess.subscribe(() => {
            this.currentUser = null;
        });
    }

    ngOnInit() {
        this.designModeService.onToggle.subscribe((designModeOn: boolean) => {
            this.designMode = designModeOn;
        });
        this.designMode = this.designModeService.isInDesignMode();
    }

    profileLink() {
        if (!this.environment || !this.environment.host || !this.profile) return null;
        const host = this.environment.host.replace(/https?:\/\//, "");
        return `${host}/${this.profile.identifier}`;
    }

    closeEdition() {
        this.editPopoverOpen = false;
        this.popover.hide();
    }

    @HostListener('document:click', ['$event'])
    onClick($event: any) {
        if (this.popover && !this.elementRef.nativeElement.contains($event.target)) {
            this.popover.hide();
        }
    }

}
