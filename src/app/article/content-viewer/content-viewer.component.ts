import { Input, Component } from '@angular/core';
import { Location } from '@angular/common';
import * as _ from "lodash";

import { ArticleService } from '../../services/article.service';
import { ProfileService } from '../../services/profile.service';
import { AuthService, AuthEvents } from './../../services';
import { Profile } from '../../../app/models/profile.model';
import { Article } from '../../models/article.model';
import { Router, NavigationStart } from '@angular/router';
import { NotificationService } from '../../shared/services/notification.service';

@Component({
    selector: "content-viewer",
    templateUrl: './page.html',
})
export class ContentViewerComponent {

    @Input()
    article: Article = null;

    @Input()
    profile: Profile = null;
    public loading = false;

    constructor(private articleService: ArticleService, private profileService: ProfileService,
        public authService: AuthService, private location: Location,
        private router: Router, private notificationService: NotificationService) {

        this.activate();

        this.authService.loginSuccess.subscribe(() => {
            this.activate();
        });
        this.authService.logoutSuccess.subscribe(() => {
            this.activate();
        });

        // FIXME see if it's needed
        // router.events.subscribe((event) => {
        //     if (event instanceof NavigationStart) {
        //         this.activate(event.url);
        //     }
        // });
    }

    isNotFoundPage(): boolean {
        return (!this.article && !this.loading);
    }

    activate(url = '') {
        this.loading = true;

        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });

        if (_.isNil(url) || url === '') {
            url = this.location.path();
        }

        const page = _.replace(url, '/' + this.profile.identifier + '/', '');

        this.articleService.getArticleByProfileAndPath(this.profile, page).subscribe(
            (article: Article) => {
                this.article = article;
                this.articleService.setCurrent(this.article);
                this.loading = false;
                window.scrollTo(0, 0);
            }, (error: any ) => {
                if (error.status === 403) {
                    this.notificationService.error({ title: "notification.error.default.title", message: "notification.http_error.403.message" });
                } else if (error.status === 404) {

                }
                this.loading = false;
        });
    }
}
