import { Observable } from 'rxjs/Observable';
import { NgPipesModule } from 'ngx-pipes';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { MomentModule } from 'angular2-moment';
import { By } from '@angular/platform-browser';

import { ArticleService } from './../../../services/article.service';
import { DateFormatPipe } from './../../../shared/pipes/date-format.pipe';
import { FolderComponent } from './folder.component';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';
import * as helpers from '../../../../spec/helpers';

describe("Folder Component", () => {
    let fixture: ComponentFixture<FolderComponent>;
    let component: FolderComponent;
    const mocks = helpers.getMocks();
    const article1 = <Article>{ id: 1, title: 'The article test' };
    const article2 = <Article>{ id: 1, title: 'The article test' };
    const articles = [ article1, article2 ];

    beforeEach(async(() => {
        spyOn(mocks.articleService, "getChildren").and.returnValue(Observable.of({ body: articles, headers: { get: (attr: String) => 2 }}));
        TestBed.configureTestingModule({
            declarations: [FolderComponent, DateFormatPipe],
            providers: [
                { provide: ArticleService, useValue: mocks.articleService },
                { provide: "amParseFilter", useValue: mocks.amParseFilter }
            ],
            schemas: [NO_ERRORS_SCHEMA],
            imports: [NgPipesModule, MomentModule]
        });
        fixture = TestBed.createComponent(FolderComponent);
        component = fixture.componentInstance;
        component.article = <Article>{};
        component.profile = <Profile>{};
        component['posts'] = articles;
    }));

    it("renders the folder content", () => {
        fixture.detectChanges();
        expect(fixture.debugElement.queryAll(By.css('.folder .media')).length).toEqual(2);
    });
});
