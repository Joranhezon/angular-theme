import { Component, Input, Inject, ViewEncapsulation, OnInit, OnChanges } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { ArticleService } from '../../../services/article.service';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';


@Component({
    selector: "noosfero-folder",
    templateUrl: './folder.html',
    styleUrls: ['folder.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class FolderComponent implements OnInit, OnChanges {

    @Input() article: Article;
    @Input() profile: Profile;

    private posts: Article[];
    private perPage = 5;
    private currentPage: number;
    private totalPosts = 0;
    public loading = false;

    constructor(private articleService: ArticleService) {}

    ngOnInit() {
        this.loadPage({ page: 1 });
    }

    ngOnChanges() {
        this.loadPage({ page: 1 });
    }

    loadPage($event: any) {
        this.loading = true;

        const filters = {
            per_page: this.perPage,
            page: $event.page
        };
        this.articleService.getChildren(this.article, filters).subscribe((result: any) => {
            this.totalPosts = <number>(<any>result.headers).get("total");
            this.posts = result.body;
            this.loading = false;
        });
    }
}
