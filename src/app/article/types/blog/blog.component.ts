import { Component, Input, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import * as _ from "lodash";

import { ArticleService } from '../../../services/article.service';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';

/**
 * @ngdoc controller
 * @name ArticleBlog
 * @description
 *  An specific {@link ArticleView} for Blog articles.
 */
@Component({
    selector: "noosfero-blog",
    templateUrl: './blog.html',
    styleUrls: ['blog.scss']
})
export class ArticleBlogComponent implements OnInit {

    @Input() article: Article;
    @Input() profile: Profile;

    private posts: Article[];
    private perPage = 3;
    private currentPage: number;
    private totalPosts = 0;
    public loading = false;

    constructor(private articleService: ArticleService) { }

    ngOnInit() {
        this.loadPage({ page: 1 });
    }

    isEmpty(): boolean {
        return _.toString(this.totalPosts) === '0';
    }

    loadPage($event: any) {
        this.loading = true;
        const filters = {
            content_type: "TextArticle",
            per_page: this.perPage,
            page: $event.page
        };

        this.articleService.getChildren(this.article, filters).subscribe((result: any) => {
            this.totalPosts = <number>(<any>result.headers).get("total");
            this.posts = result.body;
            this.loading = false;
        });
    }

}
