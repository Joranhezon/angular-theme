import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Input, Inject, Component, Directive } from '@angular/core';

import {ArticleService} from '../../../services/article.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: 'noosfero-default-article',
    templateUrl: './article-default.html',
})
export class ArticleDefaultViewComponent {

    @Input() article: Article;
    @Input() profile: Profile;

    constructor(public articleService: ArticleService, public notificationService: NotificationService,
        private sanitizer: DomSanitizer, private router: Router) { }

    delete() {
        this.notificationService.confirmation({ title: "article.remove.confirmation.title", message: "article.remove.confirmation.message" }, () => {
            this.articleService.remove(this.article).subscribe(() => {
                this.articleService.articleRemoved.next(this.article);
                if (this.article.parent) {
                    this.router.navigate(['/' + this.article.profile.identifier + '/' + this.article.parent.path]);
                } else {
                    this.router.navigate(['/' + this.article.profile.identifier]);
                }
                this.notificationService.success({ title: "article.remove.success.title", message: "article.remove.success.message" });
            });
        });
    }
}
