import { ActivatedRoute, Router } from '@angular/router';
import { EventsHubService } from './../../shared/services/events-hub.service';
import { Component, Inject, ViewEncapsulation } from '@angular/core';

import { ArticleService } from '../../services/article.service';
import { ProfileService } from '../../services/profile.service';
import { NotificationService } from '../../shared/services/notification.service';
import { BasicOptionsComponent } from './basic-options/basic-options.component';
import { Profile } from '../../../app/models/profile.model';
import { Article } from '../../models/article.model';

@Component({
    selector: 'article-cms',
    templateUrl: './cms.html',
    styleUrls: ['./cms.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CmsComponent {

    article: Article;
    parent: Article = <Article>{};
    profile: Profile;

    id: number;
    parentId: number;
    loading: boolean | number;
    path: string;


    constructor(private articleService: ArticleService, private profileService: ProfileService,
        private notificationService: NotificationService, @Inject("Window") private window: Window,
        private eventsHubService: EventsHubService, private route: ActivatedRoute, private router: Router) {

        this.parentId = route.snapshot.queryParams['parent_id'];
        this.id = route.snapshot.params['id'];

        this.path = window.location.pathname;
        this.profile = this.profileService.getCurrentProfile();

        this.profileService.profileChangeEvent.subscribe(profile => {
            this.profile = profile;
        });

        if (this.parentId) {
            this.articleService.get(this.parentId).subscribe((article: Article) => {
                this.parent = article;
            });
        }
        if (this.id) {
            this.articleService.get(this.id).subscribe((article: Article) => {
                this.article = article;
                this.article.name = this.article.title; // FIXME
            });
        } else {
            this.article = <Article>{ type: route.snapshot.queryParams['type'] || "TextArticle", published: true };
        }
    }

    save() {
        this.loading = true;
        const profile = this.profileService.getCurrentProfile();

        let observableArticle = null;
        if (this.id) {
            observableArticle = this.articleService.updateArticle(this.article);
        } else if (this.parentId) {
            observableArticle = this.articleService.createInParent(this.parentId, this.article);
        } else {
            observableArticle = this.articleService.createInProfile(profile, this.article);
        }

        observableArticle.subscribe((article: Article) => {
            this.router.navigate([article.profile.identifier + '/' + article.path]);
            this.articleService.articleCreated.next(this.article);
            this.notificationService.success({ message: `article.basic_editor.${article.type.replace(/.*::/, '')}.success.message` });
        }, (error: any) => {
            this.loading = false;
            this.eventsHubService.emitEvent(this.eventsHubService.knownEvents.ARTICLE_SAVE_ERROR, error);
            this.notificationService.error({ message: "article.basic_editor.save.failed" });
        });
    }

    cancel() {
        this.window.history.back();
    }

}
