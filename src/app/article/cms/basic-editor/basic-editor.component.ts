import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Article } from '../../../models/article.model';

@Component({
    selector: 'article-basic-editor',
    templateUrl: './basic-editor.html',
    styleUrls: ['./basic-editor.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class BasicEditorComponent {

    @Input() article: Article;
    @Input() options: any;
}
