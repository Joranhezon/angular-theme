import { SessionService } from './../../../services/session.service';
import { Component, Input, Inject, OnInit } from '@angular/core';
import { Article } from '../../../models/article.model';
import { Profile } from '../../../models/profile.model';

@Component({
    selector: 'article-editor',
    templateUrl: './article-editor.html'
})
export class ArticleEditorComponent implements OnInit {

    @Input() article: Article;
    @Input() profile: Profile;
    @Input() path: string;
    options: any;

    constructor(@Inject("Window") private window: Window,
        private sessionService: SessionService) { }

    ngOnInit() {
        this.window['uploadUrl'] = '/api/v1/profiles/' + this.profile.id + '/articles';
        this.window['listUrl'] = '/api/v1/profiles/' + this.profile.id + '/articles?content_type=Folder,UploadedFile&parent_id=';
        this.window['deleteUrl'] = '/api/v1/articles';
        this.window['renameUrl'] = '/api/v1/articles';
        this.window['privateToken'] = this.sessionService.currentUser().private_token;
        this.window['baseUrl'] = this.getDomanWithPort();
        this.options = { allowedContent: true, removeFormatAttributes: '', filebrowserBrowseUrl: '/ngx-filemanager?editor=CKEditor'};
    }

    getDomanWithPort() {
        return location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '');
    }
}
