import { Inject, Input, Component, ChangeDetectorRef, ViewEncapsulation, OnInit, Output } from '@angular/core';
import { CommentService } from '../../services/comment.service';
import { CommentComponent } from './comment.component';
import { Comment } from '../../models/comment.model';
import { Article } from '../../models/article.model';

@Component({
    selector: 'noosfero-comments',
    templateUrl: './comments.html',
    styleUrls: ['./comments.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CommentsComponent implements OnInit {

    comments: Comment[] = [];
    @Input() showForm = true;
    @Input() article: Article;
    @Input() parent: Comment;
    @Input() fullPagination = false;
    protected page = 1;
    protected perPage = 5;
    protected total = 0;

    newComment = <Comment>{};

    constructor(protected commentService: CommentService) { }

    ngOnInit() {
        if (this.parent) {
            this.comments = this.parent.replies;
        } else {
            this.loadNextPage();
        }
    }

    @Output()
    commentAdded(comment: Comment): void {
        comment.__show_reply = false;
        if (comment.reply_of) {
            this.comments.forEach((commentOnList) => {
                if (commentOnList.id === comment.reply_of.id) {
                    if (commentOnList.replies) {
                        commentOnList.replies.push(comment);
                    } else {
                        commentOnList.replies = [comment];
                    }
                }
            });
        }
        this.comments = [...this.comments, comment];
        this.resetShowReply();
    }

    commentRemoved(comment: Comment): void {
        const index = this.comments.indexOf(comment, 0);
        if (index >= 0) {
            this.comments.splice(index, 1);
        }
        this.comments = [...this.comments];
    }

    private resetShowReply() {
        this.comments.forEach((comment: Comment) => {
            comment.__show_reply = false;
        });
        if (this.parent) {
            this.parent.__show_reply = false;
        }

    }

    loadComments() {
        return this.commentService.getByArticle(this.article, { page: this.page, per_page: this.perPage });
    }

    loadNextPage() {
        this.loadComments().subscribe((result: any) => {
            if (this.fullPagination) {
                this.comments = result.body;
            } else {
                this.comments = this.comments.concat(result.body);
                this.page++;
            }
            this.total = <number>(<any>result.headers).get("total");
        });
    }

    displayMore() {
        return !this.parent && !this.fullPagination && this.getPages() >= this.page;
    }

    displayFullPagination() {
        return !this.parent && this.fullPagination && this.getPages() > 1;
    }

    private getPages() {
        return Math.ceil(this.total / this.perPage);
    }
}
