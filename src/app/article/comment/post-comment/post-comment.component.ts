import { Inject, Input, Output, EventEmitter, Component, ViewEncapsulation } from '@angular/core';
import * as _ from "lodash";

import { CommentService } from '../../../services/comment.service';
import { NotificationService } from '../../../shared/services/notification.service';
import { SessionService } from './../../../services/session.service';
import { Article } from '../../../models/article.model';
import { Comment } from '../../../models/comment.model';
import { User } from '../../../models/user.model';

@Component({
    selector: 'noosfero-post-comment',
    templateUrl: './post-comment.html',
    styleUrls: ['./post-comment.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class PostCommentComponent {

    public static EVENT_COMMENT_RECEIVED = "comment.received";

    @Input() article: Article;
    @Input() parent: Comment;
    @Output() commentSaved: EventEmitter<Comment> = new EventEmitter<Comment>();
    @Input() comment = <Comment>{};
    private currentUser: User;

    constructor(private commentService: CommentService, private notificationService: NotificationService,
        private session: SessionService) {
        this.currentUser = this.session.currentUser();
    }

    save() {
        if (this.parent && this.comment) {
            this.comment.reply_of_id = this.parent.id;
        }
        this.commentService.createInArticle(this.article, this.comment).subscribe((comment: Comment) => {
            this.comment.body = "";
            // FIXME see if it's needed this event
            this.commentSaved.next(comment);

            const emitChange = _.isNil(this.comment.id) ? this.commentService.commentCreated : this.commentService.commentUpdated;
            emitChange.next(comment);
            this.notificationService.success({ title: "comment.post.success.title", message: "comment.post.success.message" });
        });
    }

    loggedIn() {
        return this.currentUser != null;
    }
}
