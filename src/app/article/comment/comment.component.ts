import { Inject, Input, Component, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

import { CommentService } from '../../services/comment.service';
import { NotificationService } from '../../shared/services/notification.service';
import { Article } from '../../models/article.model';
import { Comment } from '../../models/comment.model';
import { DefaultResponse } from '../../models/default-response.model';

@Component({
    selector: 'noosfero-comment',
    templateUrl: './comment.html',
    styleUrls: ['./comment.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class CommentComponent {

    @Input() comment: Comment;
    @Input() article: Article;
    @Input() displayActions = true;
    @Input() displayReplies = true;

    showReply() {
        return this.comment && this.comment.__show_reply === true;
    }

    constructor(private commentService: CommentService, private notificationService: NotificationService) { }

    reply() {
        this.comment.__show_reply = !this.comment.__show_reply;
    }

    allowRemove() {
        return true;
    }

    remove() {
        this.notificationService.confirmation({ title: "comment.remove.confirmation.title", message: "comment.remove.confirmation.message" }, () => {
            this.commentService.removeFromArticle(this.article, this.comment).subscribe((response: DefaultResponse) => {
                this.commentService.commentRemoved.next(this.comment);
                this.notificationService.success({ title: "comment.remove.success.title", message: "comment.remove.success.message" });
            });
        });
    }
}
