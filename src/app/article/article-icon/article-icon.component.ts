import { Component, Input, ViewEncapsulation } from '@angular/core';

import { Article } from '../../models/article.model';

@Component({
    selector: "noosfero-article-icon",
    templateUrl: "./article-icon.html",
    styleUrls: ['article-icon.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ArticleIconComponent {

    @Input() article: Article;

    iconClass() {
        return this.article.type.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase().replace(/::/, '-');
    }
}
