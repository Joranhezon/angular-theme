import { ThemeService } from './../shared/services/theme.service';
import { EnvironmentService } from '../services/environment.service';
import { AuthEvents, AuthService } from '../services';
import { NotificationService } from '../shared/services/notification.service';
import { DesignModeService } from './../shared/services/design-mode.service';
import { Component, Inject, ViewEncapsulation, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Environment } from '../models/environment.model';
import { Box } from '../models/box.model';

@Component({
    selector: 'environment',
    templateUrl: './environment.html',
    styleUrls: ['./environment.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class EnvironmentComponent implements OnInit {

    boxes: Box[];
    public environment: Environment;

    constructor(private environmentService: EnvironmentService, private notificationService: NotificationService,
        private authService: AuthService, private designModeService: DesignModeService,
        private themeService: ThemeService, private router: Router, private route: ActivatedRoute) {
            this.environment = this.environmentService.getCurrentEnvironment();

            this.environmentService.environmentChangeEvent.subscribe(environment => {
                this.environment = environment;
            });

        }

    ngOnInit() {

        if (this.environment && this.themeService.verifyTheme(this.environment.theme)) return;
        this.designModeService.setInDesignMode(false);

        this.authService.loginSuccess.subscribe(() => {
            this.environmentService.get('default', { optional_fields: ['boxes'] }).subscribe((environment: Environment) => {
                this.environment = environment;
            });
        });
        this.authService.logoutSuccess.subscribe(() => {
            this.environmentService.get('default', { optional_fields: ['boxes'] }).subscribe((environment: Environment) => {
                this.environment = environment;
                this.environment.permissions = undefined;
            });
        });
    }
}
