import { DomSanitizer } from '@angular/platform-browser';
import { Component } from '@angular/core';
import { EnvironmentService } from '../services/environment.service';
import { Environment } from '../../app/models/environment.model';

@Component({
    selector: 'environment-home',
    templateUrl: './environment-home.html',
})
export class EnvironmentHomeComponent {

    environment: Environment;

    constructor(private environmentService: EnvironmentService, private sanitizer: DomSanitizer) {
        this.environment = environmentService.getCurrentEnvironment();
    }

    getEnvironmentDescription(): any {
        if (this.environment && this.environment.settings && this.environment.settings.description) {
            return this.sanitizer.bypassSecurityTrustHtml(this.environment.settings.description);
        } else {
            return '';
        }
    }
}
