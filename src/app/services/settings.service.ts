import { Injectable, Inject } from '@angular/core';

import { BackendBaseService } from './backend_base.service';
import { Profile } from '../models/profile.model';
import { Environment } from '../models/environment.model';
import { Observable } from 'rxjs/Observable';
import { BlockDefinition } from '../models/block-definition.model';

@Injectable()
export class SettingsService extends BackendBaseService {

    getAvailableBlocks(owner: Profile | Environment): Observable<BlockDefinition[]> {
        const context = (owner.type === 'Environment') ? 'environments' : 'profiles';

        return this.http.get<BlockDefinition[]>(this.baseURL + '/' + context + '/' + owner.id + '/settings/available_blocks');
    }
}
