import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpResponse } from '@angular/common/http';

import { Task } from '../models/task.model';
import { BackendBaseService } from './backend_base.service';

@Injectable()
export class TaskService extends BackendBaseService {

    public static TASK_TYPES = ["AddMember", "ApproveComment", "ApproveArticle",
        "AbuseComplaint", "SuggestArticle", "CreateCommunity", "AddFriend"];

    taskClosed: EventEmitter<Task> = new EventEmitter<Task>();

    get(taskId: number): Observable<Task> {
        return this.http.get<Task>(this.baseURL + '/tasks/' + taskId);
    }

    getAllPending(params: any = {}): Observable<HttpResponse<Task[]>> {
        params['all_pending'] = true;
        if (!params['content_type']) {
            params['content_type'] = TaskService.TASK_TYPES.map(t => t).join(',');
        }
        params['status'] = 1;

        return this.http.get<Task[]>(this.baseURL + '/tasks', {observe: 'response', params: this.parseParams(params)});
    }

    finishTask(task: Task): Observable<Task> {
        return this.closeTask(task, "finish");
    }

    cancelTask(task: Task): Observable<Task> {
        return this.closeTask(task, "cancel");
    }

    closeTask(task: Task, action: string): Observable<Task> {
        return this.http.put<Task>(this.baseURL  + '/tasks/' + task.id + '/' + action, {});
    }
}
