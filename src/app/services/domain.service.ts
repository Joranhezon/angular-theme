import { Injectable } from '@angular/core';

import { BackendBaseService } from './backend_base.service';
import { Domain } from '../models/domain.model';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DomainService extends BackendBaseService {


    get(domainId: number | string): Observable<Domain> {
        return this.http.get<Domain>(this.baseURL + '/domains/' + domainId);
    }

}
