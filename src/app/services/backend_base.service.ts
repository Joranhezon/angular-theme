import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable()
export abstract class BackendBaseService {

    protected baseURL = '/api/v1';

    constructor(protected http: HttpClient) { }

    parseParams(params: any): HttpParams {
        const httpParams = new HttpParams({
            fromObject: params
        });
        return httpParams;
    }

}
