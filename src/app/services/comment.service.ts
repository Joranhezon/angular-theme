import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpResponse } from '@angular/common/http';

import { ArticleService } from './article.service';
import { BackendBaseService } from './backend_base.service';
import { Article } from '../models/article.model';
import { Comment } from '../models/comment.model';
import { Observable } from 'rxjs/Observable';
import { DefaultResponse } from '../models/default-response.model';

@Injectable()
export class CommentService extends BackendBaseService {

    commentRemoved: EventEmitter<Comment> = new EventEmitter<Comment>();
    commentCreated: EventEmitter<Comment> = new EventEmitter<Comment>();
    commentUpdated: EventEmitter<Comment> = new EventEmitter<Comment>();

    getByArticle(article: Article, params: any = {}): Observable<HttpResponse<Comment[]>> {
        params['without_reply'] = true;

        return this.http.get<Comment[]>(this.baseURL + '/articles/' + article.id + '/comments', {observe: 'response', params: this.parseParams(params)});
    }

    createInArticle(article: Article, comment: Comment): Observable<Comment> {
        return this.http.post<Comment>(this.baseURL  + '/articles/' + article.id + '/comments', {comment: comment});
    }

    removeFromArticle(article: Article, comment: Comment): Observable<DefaultResponse> {
        return this.http.delete<DefaultResponse>(this.baseURL  + '/articles/' + article.id + '/comments/' + comment.id);
    }

}
