import { Injectable, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";

import { BackendBaseService } from './backend_base.service';
import { Tag } from '../models/tag.model';
import { Box } from '../models/box.model';
import { Person } from '../models/person.model';
import { Environment } from '../models/environment.model';


@Injectable()
export class EnvironmentService extends BackendBaseService {

    private environment: Environment;
    @Output() environmentChangeEvent: EventEmitter<Environment> = new EventEmitter();

    get(environmentId: number | string, params?: any): Observable<Environment> {
        params = _.isNil(params) ? {} : params;

        return this.http.get<Environment>(this.baseURL + '/environments/' + environmentId, { params: this.parseParams(params) });
    }

    getCurrentEnvironment(): Environment {
        return this.environment;
    }

    setCurrentEnvironment(environment: Environment) {
        this.environment = environment;
        this.environmentChangeEvent.emit(this.environment);
    }

    getBoxes(environment: Environment): Observable<Box[]> {
        return this.http.get<Box[]>(this.baseURL + '/environments/' + environment.id + '/boxes');
    }

    getTags(environment: Environment, params?: any): Observable<Tag[]> {
        params = _.isNil(params) ? {} : params
        params['order'] = _.isNil(params['order']) ? "taggings_count DESC" : params['order']
        params['limit'] = _.isNil(params['limit']) ? "10" : params['limit']

        return this.http.get<Tag[]>(this.baseURL + '/environments/' + environment.id + '/tags', { params: this.parseParams(params) });
    }

    getEnvironmentPeople(environment: Environment, params?: any): Observable<Person[]> {
        const httpParams = new HttpParams({
            fromObject: params
        });
        return this.http.get<Person[]>(this.baseURL + '/environments/' + environment.id + '/people', { params: this.parseParams(params) });
    }

    getBlockPreview(environment: Environment, blockType: string) {
        const params = { 'block_type': blockType };
        return this.http.get<Person[]>(this.baseURL + '/environments/' + environment.id + '/blocks/preview', { params: this.parseParams(params) });

    }

    update(environment: Environment) {
        return this.http.post<Environment>(this.baseURL + '/environments/' + environment.id, { environment: environment });
    }

    allowChangeIdentifier(): boolean {
        if (!this.environment || !this.environment.settings) return false;
        return this.environment.settings['enable_profile_url_change_enabled'];
    }

}
