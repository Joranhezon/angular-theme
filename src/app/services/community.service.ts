import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as _ from "lodash";

import { BackendBaseService } from './backend_base.service';
import { Community } from '../models/community.model';
import { Person } from '../models/person.model';
import { Profile } from '../models/profile.model';
import { DefaultResponse } from '../models/default-response.model';

@Injectable()
export class CommunityService extends BackendBaseService {

    createNewCommunity(community: Community): Observable<Community> {
        const params: any = {
            community: Object.assign({}, _.omitBy(_.pick(community, ['name', 'closed']), _.isNull))
        };

        return this.http.post<Community>(this.baseURL  + '/communities/', params);
    }

    // FIXME see if it's needed
    // updateCommunity(community: Community) {
    //     const headers = {
    //         'Content-Type': 'application/json'
    //     };
    //     const attributesToUpdate: any = {
    //         community: Object.assign({}, _.omitBy(_.pick(community, ['name', 'closed']), _.isNull))
    //     };
    //     const restRequest = this.getElement(community.id).customOperation("patch", null, null, headers, attributesToUpdate);
    //     return restRequest.toPromise().then(this.getHandleSuccessFunction());
    // }

    // FIXME see if it's needed
    // getByOwner(owner: any, params?: any) {
    //     // TODO see a better way to verify the owner type
    //     if (owner.type === "Person") {
    //         return this.getByPerson(owner, params);
    //     } else {
    //         return this.getByEnvironment(params);
    //     }
    // }

    // FIXME see if it's needed
    // getByEnvironment(params?: any) {
    //     return this.list(null, params);
    // }

    // FIXME see if it's needed
    // getByPerson(person: Person, params?: any) {
    //     const personElement = '/profiles/' + person.id;
    //     return this.list(personElement, params);
    // }

    sendInvitations(community: Community, people: Person[]): Observable<DefaultResponse> {
        const invitations = [];
        for (const invitation of people) {
            invitations.push(`${invitation.id}`);
        }
        const params = { 'contacts': invitations };

        return this.http.post<DefaultResponse>(this.baseURL  + '/communities/' + community.id + '/invite', params);
    }

    getMembershipState(person: Person, profile: Profile): Observable<DefaultResponse> {
        if ( person) {
            const params = { identifier: person.identifier }
            return this.http.get<DefaultResponse>(this.baseURL + '/communities/' + profile.id + '/membership', {params: this.parseParams(params)});
        } else {
            return Observable.throw({})
        }
    }
}
