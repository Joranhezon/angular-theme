import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { DomainService } from './domain.service';
import * as helpers from '../../spec/helpers';
import { Environment } from '../models/environment.model';
import { Profile } from '../models/profile.model';
import { Domain } from '../models/domain.model';

describe("Services", () => {
    describe("Domain Service", () => {
        let service: DomainService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    DomainService,
                ]
            });

            service = TestBed.get(DomainService);
        }));
        const environment = <Environment>{ id: 1 };
        const profile = <Profile>{ id: 1 };
        const domains = [
            { id: 1, name: "somedomain.net", owner: environment, is_default: true },
            { id: 2, name: "someotherdomain.net", owner: profile, is_default: false }
        ];

        it("should get articles by profile", () => {
            const domainId = 1;
            // FIXME refactor this test to use the community test behavior
            // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/domains/${domainId}`, { name: "domain1" }, {}, 200);
            service.get(<number>domainId).subscribe((domain: Domain) => {
                expect(domain).toEqual(<Domain>{ name: "domain1" });
            });
        });
    });
});
