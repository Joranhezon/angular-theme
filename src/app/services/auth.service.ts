import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BackendBaseService } from './backend_base.service';
import { Observable } from 'rxjs/Observable';

import { PersonService } from './../services/person.service';
// import { Injectable, Inject, EventEmitter } from '@angular/core';
import { SessionService } from './session.service';
// import { Http, Response } from '@angular/http';
import { Person } from '../models/person.model';
import { User } from '../models/user.model';
import { Credentials } from '../models/credentials.model';
import { Profile } from '../models/profile.model';
import { DefaultResponse } from '../models/default-response.model';

@Injectable()
export class AuthService extends BackendBaseService {

    public loginSuccess: EventEmitter<User> = new EventEmitter<User>();
    public loginFailed: EventEmitter<any> = new EventEmitter<any>();
    public logoutSuccess: EventEmitter<User> = new EventEmitter<User>();

    constructor(protected http: HttpClient, private sessionService: SessionService) {
        super(http);
    }

    login(credentials: Credentials): Observable<User> {
        return this.http.post<User>(this.baseURL  + '/login', credentials);
    }

    logout(user?: User): Observable<DefaultResponse> {
        return this.http.post<DefaultResponse>(this.baseURL  + '/logout', {});    }

    createAccount(user: User): Observable<User> {
        return  this.http.post<User>(this.baseURL  + '/register', user);
    }

    isAuthenticated() {
        return !!this.currentUser();
    }

    currentUser(): User {
        return this.sessionService.currentUser();
    }

    currentPerson(): Person {
        return this.currentUser() ? this.currentUser().person : null;
    }

    forgotPassword(value: string): Observable<DefaultResponse> {
        return  this.http.post<DefaultResponse>(this.baseURL  + '/forgot_password', { value: value });
    }

    newPassword(code: string, password: string, password_confirmation: string): Observable<User> {
        return  this.http.patch<User>(this.baseURL  + '/new_password', { code: code, password: password, password_confirmation: password_confirmation });
    }


    changePassword(profile: Profile, current_password: string, new_password: string, new_password_confirmation: string): Observable<User>  {
        const params = {current_password: current_password, new_password: new_password, new_password_confirmation: new_password_confirmation };

        return  this.http.patch<User>(this.baseURL  + '/users/' + profile.id, params);
    }

    loginSuccessCallback(user: User) {
        const currentUser: User = this.sessionService.create(user);
        this.loginSuccess.next(currentUser);
        return currentUser;
    }

    logoutSuccessCallback(user?: User) {
        // const user: User = this.sessionService.currentUser();
        this.sessionService.destroy();
        this.logoutSuccess.next(user);
    }

    loginFromCookie() {
        if (this.sessionService.currentUser()) return;
        const url = '/api/v1/login_from_cookie';
        return this.http.post<User>(this.baseURL  + '/login_from_cookie', {});
    }

    // FIXME see if this method is needed
    // private loginFailedCallback(response: any): any {
    //     this.loginFailed.next(response);
    //     return null;
    // }

    // FIXME see if this method is needed
    // public isAuthorized(authorizedRoles: string | string[]) {
    //     if (!(authorizedRoles instanceof Array)) {
    //         authorizedRoles = [<string>authorizedRoles];
    //     }
    //     return (this.isAuthenticated() && authorizedRoles.indexOf(this.sessionService.currentUser().userRole) !== -1);
    // }

    // subscribe(eventName: string, fn: Function) {

    //     const event: EventEmitter<any> = <EventEmitter<any>>(<any>this)[eventName];
    //     if (event) {
    //         event.subscribe(fn);
    //     } else {
    //         throw new Error(`The event: ${eventName} not exists`);
    //     }
    // }

    // FIXME see if this method is needed


    // FIXME see if this method is needed
    // reloadUser() {
    //     this.personService.getLoggedPerson().subscribe((person: Person) => {
    //         const user = this.sessionService.currentUser();
    //         user.person = person;
    //         person['user'] = null;
    //         this.loginSuccessCallback({data: user});
    //     }, (error: any) => {
    //         this.logout();
    //     });
    // }

}
