import { Injectable, Inject, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpResponse } from '@angular/common/http';
import * as _ from "lodash";

import { BackendBaseService } from './backend_base.service';
import { Role } from '../models/role.model';
import { Profile } from '../models/profile.model';
import { Person } from '../models/person.model';
import { Block } from '../models/block.model';
import { Article } from '../models/article.model';
import { Box } from '../models/box.model';
import { Tag } from '../models/tag.model';
import { Activity } from '../models/activity.model';
import { DefaultResponse } from '../models/default-response.model';

@Injectable()
export class ProfileService extends BackendBaseService {

    private profile: Profile;

    @Output() profileChangeEvent: EventEmitter<Profile> = new EventEmitter();
    @Output() profileImageChangeEvent: EventEmitter<Profile> = new EventEmitter();

    getCurrentProfile(): Profile {
        return this.profile;
    }

    setCurrentProfile(profile: Profile) {
        this.profile = profile;
        this.profileChangeEvent.emit(this.profile);
    }

    getHomePage(profile: Profile, params?: any): Observable<Article> {
        return this.http.get<Article>(this.baseURL + '/profiles/' + profile.id + '/home_page', {params: this.parseParams(params)});
    }

    getByIdentifier(identifier: string, params?: any): Observable<Profile> {
        params = _.isObject(params) ? _.assign({ key: "identifier" }, params) : { key: "identifier" };

        return this.http.get<Profile>(this.baseURL + '/profiles/' + identifier, {params: this.parseParams(params)});
    }

    // FIXME See if usefull
    // getProfileMembers(profileId: number, params?: any): Observable<Profile[]> {
    //     return this.http.get<Profile[]>(this.baseURL + '/profiles/' + profileId + '/members', {params: this.parseParams(params)});
    // }

    getBoxes(profile: Profile): Observable<Box[]> {
        return this.http.get<Box[]>(this.baseURL + '/profiles/' + profile.id + '/boxes');
    }

    getActivities(profile: Profile, params?: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(this.baseURL + '/profiles/' + profile.id + '/activities', {params: this.parseParams(params)});
    }

    getNetworkActivities(profile: Profile, params?: any): Observable<Activity[]> {
        return this.http.get<Activity[]>(this.baseURL + '/profiles/' + profile.id + '/network_activities', {params: this.parseParams(params)});
    }

    //   FIXME see it's usefull
    // getMembers(profile: Profile, params?: any): Observable<Person[]> {
    //     return this.http.get<Person[]>(this.baseURL + '/profiles/' + profile.id + '/members', {params: this.parseParams(params)});
    // }

    getMembersByRole(profile: Profile, role: Role, params?: any): Observable<HttpResponse<Person[]>>  {
        params = !params ? {} : params;
        params['roles'] = _.isNull(role) ? [] : [role.id];

        return this.http.get<Person[]>(this.baseURL + '/profiles/' + profile.id + '/members', { observe: 'response', params: this.parseParams(params) });
    }

    getBlockPreview(profile: Profile, blockType: string): Observable<Block>  {
        const params = { 'block_type': blockType };

        return this.http.get<Block>(this.baseURL + '/profiles/' + profile.id + '/blocks/preview', {params: this.parseParams(params)});
    }

    getTags(profile: Profile): Observable<Tag[]> {
        return this.http.get<Tag[]>(this.baseURL + '/profiles/' + profile.id + '/tags');
    }

    addMember(person: Person, profile: Profile): Observable<DefaultResponse> {
        return this.http.post<DefaultResponse>(this.baseURL  + '/profiles/' + profile.id + '/members', {});
    }

    removeMember(person: Person, profile: Profile): Observable<Person> {
        return this.http.delete<Person>(this.baseURL  + '/profiles/' + profile.id + '/members', {});
    }

    update(profile: Profile) {
        return this.http.post<Profile>(this.baseURL  + '/profiles/' + profile.id, { profile: profile });
    }


    uploadImage(profile: Profile, base64ImageJson: any, type: string): Observable<Profile> {
        // TODO dynamically copy the selected attributes to update
        let attributesToUpdate: any = {
            profile: { image_builder: base64ImageJson }
        };
        if (type === "top") {
            attributesToUpdate = {
                profile: { top_image_builder: base64ImageJson }
            };
        }

        return this.http.post<Profile>(this.baseURL  + '/profiles/' + profile.id, attributesToUpdate);
    }

    remove(profile: Profile): Observable<DefaultResponse> {
        return this.http.delete<DefaultResponse>(this.baseURL  + '/profiles/' + profile.id);
    }

    // isMember(person: Person, profile: Profile) {
    //     if (person) {
    //         return this.getMembers(profile, { identifier: person.identifier }).then((result: any) => {
    //             return result.data.length > 0;
    //         });
    //     } else {
    //         return Observable.resolve(false);
    //     }
    // }
}
