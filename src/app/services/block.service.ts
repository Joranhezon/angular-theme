import { Injectable, Inject } from '@angular/core';

import { Block } from '../models/block.model';
import { BackendBaseService } from './backend_base.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BlockService extends BackendBaseService {

    getBlock(block: Block, params?: any): Observable<Block> {
        return this.get(block.id, params);
    }

    get(blockId: number, params?: any): Observable<Block> {
        return this.http.get<Block>(this.baseURL + '/blocks/' + blockId, {params: this.parseParams(params)});
    }

    // See if it's needed
    // update(block: Block): Observable<Block> {
    //     return this.http.post<Block>(this.baseURL  + '/blocks/' + block.id,  { block: block });
    // }

    // See if it's needed
    // updateAll(blocks: Block[]) {
    //     const headers = {
    //         'Content-Type': 'application/json'
    //     };
    //     return this.patch({ blocks: blocks }, headers);
    // }

    uploadImages(block: Block, base64ImagesJson: any): Observable<Block> {
        return this.http.post<Block>(this.baseURL  + '/blocks/' + block.id,  { block: { images_builder: base64ImagesJson } });
    }

    // See if it's needed
    // getAvailableBlocks(owner: Profile | Environment): Promise<RestResult<BlockDefinition[]>> {
    //     let restRequest;
    //     if (owner.type === 'Environment') {
    //         restRequest = this.restangular.one("environments", owner.id);
    //     } else {
    //         restRequest = this.restangular.one("profiles", owner.id);
    //     }
    //     return restRequest.all("blocks").get("available_blocks").toPromise();
    // }
}
