import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';

import { PersonService } from './person.service';
import { CommunityService } from './community.service';
import { Community } from '../models/community.model';
import { Person } from '../models/person.model';
import { DefaultResponse } from '../models/default-response.model';
import * as helpers from '../../spec/helpers';

describe("Services", () => {
    describe("Community Service", () => {
        let service: CommunityService;
        const mocks = helpers.getMocks();
        let httpMock: HttpTestingController;

        beforeEach(async(() => {

            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    CommunityService,
                    { provide: PersonService, useValue: mocks.personService },
                ],
            });
            service = TestBed.get(CommunityService);
            httpMock = TestBed.get(HttpTestingController);
        }));

        afterEach(() => {
            httpMock.verify();
        });

        describe("Succesfull requests", () => {
            it("should send community invitations", () => {
                const communityId = 1;
                const people = [<Person>{id: 1}, <Person>{id: 3}, <Person>{id: 5}]
                const response = <DefaultResponse>{ success: true };
                const params = {'contacts': ['1', '3', '5']};

                service.sendInvitations(<Community>{ id: communityId }, people).subscribe((localResponse: DefaultResponse) => {
                    expect(localResponse.success).toBeTruthy();
                });

                const req = httpMock.expectOne(`/api/v1/communities/${communityId}/invite`);
                expect(req.request.body).toEqual(params);
                expect(req.request.method).toBe("POST");
                req.flush(response);

            });

            // FIXME see if it's needed
            // xit("should list environment communities", () => {
            //     helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/communities`,
            //         { communities: [{ name: "community1" }] }, {}, 200);
            //     service.getByEnvironment().then((result: Community[]) => {
            //         expect(result.data).toEqual(<Community[]>[{ name: "community1" }]);
            //     });
            // });

            // it("should list person communities", (done) => {
            //     $httpBackend.expectGET(`/api/v1/people/1/communities`).respond(200, { communities: [{ name: "community1" }] });
            //     let person = <any>{ id: 1 };
            //     communityService.getByPerson(person).then((result: RestResult<Community[]>) => {
            //         expect(result.data).toEqual([{ name: "community1" }]);
            //         done();
            //     });
            //     $httpBackend.flush();
            // });

            // it("should list owner communities when it is an environment", (done) => {
            //     $httpBackend.expectGET(`/api/v1/communities`).respond(200, { communities: [{ name: "community1" }] });
            //     let owner = <any>{ id: 1 };
            //     communityService.getByOwner(owner).then((result: RestResult<Community[]>) => {
            //         done();
            //     });
            //     $httpBackend.flush();
            // });

            // it("should list owner communities when it is an person", (done) => {
            //     $httpBackend.expectGET(`/api/v1/people/1/communities`).respond(200, { communities: [{ name: "community1" }] });
            //     let owner = <any>{ id: 1, type: "Person" };
            //     communityService.getByOwner(owner).then((result: RestResult<Community[]>) => {
            //         done();
            //     });
            //     $httpBackend.flush();
            // });
        });
    });
});
