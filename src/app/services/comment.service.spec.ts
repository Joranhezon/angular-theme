import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {MockBackend, MockConnection} from '@angular/http/testing';

import { EnvironmentService } from './environment.service';
import { ProfileService } from './profile.service';
import { ArticleService } from './article.service';
import {CommentService} from './comment.service';
import { Article } from '../models/article.model';
import { Comment } from '../models/comment.model';
import { DefaultResponse } from '../models/default-response.model';
import * as helpers from '../../spec/helpers';

describe("Services", () => {
    describe("Comment Service", () => {
        let commentService: CommentService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    ProfileService, EnvironmentService, ArticleService, CommentService,
                    { provide: ProfileService, useValue: mocks.profileService},
                    { provide: EnvironmentService, useValue: mocks.environmentService},
                    { provide: ArticleService, useValue: mocks.articleService},
                ]
            });

            commentService = TestBed.get(CommentService);
        }));

        describe("Succesfull requests", () => {
            it("should return comments by article", () => {
                const articleId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/articles/${articleId}/comments?without_reply=true`,
                //     {comments: [{ name: "comment1" }]}, {'total': 1}, 200);
                commentService.getByArticle(<Article>{ id: articleId }).subscribe((result: any) => {
                    expect(result.body).toEqual(<any>[{ name: "comment1" }]);
                });
            });

            it("should create a comment in an article", () => {
                const articleId = 1;
                const comment = <Comment>{ id: null };

                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/articles/${articleId}/comments`, { comment: { id: 2 } }, {}, 200);
                // service.createInProfile(<Profile>{ id: profileId }, article).subscribe((article: Article) => {

                commentService.createInArticle(<Article>{ id: articleId }, comment).subscribe((localComment: Comment) => {
                    // commentService.createInArticle(<Profile>{ id: profileId }, article).subscribe((article: Article) => {
                    // expect(result.data).toEqual(<any>{ id: 2 });
                    expect(localComment).toEqual(<Comment>{ id: 2 });
                });
            });


            it("should remove a comment from an article", () => {
                const articleId = 1;
                const comment: Comment = <any>{ id: 2 };

                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/articles/${articleId}/comments/${comment.id}`, { success: "true" }, { comment: { id: 2 } }, 200);
                commentService.removeFromArticle(<Article>{ id: articleId }, comment).subscribe((response: DefaultResponse) => {
                    expect(response.success).toBeTruthy();

                });
            });
        });
    });
});
