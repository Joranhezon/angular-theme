import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpResponse } from '@angular/common/http';

import { BackendBaseService } from './backend_base.service';
import { Person } from '../models/person.model';
import { Community } from '../models/community.model';
import { Profile } from '../models/profile.model';
import { DefaultResponse } from '../models/default-response.model';

@Injectable()
export class PersonService extends BackendBaseService {

    getLoggedPerson(): Observable<Person> {
        return this.http.get<Person>(this.baseURL + '/people/me');
    }

    getFriends(profile: Profile, params?: any): Observable<HttpResponse<Person[]>> {
        return this.http.get<Person[]>(this.baseURL + '/people/' + profile.id + '/friends', {observe: 'response', params: this.parseParams(params)});
    }

    getCommunities(profile: Profile, params?: any): Observable<HttpResponse<Community[]>> {
        return this.http.get<Community[]>(this.baseURL + '/people/' + profile.id + '/communities', {observe: 'response', params: this.parseParams(params)});
    }

    // FIXME see if it's usefull
    // isFriend(profileId: number, friendId: number, params?: any): Observable<Person> {
    //     return this.http.get<Person>(this.baseURL + '/people/' + profileId + "/friends/" + friendId, {params: this.parseParams(params)});
    // }

    addFriend(profile: Profile, params?: any): Observable<DefaultResponse> {
        return this.http.post<DefaultResponse>(this.baseURL  + '/people/' + profile.id + '/friends', {});
    }

    removeFriend(profile: Profile, params?: any): Observable<DefaultResponse> {
        return this.http.delete<DefaultResponse>(this.baseURL  + '/people/' + profile.id + '/friends', {});
    }

    // FIXME see if it's usefull
    // uploadImage(profile: Profile, base64ImageJson: any): Observable<DefaultResponse> {
    //     return this.http.post<DefaultResponse>(this.baseURL  + '/people/' + profile.id + '/friends', {person: { image_builder: base64ImageJson }});
    // }

    getFriendshipState(person: Person, friend: Profile): Observable<DefaultResponse>  {
        const params = { friend_id: friend.id };

        return this.http.get<DefaultResponse>(this.baseURL + '/people/' + person.id + '/friendship/', {params: this.parseParams(params)});
    }

    search(params: any): Observable<Person[]> {
        return this.http.get<Person[]>(this.baseURL + '/people', {params: this.parseParams(params)});
    }
}
