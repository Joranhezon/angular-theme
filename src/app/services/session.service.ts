import { Injectable, Inject, EventEmitter } from '@angular/core';
import { PersonService } from '../services/person.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { User } from '../models/user.model';
import { Person } from '../models/person.model';

@Injectable()
export class SessionService {

    constructor(private localStorageService: LocalStorageService) { }

    create(user: User): User {
        this.localStorageService.store('currentUser', user);
        return this.localStorageService.retrieve('currentUser');
    };

    destroy() {
        this.localStorageService.clear('currentUser');
        this.localStorageService.clear('settings');
    };

    currentUser(): User {
        return this.localStorageService.retrieve('currentUser');
    };

    currentPerson(): Person {
        return this.currentUser() ? this.currentUser().person : null;
    };

    setPerson(person: Person): Person {
        const user = this.currentUser();
        user.person = person;
        this.localStorageService.store('currentUser', user);
        return person;
    }

    getPrivateToken(): string {
        return this.currentUser() ? this.currentUser().private_token : null;
    };

}
