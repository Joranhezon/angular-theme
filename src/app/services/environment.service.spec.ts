import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { EnvironmentService } from './environment.service';
import * as helpers from '../../spec/helpers';
import { Environment } from '../models/environment.model';

describe("Services", () => {
    describe("Environment Service", () => {
        let service: EnvironmentService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            spyOn(mocks.sessionService, "destroy");
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    EnvironmentService,
                ]
            });
            service = TestBed.get(EnvironmentService);
        }));

        describe("Succesfull requests", () => {
            it("should return the boxes of environment ", () => {
                const environment = <Environment>{ id: 1 };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/environments/${environment.id}/boxes`, [{ position: 1 }], {}, 200);
                service.getBoxes(environment).subscribe((response: any) => {
                    expect(response.data[0]).toEqual({ position: 1 });
                });
            });

            it("should return the tags of environment ", () => {
                const environment = <Environment>{ id: 1 };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/environments/${environment.id}/tags`, [{ position: 1 }], {}, 200);
                service.getTags(environment).subscribe((response: any) => {
                    expect(response.data[0]).toEqual({ position: 1 });
                });
            });

            it("should return all people of environment", () => {
                const environment = <Environment>{ id: 1 };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/environments/${environment.id}/people`, [{ id: 2 }], {}, 200);
                service.getEnvironmentPeople(environment).subscribe((response: any) => {
                    expect(response.data).toEqual(jasmine.objectContaining([{ id: 2 }]));
                });
            });
        });
    });
});
