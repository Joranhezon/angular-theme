import { Observable } from 'rxjs/Observable';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';

import { PersonService } from './../services/person.service';
import { AuthService, AuthEvents } from './';
import { SessionService } from './session.service';
import * as helpers from '../../spec/helpers';
import { User } from '../models/user.model';
import { Credentials } from '../models/credentials.model';

// FIXME refactor this test
describe("Services", () => {
    describe("Auth Service", () => {
        let credentials: Credentials;
        let user: User;
        let service: AuthService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            spyOn(mocks.sessionService, "destroy");
            user = <User>{ id: 1, login: "user" };
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    { provide: PersonService, useValue: mocks.personService },
                    { provide: SessionService, useValue: mocks.sessionService },
                    AuthService,
                ]
            });
            service = TestBed.get(AuthService);
        }));

        describe("Succesffull login", () => {
            beforeEach(() => {
                credentials = { login: "user", password: "password" };
                const data = new FormData();
                data.append('login', 'user');
                data.append('password', 'password');
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/login`, user, {}, 200);
            });

            xit("should return loggedUser", async(() => {
                service.login(credentials).subscribe((loggedUser) => {
                    expect(loggedUser).toBeDefined();
                });
            }));

            xit("should emit event loggin successful with user logged data", (done: Function) => {
                const successEvent: any = AuthEvents[AuthEvents.loginSuccess];
                (<any>service)[successEvent].subscribe((userThroughEvent: User): any => {
                    expect(userThroughEvent).toEqual(jasmine.objectContaining(user));
                    done();
                });
                service.login(credentials);
            });

            xit("should return the current logged in user", () => {
                service.login(credentials);
                const actual: User = service.currentUser();
                expect(actual.person).toEqual(jasmine.objectContaining(user), "The returned user must be present");
            });

            it("should destroy session after logoutSuccessCallback", () => {
                service.logoutSuccessCallback();
                expect(service['sessionService'].destroy).toHaveBeenCalled();
            });
        });

        describe("Succesfull requests", () => {
            xit("should creaet a new account", () => {
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/register?email=${user.email}&id=${user.id}&login=${user.login}`,
                //     [{ login: "test" }], {}, 201);
                service.createAccount(user).subscribe((response: any) => {
                    expect(response.data[0].login).toEqual("test");
                });
            });
        });

        // FIXME copied from password service
        describe("Succesfull request", () => {
            xit("should change user password", () => {
                const data = {
                    code: '1234567890',
                    password: 'test',
                    password_confirmation: 'test'
                };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/new_password?code=${data.code}&password=${data.password}&password_confirmation=${data.password_confirmation}`,
                //     [{ login: "test" }], {}, 201);
                service.newPassword('1234567890', 'test', 'test').subscribe((response: any) => {
                    expect(response.data[0].login).toEqual("test");
                });
            });
        });

    });
});
