import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, TestBed } from '@angular/core/testing';
import {MockBackend, MockConnection} from '@angular/http/testing';

import {BlockService} from './block.service';
import { Block } from '../models/block.model';
import * as helpers from '../../spec/helpers';

describe("Services", () => {
    describe("Block Service", () => {
        let service: BlockService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    BlockService,
                ]
            });
            service = TestBed.get(BlockService);
        }));

        describe("Succesfull requests", () => {
            it("should return api content of a block", () => {
                const blockId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/blocks/${blockId}`, { block: { api_content: [{ name: "article1" }] } }, {}, 200);
                service.getBlock(<Block>{ id: blockId }).subscribe((content: any) => {
                    expect(content).toEqual([{ name: "article1" }]);
                });
            });

            // FIXME see if it's needed
            // it("update block settings", () => {
            //     const blockId = 1;
            //     helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/blocks/${blockId}`, { block: { id: blockId } }, {}, 200);
            //     service.update(<any>{ id: blockId, display: 'never' }).then((result: RestResult<Block>) => {
            //         expect(result.data).toEqual(<Block>{ id: blockId });
            //     });
            // });
        });
    });
});
