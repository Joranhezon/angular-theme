import { async, fakeAsync, tick, TestBed, ComponentFixture } from '@angular/core/testing';
import { MockBackend, MockConnection } from '@angular/http/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import * as helpers from '../../spec/helpers';
import { Article } from '../models/article.model';
import { Profile } from '../models/profile.model';
import { EnvironmentService } from './index';
import { ArticleService } from './article.service';

describe("Services", () => {
    describe("Article Service", () => {
        let service: ArticleService;
        const mocks = helpers.getMocks();

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [HttpClientTestingModule],
                providers: [
                    ArticleService,
                    { provide: EnvironmentService, useValue: mocks.environmentService },

                ]
            });
            service = TestBed.get(ArticleService);
        }));

        describe("Succesfull requests", () => {
            it("should remove article", () => {
                const articleId = 1;
                service.remove(<Article>{ id: articleId });
            });

            it("should return article children", () => {
                const articleId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/articles/${articleId}/children`, { articles: [{ name: "article1" }] }, {}, 200);
                service.getChildren(<Article>{ id: articleId }).subscribe((result: any) => {
                    expect(result.body).toEqual(<Article[]>[{ name: "article1" }]);
                });
            });

            it("should get articles by profile", () => {
                const profileId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/articles`, { articles: [{ name: "article1" }] }, {}, 200);
                service.getByProfile(<Profile>{ id: profileId }).subscribe((articles: Article[]) => {
                    expect(articles).toEqual(<Article[]>[{ name: "article1" }]);
                });
            });

            it("should get articles by profile with additional filters", () => {
                const profileId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/articles?path=test`, { articles: [{ name: "article1" }] }, {}, 200);
                service.getByProfile(<Profile>{ id: profileId }, { path: 'test' }).subscribe((articles: Article[]) => {
                    expect(articles).toEqual(<Article[]>[{ name: "article1" }]);
                });
            });

            it("should get article children with additional filters", () => {
                const articleId = 1;
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/articles/${articleId}/children?path=test`, { articles: [{ name: "article1" }] }, {}, 200);
                service.getChildren(<Article>{ id: articleId }, { path: 'test' }).subscribe((result: any) => {
                    expect(result.body).toEqual(<Article[]>[{ name: "article1" }]);
                });
            });

            it("should create an article in a profile", () => {
                const profileId = 1;
                const article: Article = <any>{ id: null };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/profiles/${profileId}/articles`, { article: { id: 2 } }, {}, 200);
                service.createInProfile(<Profile>{ id: profileId }, article).subscribe((localArticle: Article) => {
                    expect(localArticle).toEqual(<Article>{ id: 2 });
                });
            });

            it("should search for articles in environment", () => {
                const profileId = 1;
                const article: Article = <any>{ id: null };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/search/article?query=query`, { articles: [{ id: 2 }] }, {}, 200);
                service.search({ query: 'query' }).subscribe((result: any) => {
                    expect(result.body).toEqual(<Article[]>[{ id: 2 }]);
                });
            });

            it("should update an article", () => {
                const article: Article = <any>{ id: 10, name: "article name" };
                // FIXME refactor this test to use the community test behavior
                // helpers.mockBackendConnection(TestBed.get(MockBackend), `/api/v1/articles/10`, { article: { id: 2 } }, {}, 200);
                service.updateArticle(article).subscribe((localArticle: Article) => {
                    expect(localArticle).toEqual(<Article>{ id: 2 });
                });
            });
        });
    });
});
