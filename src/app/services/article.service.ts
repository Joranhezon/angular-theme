import { Title } from '@angular/platform-browser';
import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { HttpResponse } from '@angular/common/http';
import * as _ from "lodash";

import { BackendBaseService } from './backend_base.service';
import { EnvironmentService } from './environment.service';
import { Environment } from '../models/environment.model';
import { Profile } from '../models/profile.model';
import { Article } from '../models/article.model';
import { DefaultResponse } from '../models/default-response.model';

@Injectable()
export class ArticleService extends BackendBaseService {

    article: Article;
    environment: Environment;
    articleRemoved: EventEmitter<Article> = new EventEmitter<Article>();
    articleCreated: EventEmitter<Article> = new EventEmitter<Article>();

    constructor(protected http: HttpClient, private titleService: Title,
        private environmentService: EnvironmentService) {
        super(http);
    }


    setCurrent(article: Article) {
        this.article = article;
    }

    getCurrent(): Article {
        return this.article;
    }

    get(articleId: number|string): Observable<Article> {
        return this.http.get<Article>(this.baseURL + '/articles/' + articleId);
    }

    getByProfile(profile: Profile, params?: any): Observable<Article[]> {
        return this.http.get<Article[]>(this.baseURL + '/profiles/' + profile.id + '/articles', {params: this.parseParams(params)});
    }

    getArticleByProfileAndPath(profile: Profile, path: string): Observable<Article> {
        const params = { key: 'path' };

        return this.http.get<Article>(this.baseURL + '/profiles/' + profile.id + '/articles/' + path, {params: this.parseParams(params)});
    }

    getChildren(article: Article, params?: any): Observable<HttpResponse<Article[]>> {
        return this.http.get<Article[]>(this.baseURL + '/articles/' + article.id + '/children', {observe: 'response', params: this.parseParams(params)});
    }

    search(params: any): Observable<HttpResponse<Article[]>> {
        return this.http.get<Article[]>(this.baseURL + '/articles', {observe: 'response', params: this.parseParams(params)});
    }

    updateArticle(article: Article): Observable<Article> {
        const params: any = {
            article: Object.assign({}, _.omitBy(_.pick(article, ['name', 'body', 'published', 'start_date', 'end_date']), _.isNull))
        };

        return this.http.post<Article>(this.baseURL  + '/articles/' + article.id, params);
    }

    createInProfile(profile: Profile, article: Article): Observable<Article> {
        return this.http.post<Article>(this.baseURL  + '/profiles/' + profile.id + '/articles', {article: article});
    }

    createInParent(parentId: number, article: Article): Observable<Article> {
        return this.http.post<Article>(this.baseURL  + '/articles/' + parentId + '/children', {article: article});
    }

    remove(article: Article): Observable<DefaultResponse> {
        return this.http.delete<DefaultResponse>(this.baseURL  + '/articles/' + article.id );
    }



}
