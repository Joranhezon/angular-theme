import { HotspotModule } from './hotspot.module';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, Input } from '@angular/core';
import * as plugins from '../../plugins';
import {PluginHotspot} from './plugin-hotspot';
import { Comment } from '../models/comment.model';

export interface CommentFormHotspotInterface {
    comment: Comment;
    parent: Comment;
}

@Component({
    selector: "noosfero-hotspot-comment-form",
    template: '',
})
export class CommentFormHotspotComponent extends PluginHotspot {

    @Input() comment: Comment;
    @Input() parent: Comment;

    constructor(private viewContainerRef: ViewContainerRef, private factory: ComponentFactoryResolver) {
        super("comment_form_extra_contents");
    }

    addHotspot(component: any) {
        const compFactory = this.factory.resolveComponentFactory(component);
        this.viewContainerRef.clear();
        const componentRef = (this.viewContainerRef.createComponent(compFactory) as ComponentRef<CommentFormHotspotInterface>);
        componentRef.instance.comment = this.comment;
        componentRef.instance.parent = this.parent;
        componentRef.changeDetectorRef.detectChanges();
    }
}
