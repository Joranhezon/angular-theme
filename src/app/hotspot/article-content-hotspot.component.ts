import { HotspotModule } from './hotspot.module';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, Input } from '@angular/core';
import { PluginHotspot } from './plugin-hotspot';
import { Article } from '../models/article.model';

export interface ArticleContentHotspotInterface {
    article: Article;
}

@Component({
    selector: "noosfero-hotspot-article-content",
    template: '',
})
export class ArticleContentHotspotComponent extends PluginHotspot {

    @Input() article: Article;

    constructor(private viewContainerRef: ViewContainerRef, private factory: ComponentFactoryResolver) {
        super("article_extra_content");
    }

    addHotspot(component: any) {
        const compFactory = this.factory.resolveComponentFactory(component);
        this.viewContainerRef.clear();
        const componentRef = (this.viewContainerRef.createComponent(compFactory) as ComponentRef<ArticleContentHotspotInterface>);
        componentRef.instance.article = this.article;
        componentRef.changeDetectorRef.detectChanges();
    }
}
