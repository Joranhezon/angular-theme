import { HotspotModule } from './hotspot.module';
import { Component, ComponentFactoryResolver, ComponentRef, ViewContainerRef, Input } from '@angular/core';
import { PluginHotspot } from './plugin-hotspot';
import * as plugins from '../../plugins';
import { Article } from '../models/article.model';

export interface ArticleToolbarHotspotInterface {
    article: Article;
}

@Component({
    selector: "noosfero-hotspot-article-toolbar",
    template: '',
})
export class ArticleToolbarHotspotComponent extends PluginHotspot {

    @Input() article: Article;

    constructor(private viewContainerRef: ViewContainerRef, private factory: ComponentFactoryResolver) {
        super('article_extra_toolbar_buttons');
    }

    addHotspot(component: any) {
        const compFactory = this.factory.resolveComponentFactory(component);
        this.viewContainerRef.clear();
        const componentRef = (this.viewContainerRef.createComponent(compFactory) as ComponentRef<ArticleToolbarHotspotInterface>);
        componentRef.instance.article = this.article;
        componentRef.changeDetectorRef.detectChanges();
    }
}
